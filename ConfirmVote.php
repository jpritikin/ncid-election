<?
// ConfirmVote.php - secure page

function clean($input) {
    $input = stripslashes($input);
    $input = str_replace("'", "''", $input);
    $input = str_replace("\\", "", $input);
    return($input);
}

// session check
session_start();
header("Cache-control: private");
if (!session_is_registered("SESSION"))
{
	// if session check fails, invoke error handler
	header("Location: InvalidLogin.php?e=2");
	exit();
}

$name = $_SESSION['name'];

if (!$_POST['vote']) {
    header("Location: Ballot.php");
} else {
    $vote = clean($_POST['vote']);
}

if (!($vote=="YES" || $vote=="NO")) {
    header("Location: Ballot.php");
    exit();
}

$_SESSION['vote'] = $vote;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Philadelphia II - Confirm Ballot choice</TITLE>
<META content="text/html; charset=iso-8859-1">
<link href="style.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY>
    <?php include("menu-user.htm"); ?>
    <?php include("top.htm"); ?>
		<h1>Confirm your vote</h1>
          <TABLE border="0" width="100%" cellpadding="0" cellspacing="0">
            <TBODY>
              <TR>
                <TD align="center" colspan="4" height="30"><h3>You voted:
                    <?PHP
  if ($vote == "YES") {
	  print "<B>YES</B>, to <B>ENACT</B> the National Initiative for Democracy.<BR>";
  } else {

	  print "<B>No</B>, Not to <B>ENACT</B> the National Initiative for Democracy.<BR>";
  }
  ?>
                    <BR>
                  </h3></TD>
              </TR>
              <TR>
                <TD align="center" class="text"><FORM method="post" id="vote" name="vote" action="Vote.php">
                    If this is correct, click here<BR>
                    <BR>
                    <INPUT type="submit" name="submit" value="Confirm your vote" class="Button">
                  </FORM></TD>
                <TD align="center" class="text"><FORM method="post" id="vote" name="vote" action="Ballot.php">
                    If this is not correct, click here<BR>
                    <BR>
                    <INPUT type="submit" name="return" value="Try Again" class="Button">
                  </FORM></TD>
              </TR>
            </TBODY>
          </TABLE>
          <?php include("bottom.htm"); ?>
</BODY>
</HTML>
