<?php
  session_start();
  $name = $_SESSION["name"];
  header("Cache-control: private");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<TITLE>About This Election</TITLE>
<link href="style.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY>
  <?php
      if (session_is_registered("SESSION")) {
		include("menu-user.htm");
      } else {
	    include("menu-anon.htm");
	  }
  ?>
  <?php include("top.htm"); ?>
          <h1>Summary of the proposed legislation</h1>
          <blockquote>
            <p>The National Initiative for Democracy (NI4D) is a proposed Constitutional amendment which recognizes the Peoples' right to make laws and a federal law which spells out orderly procedures for the People to develop and vote on laws.</p>
            <p>The <a href="Amendment.php">Democracy Amendment</a>, a proposed amendment to the Constitution of the United States of America, will:</p>
            <ol>
              <li>recognize the legislative power of the people to make laws</li>
              <li>permit the people to amend the constitution by holding two successive elections, more than six months but less than one year apart</li>
              <li>sanction the election for The National Initiative permitting the enactment of the Democracy Amendment and the Democracy Act (*),</li>
              <li>create the Electoral Trust to administer the procedures established by the Democracy Amendment and Act,</li>
              <li>outlaw the use of funds from non-natural persons (for example corporate funds) in initiative elections under this article, and</li>
              <li>outlaw non-natural persons (for example corporations) from sponsoring initiatives under this article.</li>
            </ol>
            <p>The <a href="Act.php">Democracy Act</a>, a proposed federal law, will:</p>
            <ol>
              <li>set out deliberative procedures to be used by citizens to create laws by initiative,</li>
              <li>describe the key responsibilities of the Electoral Trust which shall administer the initiative process on behalf of the people,</li>
              <li>appropriate funds for the Electoral Trust from the United States Treasury, and</li>
              <li>specify the threshold of affirmative votes needed to enact this legislation (*).</li>
            </ol>
            <p>The National Initiative does not change or eliminate Congress, the President, or the judicial system. Laws created by initiative must still stand up in the courts just like laws created by Congress. The National Initiative adds an additional check &ndash;&ndash; the People &ndash;&ndash; to our system of checks and balances, while setting up a working partnership between the People and their elected representatives.</p>
            <p>Voting &quot;yes&quot; means that you are for the Democracy Amendment and Democracy Act<br />
              Voting &quot;no&quot; means that you are against the Democracy Amendment and Democracy Act</p>
            <p>(*) This legislation will be considered enacted when the number of affirmative votes is greater than half the total number of government-validated votes cast in the presidential election occurring immediately prior to this election's certification.</p>
            <p>The full text of the proposed Amendment and Act are available here: </p>
            <ul>
              <li><A href="Amendment.php" target="_blank">The Democracy Amendment</A> (562 words, 7 paragraphs) </li>
              <li><A href= "Act.php" target="_blank">The Democracy Act</A> (3132 words, 59 paragraphs)</li>
            </ul>
            <p>To learn more about this election, it's constitutionality, and  Philadelphia II,  read <a href="LearnMore.php">about this election</a>.</p>
          </blockquote>
          <table align="center">
            <tbody>
              <tr>
                <?php if (session_is_registered("SESSION")) { ?>
                <td><FORM action="Ballot.php">
                    <INPUT type="submit" value="Return to your ballot" class="Button">
                  </FORM></td>
                <?php } else { ?>
                <td><FORM action="login.php">
                    <INPUT type="submit" value="Return to login screen" class="Button">
                  </FORM></td>
                <td>&nbsp;</td>
                <td><FORM action="EditNewVoter.php">
                    <INPUT type="submit" value="Register" class="Button">
                  </FORM></td>
                <?php } ?>
              </tr>
            </tbody>
          </table>
          <?php include("bottom.htm"); ?>
</BODY>
</HTML>
