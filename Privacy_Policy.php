<?php
  session_start();
  $name = $_SESSION["name"];
  header("Cache-control: private");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
  <HEAD>
    <TITLE>
      About This Election
    </TITLE>
    <link href="style.css" rel="stylesheet" type="text/css">
  </HEAD>
  <BODY>
	<?php
      if (session_is_registered("SESSION")) {
		include("menu-user.htm");
      } else {
	    include("menu-anon.htm");
	  }
	?>
    <?php include("top.htm"); ?>
	<h1>Privacy Policy</h1>

<p>The non-profit organization Philadelphia II is conducting an impartial national election on <em><strong>The National Initiative for Democracy</strong></em>. This election is conducted electronically on the votep2.us web site and on paper. In order to fulfill our legal requirement to certify that each person voted is eligible to vote in this election, we collect the following information:</p>
<ul>
  <li>Your name.</li>
  <li>Your address, including street, city, postal code, county and state of residence.</li>
  <li>Your telephone number.</li>
  <li>Your county/parish and state where you are registered to vote </li>
  <li>Your date and place of birth</li>
  <li>We do not collect any information about your race or ethnic identity.</li>
  </ul>
<p>This information is used to determine if you are eligible to vote in this election. The above information will be manually compared with your public voting registration in your state of residence. </p>
<p>We also collect and store: </p>
<ul>
  <li>The voter's e-mail address.</li>
  <li>A security question used if you loose your password. </li>
  <li>A Yes or No vote on the National Initiative for Democracy.</li>
  <li>The date the ballot is executed.</li>
</ul>
<p>So that you may access your ballot and keep your personal information up to date, you are provided with : </p>
<ul>
  <li>A Identification Number</li>
  <li>A Password</li>
  </ul>

<p>Similar to a petition, your vote in this election is not
anonymous. Your ballot is not considered valid until it is signed (either
physically or electronically) by you and it is certified that you are
eligible to have voted.  You may be contacted by someone representing
Philadelphia II to verify this information in certifying your ballot.
This information may be used to prove that we have met the legal
obligations for an upstanding election.  Providing accurate
information and keeping your information up to date is the best way to
ensure that your ballot is counted.</p>

<p>If the National Initiative is not approved, all the personal
information we collected, including the ballots, will be
destroyed. If the National Initiative is approved and the
election has been fully verified beyond reasonable doubt, the voter
registration information will be turned over to the newly created
Electoral Trust to help create the national voter registration
database. The ballots (how people voted on The National Initiative)
will be destroyed.</p>

<p>If, instead of completing the election, the National Initiative for
Democracy can be enacted by petition then votes in the affirmative may
be converted into a petition. Petition entries will contain your name,
city, and state. Your other identifying information will never be
disclosed publicly, re-sold, or redistributed. Your name, city, and state
will only be disclosed in a petition to the federal
government. Otherwise, your name, city, state, and how you voted will
never be disclosed publicly, re-sold, or redistributed.</p>

<p>If the registrant agrees on the registration form to be kept
informed, the registrant will receive communications related to
the Philadelphia II election. In that case, your email address is
imported into a mailing list adminstered by The Democracy
Foundation. Your email address goes no farther than this.</p>

<p>All voter information is stored on a secured server.
All software utilized to conduct the election is open source, permitting
scrutiny and auditing by the public.
</p>
<table align="center">
            <tbody>
              <tr>
                <?php if (session_is_registered("SESSION")) { ?>
                <td><FORM action="Ballot.php">
                    <INPUT type="submit" value="Return to your ballot" class="Button">
                  </FORM></td>
                <?php } else { ?>
                <td><FORM action="login.php">
                    <INPUT type="submit" value="Return to login screen" class="Button">
                  </FORM></td>
                <td>&nbsp;</td>
                <td><FORM action="EditNewVoter.php">
                    <INPUT type="submit" value="Register" class="Button">
                  </FORM></td>
                <?php } ?>
              </tr>
            </tbody>
          </table>
          <?php include("bottom.htm"); ?>
  </BODY>
</HTML>

