<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>The National Initiative for Democracy - The Democracy Act</title>
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<?php
      if (session_is_registered("SESSION")) {
		include("menu-user.htm");
      } else {
	    include("menu-anon.htm");
	  }
	?>
<?php include("top.htm"); ?>
          <h1>The Democracy Act</h1>
          <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
            <tbody>
              <tr>
                <td><center>
                    September 17, 2002
                  </center>
				  <blockquote>
                    <p><strong>AN ACT</strong> establishing legislative procedures and an
                    administrative agency to permit the citizens of the United States to
                    exercise their legislative power; and adding to the Federal Code.</p>
                    <p> <strong>Be It Enacted By The People Of The United States:</strong> </p>
                    <p> <strong>Section 1. TITLE. </strong><br>
                      This act shall be known and may be cited as the Democracy Act. </p>
                    <p> <strong>Section 2. PREAMBLE. </strong><br>
                      We, the People of the United
                      States, inherently possess the sovereign authority and power to govern
                      ourselves. We asserted this power in our Declaration of Independence
                      and in the ratification of our Constitution. We, the People, choose now
                      to participate as lawmakers in our local, state and national
                      governments. We, the People, sanction the election conducted by the
                      nonprofit corporation Philadelphia II enabling our empowerment as
                      lawmakers. We, the People, shall exercise our legislative powers by
                      initiative concurrently with the legislative powers we delegated to our
                      elected representatives. THEREFORE, We, the People, enact this
                      Democracy Act, establishing a "Legislature of the People." </p>
                    <p> <strong>Section 3. PROCEDURES. </strong><br>
                      The United States Electoral
                      Trust (hereinafter "Electoral Trust") shall qualify initiatives
                      chronologically and shall conduct the entire initiative process
                      chronologically. The Electoral Trust shall take advantage of
                      contemporary technology in implementing these procedures. The essential
                      elements of the initiative process include, but are not limited to, the
                      following: </p>
                    <ul>
                      <p><strong>A. Sponsor. </strong><br>
                      Only citizens of the United States who
                      are registered to vote may sponsor an initiative. The Sponsor shall be
                      identified on the initiative, on any petition, and on any qualifying
                      poll.</p>
                      <p> <strong>B. Form. </strong><br>
                        An initiative shall comprise a Title, a
                        Summary, a Preamble that states the reasons for, and explains why, the
                        initiative is proposed, and the complete text of the initiative. </p>
                      <p> <strong>C. Content. </strong><br>
                        An initiative shall pertain to a
                        matter of public policy relevant to the government jurisdiction to
                        which it is applicable. The Sponsor shall determine the wording of the
                        initiative. The Title and Summary shall be subject to the approval of
                        the Electoral Trust. </p>
                      <p> <strong>D. Subject. </strong><br>
                        An initiative shall address  one subject only, but may include related or mutually dependent parts. </p>
                      <p> <strong>E. Word Limit. </strong><br>
                        An initiative shall contain no more
                        than five thousand words, exclusive of the Title, Preamble, Summary,
                        References,  Definitions, and language that quotes existing law. </p>
                      <p> <strong>F. Qualification. </strong><br>
                        Following approval of the Title
                        and Summary by the Electoral Trust, an initiative may qualify for
                        election in the relevant government jurisdiction by any one of the
                      following methods: </p>
                      <ul>
                        <p><strong>1) Citizen Petition. </strong><br>
                        An initiative shall qualify
                        for election if it is the subject of a petition signed manually or
                        electronically by a number of registered voters, to be specified by the
                        Electoral Trust, within the relevant government jurisdiction. The time
                        period allotted to gather qualifying petition signatures shall be not
                        more than two years, beginning on the date the first signature is
                        collected.</p>
                        <p> <strong>2) Public Opinion Poll of Citizens. </strong><br>
                          An initiative
                          shall qualify for election if the subject matter described in the title
                          and summary is approved in a public opinion poll. To qualify by this
                          method, the polling plan, including the number of respondents, the
                          methodology and the entity that will conduct the poll, shall be
                          approved by the Electoral Trust. </p>
                        <p> <strong>3) Legislative Resolution. </strong><br>
                          An initiative shall
                          qualify for election if a resolution, the wording of which is identical
                          to the initiative as submitted by its sponsor, is passed by simple
                          majority in the legislative body of the relevant jurisdiction; except
                          that, if the initiative proposes to create or alter a constitution or
                          charter, such resolution must pass by a two-thirds majority. </p>
                      </ul>
                      <p> <strong>G. Withdrawal. </strong><br>
                        The Sponsor of an initiative may
                        withdraw an initiative from further consideration and processing at any
                        time prior to a deadline established by the Electoral Trust. </p>
                      <p> <strong>H. Public Hearing. </strong><br>
                        After an initiative qualifies
                        for election, the Electoral Trust shall appoint a Hearing Officer to
                        conduct a public hearing on the initiative. Representatives of the
                        Sponsor and representatives of the legislative body of the relevant
                        jurisdiction shall participate in the hearing in accordance with
                        policies and procedures established by the Electoral Trust. Testimony
                        on the initiative by citizens, proponents, opponents, and experts shall
                        be solicited and their testimony shall be published as the Hearing
                        Record. </p>
                      <p> <strong>I. Deliberative Committee. </strong><br>
                        After the public
                        hearing on each initiative, the Electoral Trust shall convene a
                        Deliberative Committee to review that initiative. The Deliberative
                        Committee shall consist of citizens selected at random from the voter
                        registration rolls of the relevant jurisdiction maintained by the
                        Electoral Trust. Members of the Deliberative Committee shall be fairly
                        compensated for time spent and expenses incurred in performance of
                        Committee duties. The Electoral Trust shall provide technical support
                        and such additional resources as are necessary for the effective
                        discharge of the Committee's duties. The Deliberative Committee shall
                        review the Hearing Record, secure expert advice, deliberate the merits
                        of the initiative, and prepare a written report of its deliberations
                        and recommendations. By two-thirds vote, the Committee may alter the
                        Title, Summary, Preamble or text of the initiative, provided that the
                        changes are consistent with the  stated purpose of the initiative. </p>
                      <p> <strong>J. Legislative Advisory Vote. </strong><br>
                        Each initiative,
                        together with its Hearing Record and report of the Deliberative
                        Committee, shall be transmitted to the legislative body of the relevant
                        jurisdiction. The legislative body shall conduct a public vote of its
                        members, recording the yeas and nays on the initiative, within 90 days
                        after receipt thereof. The vote of the legislative body is non-binding,
                        serving only as an advisory to the citizens. </p>
                      <p> <strong>K. Election. </strong><br>
                        Upon completion of the Legislative
                        Advisory Vote, or 90 days after the initiative has been delivered to
                        the legislative body of the relevant jurisdiction, whichever occurs
                        first, the Electoral Trust shall publish a schedule for the election of
                        the initiative and shall conduct an election in accordance with the
                        published schedule. </p>
                      <p> <strong>L. Enactment. </strong><br>
                        An initiative that creates or
                        modifies a constitution or charter assumes the force of law when it is
                        approved by more than half the registered voters in the relevant
                        jurisdiction in each of two successive elections conducted by the
                        Electoral Trust. If such initiative is approved in the first election,
                        the second election shall occur no earlier than six months and no later
                        than a year after, the first election. An initiative that enacts,
                        modifies or repeals statute law assumes the force of law when approved
                        by more than half the registered voters participating in an election
                        conducted by the Electoral Trust in the relevant jurisdiction. </p>
                      <p> <strong>M. Effective Date. </strong><br>
                        The effective date of an
                        initiative, if not otherwise specified in the initiative, shall be
                        forty-five days after certification of its enactment by the Electoral
                        Trust. </p>
                      <p> <strong>N. Judicial Review. </strong><br>
                        No court shall have the power
                        to enjoin any initiative election except on grounds of fraud. After an
                        initiative has been enacted into statute law, courts, when requested,
                        may determine the constitutionality of the law. Courts have no power to
                        adjudicate initiatives that amend the United States Constitution. </p>
                      <p> <strong>O. Promotional Communications. </strong><br>
                        Any communication,
                        regardless of the medium through which conveyed, that promotes or
                        opposes an initiative shall conspicuously identify the person(s)
                        responsible for that communication, in a manner specified by the
                        Electoral Trust. </p>
                      <p> <strong>P. Campaign Financing. </strong><br>
                        Only United States citizens
                        may contribute funds, services or property in support of or in
                        opposition to an initiative. Contributions from corporations including,
                        but not limited to, such incorporated entities as industry groups,
                        labor unions, political parties, political action committees, organized
                        religions and associations, are specifically prohibited. Such entities
                        are also prohibited from coercing or inducing employees, clients,
                        customers, members, or any other associated persons to support or
                        oppose an initiative. Violation of these prohibitions is a felony
                        punishable by not more than one year in prison, or a fine not to exceed
                        One Hundred Thousand Dollars, or both, per instance, applied to each
                        person found guilty of the violation. </p>
                      <p> <strong>Q. Financial Disclosure. </strong><br>
                        The Electoral Trust shall
                        establish financial reporting requirements applicable to initiative
                        sponsors, proponents and opponents, with monetary thresholds
                        appropriate to the affected government jurisdiction. The Electoral
                        Trust shall make all financial reports available to the public
                        immediately upon its receipt thereof. Failure of sponsors, proponents
                        or opponents to comply with these reporting requirements shall be a
                        felony punishable by not more than one year in prison or a fine not to
                        exceed One Hundred Thousand Dollars, or both, per instance, applied to
                        each person found guilty of the violation. </p>
                    </ul>
                    <p> <strong>Section 4. UNITED STATES ELECTORAL TRUST. </strong><br>
                      The
                      Electoral Trust shall administer the Democracy Amendment and the
                      Democracy Act. The Electoral Trust shall be governed by a Board of
                      Trustees and a Director. The Electoral Trust shall take advantage of
                      contemporary technology in carrying out its mission. The activities of
                      the Electoral Trust shall be transparent to the public. </p>
                    <ul>
                      <p><strong>A. Mission. </strong><br>
                      The Electoral Trust shall impartially
                      administer the Democracy Amendment and the Democracy Act, including the
                      legislative procedures herein, so as to facilitate the exercise of the
                      citizens' legislative power. The Electoral Trust shall ensure that
                      citizens may file, qualify and vote on initiatives relevant to any
                      government jurisdiction at any time and from any location. The
                      Electoral Trust shall neither influence the outcome of any initiative,
                      nor alter the substance of any initiative, except as specified in
                      Section 3.I, "Deliberative Committee".</p>
                      <p> <strong>B. Board of Trustees. </strong><br>
                        The Board of Trustees shall establish policy for and perform oversight of the Electoral Trust. </p>
                      <ul>
                        <p><strong>1) Membership.</strong><br>
                        The Board of Trustees shall include
                        53 members: one member elected by the citizens of each of the 50
                        states, the District of Columbia, Puerto Rico and the Territories of
                        the United States.</p>
                        <p> <strong>2) Term of Office. </strong><br>
                          Members of the Board of
                          Trustees shall serve a single four year term except as follows:
                          Immediately after the first election, the members shall be divided as
                          equally as possible into two classes. The seats of the members of the
                          first class shall be vacated at the expiration of the second year; the
                          seats of the members of the second class shall be vacated at the
                          expiration of the fourth year. </p>
                        <p> <strong>3) Removal Of Trustees. </strong><br>
                          Any member of the Board of
                          Trustees shall be removed from office upon a three-fourths vote of the
                          full membership of the Board of Trustees, or by a majority of the
                          voters participating in a recall election in the political jurisdiction
                          from which the member was elected. </p>
                        <p> <strong>4) Vacancies. </strong><br>
                          A vacancy on the Board of Trustees
                          shall be filled by majority vote of the full membership of the Board of
                          Trustees on candidates who shall represent the political jurisdiction
                          of the Trustee whose seat is vacant. A member appointed to fill a
                          vacancy shall not subsequently be elected to the Board of Trustees. </p>
                        <p> <strong>5) Meetings. </strong><br>
                          The Board of Trustees shall meet at
                          least annually and at such other times and in such places as it deems
                          appropriate to conduct its business. All meetings of the Board shall be
                          publicized in advance and open to the public, except as required by
                          law. The Electoral Trust shall publish the minutes and video recordings
                          of all meetings of the Board, except as required by law. </p>
                      </ul>
                      <p> <strong>C. Interim Board.</strong><br>
                        The members of the Interim Board,
                        hereby appointed, are the highest elected official (e.g., Lieutenant
                        Governor, Secretary of State) responsible for the conduct of elections
                        from each of the fifty states and Puerto Rico and the highest official
                        responsible for the conduct of elections from the District of Columbia
                        and the Territories of the United States. The responsibility and
                        authority of this initial Board shall be confined to establishing
                        policy and oversight for the registration of each citizen of the United
                        States eligible to vote on an initiative, and establishing policy and
                        oversight for the election of the members of the Board of Trustees. </p>
                      <p> <strong>D. Director. </strong><br>
                        The Director of the Electoral Trust
                        is the Chief Executive Officer of the Electoral Trust and is
                        responsible for its day-to-day management and operations, consistent
                        with the policies established by the Board of Trustees. The Director
                        shall conduct the first election of the Board of Trustees as soon as
                        possible. </p>
                      <ul>
                        <p><strong>1) Term of Office. </strong><br>
                        The Director, except for the
                        first Director, shall be appointed by majority vote of the Board of
                        Trustees. The Director shall serve for a single term of six years. The
                        Board of Directors of Philadelphia II shall appoint the first Director.</p>
                        <p> <strong>2) Removal Of Director. </strong><br>
                          The Director shall be
                          removed from office upon a three-fourths vote of the full membership of
                          the Board of Trustees, or by a majority of the voters participating in
                          a national recall election. </p>
                        <p> <strong>3) Vacancy. </strong><br>
                          A vacancy in the position of Director shall be filled by majority vote of the full membership of the Board of Trustees . </p>
                      </ul>
                      <p> <strong>E. Oath or Affirmation of Office. </strong><br>
                        Each Member of
                        the Board of Trustees, the Interim Board, the Director and each
                        employee of the Electoral Trust shall execute the following oath or
                        affirmation of office as a condition of his or her service: "I, (name),
                        (swear or affirm) that I will, to the best of my ability, defend and
                        uphold the Constitution of the United States and the sovereign
                        authority of the People to exercise their legislative power." </p>
                      <p> <strong>F. Organization and Responsibilities. </strong><br>
                        The
                        Electoral Trust shall staff and organize itself to fulfill its mission
                        and shall develop policies, procedures and regulations to register
                        citizens upon their becoming eligible to vote, to assist sponsors in
                        preparing initiatives for qualification, to process initiatives, to
                        administer initiative elections and to administer elections and recall
                        elections of the Board of Trustees and recall elections of the
                        Director. The Electoral Trust may select and contract for facilities
                        and services,  and prescribe staff duties and compensation. The
                        Electoral Trust may also apply for and receive funds, and incur debt
                        when necessary, and shall act in a responsible manner as a fiduciary
                        agency of the People. </p>
                      <ul>
                        <p><strong>1) Existing Law. </strong><br>
                        In fulfilling its
                        responsibilities and performing its duties, the Electoral Trust shall
                        comply with applicable laws and regulations of every government
                        jurisdiction of the United States in which it operates that do not
                        conflict with its mission defined in Section 4A, "Mission". Where laws
                        are in conflict, this Democracy Act shall supersede.</p>
                        <p> <strong>2) Voter Registration. </strong><br>
                          The Electoral Trust shall
                          develop requirements, facilities and procedures for universal lifetime
                          voter registration of citizens of the United States which shall be
                          binding in elections conducted under the authority of the Democracy
                          Amendment and this Act in every government jurisdiction in which a
                          voter is, or may become, a legal resident. </p>
                        <p> <strong>3) Research and Drafting Service.</strong><br>
                          The Electoral
                          Trust shall establish and operate a legislative research and drafting
                          service to assist citizens in preparing initiatives. </p>
                        <p> <strong>4) Communication.</strong><br>
                          The Electoral Trust shall
                          establish the means, procedures and regulations to facilitate the
                          communication of timely, comprehensive, balanced, and pertinent
                          information on the subject matter of each initiative, which information
                          shall be conveyed to the citizens of the relevant jurisdiction by
                          various media, including radio, television, print, and the Internet
                          and/or other electronic media. The Electoral Trust shall establish and
                          maintain a web site for each qualified initiative that will contain, at
                          a minimum, a summary of the Hearing Record, the report of the
                          Deliberative Committee, the result of the Legislative Advisory Vote,
                          statements prepared by the Sponsor, other proponents and opponents, and
                          a balanced analysis prepared by the Electoral Trust of the pros and
                          cons of the initiative, its societal, environmental, and economic
                          implications, costs and benefits. </p>
                        <p> <strong>5) Hearings and Deliberative Committees. </strong><br>
                          The
                          Electoral Trust shall organize a Hearing to receive testimony and shall
                          convene a Deliberative Committee to deliberate on each qualified
                          initiative. The Electoral Trust shall provide or arrange for
                          professional Hearing Officers and Deliberation Facilitators, technical
                          consultants and support staff and facilities as needed for the
                          effective conduct of Hearings and Committee activities. </p>
                        <p> <strong>6) Elections </strong><br>
                          The Electoral Trust shall devise and
                          administer policies and procedures to conduct elections of initiatives,
                          of the Board of Trustees, and for the recall of any Trustee or the
                          Director. In doing so, it shall take advantage of contemporary
                          technology in developing procedures for voting and validating votes.
                          All such policies and procedures shall be neutral with respect to the
                          content of initiatives administered and the outcomes of elections
                          conducted. </p>
                      </ul>
                      <p> <strong>G. Budgets. </strong><br>
                        Budgets covering all elements of the
                        Electoral Trust's operations and activities shall be prepared and
                        published consistent with government practices and the public nature of
                        the Electoral Trust's responsibilities. </p>
                    </ul>
                    <p> <strong>Section 5. APPROPRIATIONS. </strong><br>
                      The People hereby
                      authorize the appropriation of funds from the Treasury of the United
                      States, pursuant to Article I, Section 9(7) of the United States
                      Constitution, to enable the Electoral Trust to organize itself, repay
                      debts herein described, and begin the performance of its duties. Debts
                      to be repaid under this Section are those incurred by Philadelphia II,
                      the proceeds of which were used to pay the costs of preparing for and
                      conducting the election for the enactment of the National Initiative
                      for Democracy, which costs shall include, but shall not be limited to,
                      the production cost of ballots, printing, mail, print and electronic
                      communications, including the Internet, and services in support of the
                      election conducted by Philadelphia II, and related costs such as the
                      cost of the legal defense of Philadelphia II's operations, all of which
                      shall have been audited and certified as bona fide by the Electoral
                      Trust prior to repayment. Hereafter, appropriations shall be made
                      annually to the Electoral Trust as an independent agency of the United
                      States Government. </p>
                    <p> <strong>Section 6. SEVERABILITY. </strong><br>
                      In the event that any one
                      or more of the provisions of this Act shall for any reason be held to
                      be invalid as a result of judicial action, the remaining provisions of
                      this Act shall be unimpaired. </p>
                    <p> <strong>Section 7. ENACTMENT BY THE PEOPLE. </strong> </p>
                    <ul>
                      <p><strong>A. The Ballot.</strong><br>
                      Philadelphia II shall present a
                      ballot to the citizens of the United States for their legislative
                      decision on the enactment of the National Initiative for Democracy by
                      direct contact, mail, print, Internet and/or other media. Regardless of
                      the media through which they are presented and transmitted, all ballots
                      shall provide for entry of the following information:</p>
                      <ul type-disc="">
                        <li>The voter's name.</li>
                        <li>The voter's address, including street, city, postal code, county and state of residence.</li>
                        <li>The voter's telephone number.</li>
                        <li>The voter's e-mail address.</li>
                        <li>A Yes or No vote on the National Initiative for Democracy.</li>
                        <li>The date the ballot is executed.</li>
                        <li>The voter's Identification Number (provided by Philadelphia II).</li>
                        <li>The voter's Password (provided by Philadelphia II).</li>
                        <li>The physical or electronic signature of the voter.</li>
                      </ul>
                      <p> <strong>B.The Election. </strong><br>
                        Citizens registered to vote in any
                        government jurisdiction within the United States may participate in the
                        election for the National Initiative by executing a ballot such as
                        described above and conveying it to Philadelphia II. The Amendment
                        shall have been ratified and the Democracy Act enacted when
                        Philadelphia II has received a number of affirmative votes greater than
                        half the total number of government-validated votes cast in the
                        presidential election occurring immediately prior to this election's
                        certification by the President of Philadelphia II to the government of
                        the United States, provided that the number of affirmative votes
                        exceeds the number of negative votes received by Philadelphia II at
                        that time. </p>
                    </ul>
                    <p><strong>Section 8. DEFINITIONS</strong></p>
                    <ul>
                      <p><strong>Administer</strong><br>
                      Plan, manage and execute the operations of an organization in accordance with
                      governing policy, organizational regulations and pertinent constitutional and
                      statute law.</p>
                      <p> <strong>Appropriation</strong><br>
                        A legislative act of the U.S. House of Representatives transferring public
                        funds from the United States Treasury, in accordance with Article I, Section
                        9(7) of the Constitution. </p>
                      <p> <strong>Authorize (an appropriation)</strong><br>
                        A legislative act to empower or give necessary authority to make an
                        appropriation of public funds from the United States Treasury. </p>
                      <p> <strong>Ballot</strong><br>
                        A document listing alternatives to be voted on or questions to be answered,
                        along with other pertinent information. In this context, the ballot requests a
                        simple "Yes" or "No" vote on the National Initiative for Democracy, plus
                        information allowing verification of the voter's registration status together
                        with data that can be used to contact the voter to confirm that his or her
                        vote was accurately recorded. </p>
                      <p> <strong>Budget</strong><br>
                        An itemized summary of anticipated income and intended expenditures for a
                        specified period of time. </p>
                      <p> <strong>Campaign</strong><br>
                        An operation or related set of operations pursued to accomplish a political
                        purpose. In this context it refers to all of the activities conducted by any
                        citizen or group of citizens together with all the resources applied by them
                        to the goal of enacting or defeating an initiative. </p>
                      <p> <strong>Certify/certification</strong><br>
                        To confirm formally as to truth, accuracy, or genuineness; to guarantee as
                        having met a standard. In this context: </p>
                      <ul type-disc="">
                        <li>Citizens who vote to enact the National Initiative will certify
                          their status as registered voters; and</li>
                        <li>the Electoral Trust will certify the results of an election as
                          being true and accurate, and having conformed to the law governing initiative
                          elections; and</li>
                        <li>Philadelphia II will certify to the United States government that,
                          as a result of a national election, the Democracy Amendment and the Democracy
                          Act are the law of the land.</li>
                      </ul>
                      <p> <strong>Charter</strong><br>
                        A document that has been ratified by the people effected to establish and
                        define the fundamental powers and privileges of a governing body for a
                        municipality, county or other corporation.. </p>
                      <p> <strong>Chief Executive Officer</strong><br>
                        The executive with responsibility and authority to plan, manage and conduct
                        the operations of an organization; including the appointment of subordinate
                        managers, hiring of employees, contracting for services, and undertaking or
                        overseeing all other activities necessary to fulfill the mission of the
                        organization subject to policies and guidelines established by the governing
                        board of the organization or other superior authority. </p>
                      <p> <strong>Citizen</strong><br>
                        A person entitled by birth or naturalization to the protection of a state or
                        nation; in particular, one entitled to vote and enjoy other privileges. </p>
                      <p> <strong>Coerce</strong><br>
                        To force to act in a certain way by use of pressure, threats, or intimidation. </p>
                      <p> <strong>Deliberation Facilitator</strong><br>
                        A professional in group processes and the effective conduct of meetings who is
                        made available by the Electoral Trust to assist the citizen members of a
                        Deliberative Committee in the conduct of their deliberations. </p>
                      <p> <strong>Election</strong><br>
                        In this context, the entire process, and the infrastructure supporting that
                        process, by which votes are cast and tabulated to determine whether or not an
                        initiative has been approved or rejected by the voters; or the process by
                        which votes are cast and tabulated to determine the membership of the Board of
                        Trustees of the Electoral Trust.. </p>
                      <p> <strong>File (an initiative)</strong><br>
                        An initiative is filed when the Sponsor submits the initiative to the
                        Electoral Trust for approval of its Title and Summary. </p>
                      <p> <strong>Government (local, state or national)</strong><br>
                        A governing body that is defined by and draws its authority from a
                        constitution or charter.</p>
					  <p><strong>Government Jurisdiction</strong><br>
                        A geographic area subject to governance by a legislative body. In this
                        context, national, state, county or equivalent (e.g., parish), municipality
                        (e.g., cities and towns), commonwealth (i.e., Puerto Rico) and Trust Territory
                        (i.e., American Samoa, Guam and Virgin Islands), plus the District of Columbia
                        -- are the specific jurisdictions referred to and included under the Democracy
                        Act. </p>
                      <p> <strong>Induce</strong><br>
                        In this context, to lead or move, as to a course of action, by promise of
                        reward or consideration. </p>
                      <p> <strong>Initiative</strong><br>
                        The legislative instrument chosen by the voting citizens of the United States
                        to exercise their inherent power to enact or modify any governmental policy,
                        law, charter, or constitution; as set forth in the Democracy Amendment to the
                        U.S. Constitution. </p>
                      <p> <strong>Initiative Process</strong><br>
                        Infrastructure and procedures by which legislation may be introduced and
                        enacted directly by the people. </p>
                      <p> <strong>Jurisdiction</strong><br>
                        See "Government Jurisdiction." </p>
                      <p> <strong>Lawmaker/Legislator</strong><br>
                        One who makes or enacts laws. In this context, either a member of an elected
                        legislative body such as Congress, a state legislature, or a city council; or
                        a citizen eligible to vote in the Legislature of the People. </p>
                      <p> <strong>Legislation</strong><br>
                        A legislative resolution or statute law produced by a legislature. </p>
                      <p> <strong>Legislative Advisory Vote.</strong><br>
                        A legally non-binding vote required by the Democracy Act to be taken by the
                        legislative body of the government jurisdiction affected by an initiative, in
                        which the members of the elected legislature publicly vote yea or nay on the
                        initiative. Serves as an advisory or cue to the citizens. </p>
                      <p> <strong>Legislative Body</strong><br>
                        An elected group of individuals having the power to create, amend and repeal
                        laws together with the policies, procedures and infrastructure established by
                        and under a governing constitution or charter. </p>
                      <p> <strong>Legislative Resolution</strong><br>
                        A formal expression of the opinion or "will" of a legislative body. </p>
                      <p> <strong>Legislature</strong><br>
                        An officially elected or otherwise selected body of people vested with the
                        responsibility and power to make laws for a political unit, such as a state or
                        nation. </p>
                      <p> <strong>Legislature of the People</strong><br>
                        The body of citizens who are eligible to vote in an election conducted by the
                        Electoral Trust, which administers the policies, procedures and infrastructure
                        established by and under the authority of the Democracy Amendment and the
                        Democracy Act. </p>
                      <p> <strong>National Initiative</strong><br>
                        Short title for the National Initiative For Democracy. </p>
                      <p> <strong>National Initiative for Democracy</strong><br>
                        The Democracy Amendment to the Constitution of the United States and the
                        Democracy Act, packaged together for concurrent presentation to the citizens
                        of the United States in a national election to be conducted by Philadelphia
                        II. </p>
                      <p> <strong>Opponent (of an initiative)</strong><br>
                        Any person who attempts, by any action, including but not limited to the
                        contribution of funds, services, or other resources to be used for the
                        creation or dissemination of information, to advocate that a qualified
                        initiative be defeated at election. </p>
                      <p> <strong>Petition</strong><br>
                        In this context, a document in which registered voters indicate that they wish
                        an initiative to be qualified for election. Petitions may be hard copy or
                        electronic documents, and may be signed manually or electronically. </p>
                      <p> <strong>Philadelphia II</strong><br>
                        The nonprofit corporation conducting the election for the National Initiative. </p>
                      <p> <strong>Poll</strong><br>
                        In this context, a validated sampling of registered voters in which the
                        respondents indicate whether or not they wish an initiative to be qualified
                        for election. </p>
                      <p> <strong>Polling Plan</strong><br>
                        A document that describes the number and source of respondents; the method by
                        which the respondents for a poll will be drawn; how the data will be
                        collected, tabulated and presented; and how the question(s) on the poll will
                        be worded. The Electoral Trust may require a polling plan to include such
                        additional information as will permit it to carry out its responsibility to
                        determine if the planned poll will accurately reflect the views of the
                        citizens in the government jurisdiction affected by the initiative addressed
                        by the proposed poll. </p>
                      <p> <strong>Proponent (of an initiative)</strong><br>
                        Any person who attempts, by any action, including but not limited to the
                        contribution of funds, services, or other resources to be used for the
                        creation or dissemination of information, to advocate that a qualified
                        initiative be enacted at election. </p>
                      <p> <strong>Qualify (an initiative)</strong><br>
                        To qualify for election an initiative must meet criteria established by the
                        Democracy Act, thereby enabling the Electoral Trust to begin the processing of
                        the initiative that leads to its enactment or defeat in an election by
                        registered voters. </p>
                      <p> <strong>Ratify</strong><br>
                        An act of approval by a sovereign authority. </p>
                      <p> <strong>Registered Voter</strong><br>
                        In this context, any citizen of the United States who is at least 18 years
                        old, who has registered once in his or her lifetime, is not imprisoned for a
                        felony, and who has not been classified as "incompetent" by a court, provided
                        that he or she has not renounced or otherwise given up United States
                        citizenship. </p>
                      <p> <strong>Signature, Electronic</strong><br>
                        "Electronic signature" is a generic, technology-neutral term that refers to
                        the result of any of the various methods by which one can "sign" an electronic
                        document. Examples of electronic signatures include: a digitized image of a
                        handwritten signature, a secret code or personal identification number (PIN)
                        (such as are used with ATM cards and credit cards) or a unique
                        biometrics-based identifier, such as a fingerprint or a retinal scan. The
                        Electoral Trust will specify and/or implement electronic signature technology
                        to be used by voters who choose to submit ballots signed electronically. </p>
                      <p> <strong>Signature, Manual</strong><br>
                        A person's name or equivalent mark written in the person's own handwriting. </p>
                      <p> <strong>Sovereign</strong><br>
                        When used as a noun: one who, singly or in company with others, possesses
                        supreme authority in a nation or other governmental unit. When used as an
                        adjective: self-governing; independent; possessing highest authority and
                        jurisdiction. </p>
                      <p> <strong>Sponsor</strong><br>
                        A person, or a group of individually identified people, responsible for the
                        submission of an initiative to the Electoral Trust for qualification and
                        processing. </p>
                      <p> <strong>Statute Law</strong><br>
                        An enactment by a legislative body, e.g., laws, resolutions and ordinances. </p>
                      <p> <strong>The People of the United States</strong><br>
                        The introductory phrase of the Democracy Act begins with the phrase "Be It
                        Enacted By The People Of The United States." In this context the term "People
                        of the United States" is used for consistency with our Constitution and
                        Declaration of Independence. </p>
                      <p> <strong>Transparency, Transparent</strong><br>
                        In general usage: free from guile; candid, open and easily understood. In this
                        context, the term "transparent" refers to the fact that the workings and
                        products of the Electoral Trust are to be continuously public; that is, open
                        to inspection and review by the citizenry except as may be required by law.</p>
                      <p>&nbsp;</p>
                    </ul>
                  </blockquote></td>
              </tr>
              <tr>
                <td><table align="center">
                    <tbody>
                      <tr>
                        <?php if (session_is_registered("SESSION")) { ?>
                        <td><form action="Ballot.php">
                            <input name="submit" type="submit" class="Button" value="Return to your ballot">
                          </form></td>
                        <?php } else { ?>
                        <td><form action="login.php">
                            <input name="submit2" type="submit" class="Button" value="Return to login screen">
                          </form></td>
                        <td>&nbsp;</td>
                        <td><form action="EditNewVoter.php">
                            <input name="submit2" type="submit" class="Button" value="Register">
                          </form></td>
                        <?php } ?>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
            </tbody>
          </table>
          <!-- ==============================================================	-->
          <?php include("bottom.htm"); ?>
</body>
</html>
