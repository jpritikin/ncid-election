<?
require 'vars.php';
require 'mgmail.php';

// session check
session_start();
header("Cache-control: private");
if (!session_is_registered("SESSION"))
{
	// if session check fails, invoke error handler
	header("Location: InvalidLogin.php?e=2");
	exit();
}

    if ($_POST['submit'] == "Back") {
	header("Location: EditVoterInfo.php");
	exit();
    }

$voter_id = $_SESSION['voter_id'];
$password = $_SESSION['password'];
$name = $_SESSION["name"];

function do_query($query) {
    $result = mysql_query ($query);

    $info = mysql_info();
    if (!$result || substr_count($info, "Rows matched: 1") == 0) {
	$hdrs = "From: ModifyVoterInfo.php@votep2.us\r\n";

	$msg = "mysql_error: ".mysql_error()."\n\n".mysql_info()."\n\n".$query."\n";

	mgmail("mgrant@grant.org", "Error while adding to database", $msg, $hdrs);

	echo "We're sorry, an error has occured while updating the database.  Your data has not been lost, it will be processed manually.<BR>";
	
	exit;
    }
    return 1;  // success
}

function clean($input) {
    $input = stripslashes($input);
    $input = str_replace("'", "''", $input);
    $input = str_replace("\\", "", $input);
    return($input);
}

foreach ($_POST as $key => $value) {
  $_POST[$key] = stripslashes($value);
}

if (!$voter_id ||
    !$password ||
    !$_POST['first_name'] ||
    !$_POST['last_name'] ||
    !$_POST['address_1'] ||
    !$_POST['city'] ||
    !$_POST['country'] ||
    !$_POST['postal_code'] ||
    !$_POST['email'] ||
    !$_POST['phone'] ||
    !$_POST['reg_state'] ||
    !$_POST['birth_city'] ||
    !$_POST['birth_country'] ||
    !$_POST['birth_date'] ||
    !$_POST['question'] ||
    !$_POST['response'])
{
  // if any of the required fields are missing, go back to form page.
  // this can only happen if someone is messing around.
	header("Location: EditNewVoter.php");
	exit();
}

$title = clean($_POST['title']);
$first_name = clean($_POST['first_name']);
$middle_name = clean($_POST['middle_name']);
$last_name = clean($_POST['last_name']);
$suffix = clean($_POST['suffix']);
$address_1 = clean($_POST['address_1']);
$address_2 = clean($_POST['address_2']);
$address_3 = clean($_POST['address_3']);
$city = clean($_POST['city']);
$state = clean($_POST['state']);
$country = clean($_POST['country']);
$postal_code = clean($_POST['postal_code']);
$email = clean($_POST['email']);
$phone = clean($_POST['phone']);
$keep_informed = $_POST['keep_informed'] ? "Yes" : "No";
$reg_county = clean($_POST['reg_county']);
$reg_state = clean($_POST['reg_state']);
$registered = $_POST['not_registered']=="true" ? "No" : "Yes";
$birth_city = clean($_POST['birth_city']);
$birth_state = clean($_POST['birth_state']);
$birth_country = clean($_POST['birth_country']);
list($m,$d,$y) = explode("/", $_POST['birth_date']);
$birth_date = $y."/".$m."/".$d;
$birth_date = str_replace("'","",$birth_date);
$birth_date = str_replace("\\","",$birth_date);
$ssn = clean($_POST['ssn']);
$question = clean($_POST['question']);
$response = clean($_POST['response']);
$new_password = clean($_POST['password']);

mysql_connect ($sql_host, $sql_user, $sql_pass);

mysql_select_db ($sql_db);

// Update an existing record

$query = "UPDATE $voter_table
          SET password='$new_password',title='$title',first_name='$first_name',middle_name='$middle_name',last_name='$last_name',suffix='$suffix',address_1='$address_1',address_2='$address_2',address_3='$address_3',city='$city',state='$state',country='$country',postal_code='$postal_code',email='$email',phone='$phone',keep_informed='$keep_informed',reg_county='$reg_county',reg_state='$reg_state',registered='$registered',birth_city='$birth_city',birth_state='$birth_state',birth_country='$birth_country',birth_date='$birth_date',ssn='$ssn',question='$question',response='$response'
          WHERE voter_id='$voter_id' AND password='$password'";

do_query($query);

if ($password != $new_password) {
    $_SESSION['password'] = $new_password;
}

// make sure the name matches if it got changed
$name = $title." ".$first_name." ".$middle_name." ".$last_name." ".$suffix;
$name=str_replace("  "," ",$name);
$_SESSION['name'] = $name;

?>

<HTML>
  <HEAD>
    <TITLE>
      Philadelphia II - Add Voter
    </TITLE>
    <link href="style.css" rel="stylesheet" type="text/css">
  </HEAD>
  <BODY>
    <?php include("menu-user.htm"); ?>
	<?php include("top.htm"); ?>
	      <div align="center">

	                <TABLE width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
	                  <TBODY>
	                    <TR>
	                      <TD height="30">
	                        <BR>
	                        <DIV class="c1">
	                          <STRONG>Thank you for updating your information</STRONG>
							</DIV>
			        <BR>                          </TD>
			    </TR>
	                    <TR>
	                      <TD height="30">
	                        <TABLE align="center">
	                          <TBODY>
	                            <TR>
	                              <TD align="center">
	                                <FORM id="form1" name="Return" method="post" action="Ballot.php">
	                                  <INPUT type="submit" name="Close" value="Return to Ballot" class="Button">
                                    </FORM>				      </TD>
				    </TR>
                              </TBODY>
                            </TABLE>
			        <BR>                          </TD>
			    </TR>

                      </TBODY>
                    </TABLE>
        </div>
    <?php include("bottom.htm"); ?>
  </BODY>
</HTML>

