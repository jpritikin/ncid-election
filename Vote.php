<?
require 'vars.php';
require 'mgmail.php';

// Vote.php - secure page

// session check
session_start();
header("Cache-control: private");
if (!session_is_registered("SESSION"))
{
	// if session check fails, invoke error handler
	header("Location: InvalidLogin.php?e=2");
	exit();
}

$vote = $_SESSION['vote'];
$name = $_SESSION['name'];
$email = $_SESSION['email'];
$voter_id = $_SESSION["voter_id"];
$password = $_SESSION["password"];

if (!($vote=="YES" || $vote=="NO")) {
    header("Location: Ballot.php");
    exit();
}

session_destroy();

// write $vote to database
mysql_connect ($sql_host, $sql_user, $sql_pass);

mysql_select_db ($sql_db);

$update = "UPDATE $voter_table
           SET latest_vote='$vote', date_voted=NOW()
	   WHERE voter_id='$voter_id' AND password='$password'";

$result = mysql_query ($update) or die(mysql_error());

$info = mysql_info();
if (!$result || substr_count($info, "Rows matched: 1") == 0) {
    // some error occured
    $msg = "mysql_error: ".mysql_error()."\n\n$update\n\n$info";
    $hdrs = "From: Vote.php@votep2.us\r\n";
    mgmail("mgrant@grant.org", "error updating voter", $msg, $hdrs);
}
require 'thankyoumail.php';
$msg = str_replace("%NAME%", $name, $msg);
mgmail($email, "Vote Acknowledgement", $msg, $hdrs);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
  <HEAD>
    <TITLE>
      Philadelphia II - Vote
    </TITLE>
    <link href="style.css" rel="stylesheet" type="text/css">
  </HEAD>
  <BODY>
    <?php include("menu-anon.htm"); ?>
	<?php include("top.htm"); ?>
	<h1>Thank you for voting</h1>
	
		  <TABLE width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
			<TBODY>
			  <TR>
			    <TD height="30">
			      <TABLE class="c2">
				<TBODY>
				  <tr>
				    <td>
				      <p class="c4">
					<CENTER>You are now logged out.</CENTER>
				      </p>
				    </td>
				  </tr>
				  <TR>
				    <TD>
				      <P class="c3">
					Thank you for participating in the Philadelphia II election on the National Initiative.  Your vote is secret.  Only you will know how you voted.
				      </p>
				      <p class="c3">
					You should have received your <b>voter id</b> and <b>password</b> via email.  Remember, you may log back into the site at any time and change your vote.  You are also encouraged to keep your registration information current to aid us in the manual verification phase.
				      </p>
				      <p class="c3">
					 Help us by giving us your views on the National Initiative in an exit poll.  The information you give us in the poll is anonymous and in no way tied to your name or identity.
				      </p>
				    </TD>
				  </TR>
				  <TR>
				    <TD align="center">
				      <TABLE bordercolor="#000066">
				        <TBODY>
					  <TR>
					    <TD class="Button">
					      <A href="Poll_Questionaire.php">Participate in our Exit Poll</A>
					    </TD>
 					  </TR>
					</TBODY>
				      </TABLE>
				    </TD>
				  </TR>
				</TBODY>
			      </TABLE>
			      <BR>
			    </TD>
			  </TR>
			</TBODY>
		  </TABLE>

<!-- Google Code for voted Conversion Page -->
<script language="JavaScript" type="text/javascript">
<!--
var google_conversion_id = 1032607761;
var google_conversion_language = "en_US";
var google_conversion_format = "1";
var google_conversion_color = "ffffff";
var google_conversion_label = "47OHCLuckAEQkbCx7AM";
//-->
</script>
<script language="JavaScript" src="https://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<img height="1" width="1" border="0" src="https://www.googleadservices.com/pagead/conversion/1032607761/?label=47OHCLuckAEQkbCx7AM&amp;guid=ON&amp;script=0"/>
</noscript>

          <?php include("bottom.htm"); ?>
  </BODY>
</HTML>

