<?
    require 'vars.php';

    header("Cache-control: private");

    function clean($input) {
	$input = stripslashes($input);
	$input = str_replace("'", "''", $input);
	$input = str_replace("\\", "", $input);
	return($input);
    }

    $title = clean($_POST['title']);
    $first_name = clean($_POST['first_name']);
    $middle_name = clean($_POST['middle_name']);
    $last_name = clean($_POST['last_name']);
    $suffix = clean($_POST['suffix']);
    $address_1 = clean($_POST['address_1']);
    $address_2 = clean($_POST['address_2']);
    $address_3 = clean($_POST['address_3']);
    $city = clean($_POST['city']);
    $state = strtoupper(clean($_POST['state']));
    $country = clean($_POST['country']);
    $postal_code = clean($_POST['postal_code']);
    $email = clean($_POST['email']);
    $phone = clean($_POST['phone']);
    $keep_informed = $_POST['keep_informed'] ? "Yes" : "No";
    $reg_county = clean($_POST['reg_county']);
    $reg_state = strtoupper(clean($_POST['reg_state']));
    $registered = $_POST['not_registered']=="true" ? "No" :"Yes";
    $birth_city = clean($_POST['birth_city']);
    $birth_state = strtoupper(clean($_POST['birth_state']));
    $birth_country = strtoupper(clean($_POST['birth_country']));
    list($m,$d,$y) = explode("/", $_POST['birth_date']);
    $birth_date = $y."/".$m."/".$d;
    $birth_date = str_replace("'","",$birth_date);
    $birth_date = str_replace("\\","",$birth_date);
    $question = clean($_POST['question']);
    $response = clean($_POST['response']);
    $new_password = clean($_POST['password']);

    // test form in case javascript is disabled
    $form_good = 1;
    $error_msg = "";
    if (!$first_name) {
	$error_msg .= "No first name.<BR>";
	$form_good = 0;
    }
    if (!$last_name) {
	$error_msg .= "No last name.<BR>";
	$form_good = 0;
    }
    if (!$address_1) {
	$error_msg .= "Non address.<BR>";
	$form_good = 0;
    }
    if (!$city) {
	$error_msg .= "No city.<BR>";
	$form_good = 0;
    }
    if ($state=="0") {
	$error_msg .= "No state.<BR>";
	$form_good = 0;
    }
    if (!preg_match('/^[^@]+@[^@.]+([.][^@.]+)+$/', $email)) {
	$error_msg .= "Invalid email address. Address must be like user@domain.name<BR>";
	$form_good = 0;
    }
    if (!preg_match('/^[0-9.\-+*#( )]{10,}$/', $phone)) {
	$error_msg .= "Invalid phone number with areacode and/or country code, enter yours like: 202-555-1212 or +33.98.78.54.32.10<BR>";
	$form_good = 0;
    }
    if ($reg_state=="0") {
	$error_msg .= "Missing state you are registered to vote in (if not registered, enter the state you live in.)<BR>";
	$form_good = 0;
    }
    if (!$birth_city) {
	$error_msg .= "No city or town where you were born.<BR>";
	$form_good = 0;
    }
    if (!$birth_state && ($birth_country=="USA" || $birth_country=="US")) {
	$error_msg .= "No US State where you were born.<BR>";
	$form_good = 0;
    }
    if (($birth_country=="USA" || $birth_country=="US") &&
	   ($birth_state != "AA") &&
	   ($birth_state != "AE") &&
	   ($birth_state != "AL") &&
	   ($birth_state != "AK") &&
	   ($birth_state != "AP") &&
	   ($birth_state != "AZ") &&
	   ($birth_state != "AR") &&
	   ($birth_state != "CA") &&
	   ($birth_state != "CO") &&
	   ($birth_state != "CT") &&
	   ($birth_state != "DE") &&
	   ($birth_state != "DC") &&
	   ($birth_state != "FL") &&
	   ($birth_state != "GA") &&
	   ($birth_state != "HI") &&
	   ($birth_state != "ID") &&
	   ($birth_state != "IL") &&
	   ($birth_state != "IN") &&
	   ($birth_state != "IA") &&
	   ($birth_state != "KS") &&
	   ($birth_state != "KY") &&
	   ($birth_state != "LA") &&
	   ($birth_state != "ME") &&
	   ($birth_state != "MD") &&
	   ($birth_state != "MA") &&
	   ($birth_state != "MH") &&
	   ($birth_state != "MI") &&
	   ($birth_state != "MN") &&
	   ($birth_state != "MS") &&
	   ($birth_state != "MO") &&
	   ($birth_state != "MT") &&
	   ($birth_state != "NE") &&
	   ($birth_state != "NV") &&
	   ($birth_state != "NH") &&
	   ($birth_state != "NJ") &&
	   ($birth_state != "NM") &&
	   ($birth_state != "NY") &&
	   ($birth_state != "NC") &&
	   ($birth_state != "ND") &&
	   ($birth_state != "OH") &&
	   ($birth_state != "OK") &&
	   ($birth_state != "OR") &&
	   ($birth_state != "PA") &&
	   ($birth_state != "PW") &&
	   ($birth_state != "RI") &&
	   ($birth_state != "SC") &&
	   ($birth_state != "SD") &&
	   ($birth_state != "TN") &&
	   ($birth_state != "TX") &&
	   ($birth_state != "UT") &&
	   ($birth_state != "VT") &&
	   ($birth_state != "VA") &&
	   ($birth_state != "WA") &&
	   ($birth_state != "WV") &&
	   ($birth_state != "WI") &&
	   ($birth_state != "WY") &&
	   ($birth_state != "AS") &&
	   ($birth_state != "FM") &&
	   ($birth_state != "GU") &&
	   ($birth_state != "MP") &&
	   ($birth_state != "PR") &&
	   ($birth_state != "VI") ) {
	$error_msg .= "Invalid state, enter a valid 2-letter US state abbreviation.<BR>";
	$form_good = 0;
    }
    if (!preg_match('/^\d?\d\/\d?\d\/\d\d\d\d$/', $_POST['birth_date'])) {
	$error_msg .= "Invalid birth date, enter yours like mm/dd/yyyy numerically<BR>";
	$form_good = 0;
    }
    if ($question=="") {
	if ($_POST['selQuestion']=="0") {
	    $error_msg .= "No secret question.<BR>";
	    $form_good = 0;
	} else {
	    switch($_POST['selQuestion']) {
		case "1": $question = "Favorite pet";
		break;
		case "2"; $question = "Favorite drink";
		break;
		case "3"; $question = "Favorite show";
		break;
		case "4"; $question = "Mother's maiden name";
		break;
		case "5"; $question = "Place where you grew up";
		break;
		case "6"; $question = "Childhood best friend";
		break;
		case "7"; $question = "A password or phrase";
		break;
		$error_msg .= "Please select a valid secret question.<BR>";
		$form_good = 0;
	    }
	}
    }
    if (!$response) {
	$error_msg .= "No response to your secret question.<BR>";
	$form_good = 0;
    }

    if ($form_good) {
	mysql_connect ($sql_host, $sql_user, $sql_pass);

	mysql_select_db ($sql_db);

	$result = mysql_query ("SELECT voter_id,password
				FROM $voter_table
				WHERE first_name='$first_name' AND last_name='$last_name' AND birth_city='$birth_city' AND birth_date='$birth_date'") or die(mysql_error());
	if (mysql_num_rows($result)) {
		$found = 1;
	}

	mysql_free_result($result);
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
    <TITLE>
      Philadelphia II - Confirm Your Registration    </TITLE>
    <SCRIPT language="JavaScript" type="text/javascript">

var submitted = false

function GoBack()
{
    document.frmMemberInfo.action = "";

    if ( ! submitted )
    {
        submitted = true;
        document.frmMemberInfo.action = "<?= $_POST['reedit_uri'] ?>";
        return true;
    }
    return false;
}

function GoFoward()
{
    document.frmMemberInfo.action = "";

    if ( ! submitted )
    {
        submitted = true;
        document.frmMemberInfo.action = "AddNewVoter.php";
	document.frmMemberInfo.confirm.value = "yes";
        return true;
    }
    return false;
}

    </SCRIPT>
    <STYLE type="text/css">
    <--
      table.c1 {text-align: center}
      .Button {color: #000066; font-size: 150%; font-weight: bold; background-color: white; border: outset #000066 }
.copywrite {color: #000000; font-family: Verdana; font-size: 7pt; margin-left: 10;
		     margin-right: 10 }
    -->
    </STYLE>
    <link href="style.css" rel="stylesheet" type="text/css">
  </HEAD>
  <BODY>
    <?php include("menu-anon.htm"); ?>
    <?php include("top.htm"); ?>

      <FORM method="post" id="frmMemberInfo" name="frmMemberInfo" action="AddNewVoter.php">
	<?
	  foreach ($_POST as $key => $value) {
	    echo "        <input type=\"hidden\" id=\"$key\" name=\"$key\" value=\"".stripslashes($value)."\">\n";
	  }
	  echo "      <input type=\"hidden\" id=\"confirm\" name=\"confirm\" value=\"no\">\n";
	?>

		    <? if (!$form_good) { ?>
			  <h3>Your registration information contained the following errors:</h3>
			<FONT color="red">
		          <? echo $error_msg; ?>
			</FONT>
			<p>Please press the "Back" button in your browser to return to the previous page and then fix the errors before continuing.</p>
		    <? } else { ?>
			    <h1>Confirm Your Registration</h1>
			<TABLE class="c1">
			  <TBODY>
			    <TR>
			      <TD colspan="3" width="80%" align="left">
				<? if ($found) { ?>
				<p>You appear to have already registered.  If you would like to have your password emailed to you, click on the below button.<p>
				<CENTER>
				  <INPUT type="submit" name="submit" value="Mail me my password" class="Button"><BR><BR>
				</CENTER>
				<p>If you are sure you have not registered, continue to register.  Please note that since we must validate every registered voter, if you register and vote more than once, only one of your votes will count.  If you have any questions or need help, please email us at <A href="mailto:info@philadelphiaii.us">info@philadelphiatwo.org.<A>
				<? } ?>
				<p>Please confirm that your information is correct. If it is correct, click on 'Submit Information'. If it is not correct, click on 'Back' and correct the information.</p>
			      </TD>
			    </TR>
			    <TR>
			      <TD colspan="3" height="10">&nbsp;
			      </TD>
			    </TR>
			    <TR>
			      <TD colspan="3" align="left">
			        <B>Your Name and Address:<B>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left" width="180">
				Title :
			      </TD>
			      <TD align="left" width="320">
				<?= $_POST['title'] ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				First name :
			     </TD>
			      <TD align="left">
				<?= $_POST['first_name'] ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				Middle name or initial :
			      </TD>
			      <TD align="left">
				<?= $_POST['middle_name'] ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				Last name :
			      </TD>
			      <TD align="left">
				<?= $_POST['last_name'] ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				Suffix :
			      </TD>
			      <TD align="left">
				<?= $_POST['suffix'] ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				Address :
			      </TD>
			      <TD align="left">
				<?= $_POST['address_1'] ?>
			      </TD>
			    </TR>
			    <? if ($_POST['address_2']) { ?>
			      <TR>
				<TD width="50">&nbsp;
				</TD>
				<TD align="left">&nbsp;
				</TD>
				<TD align="left">
				  <?= $_POST['address_2'] ?>
				</TD>
			      </TR>
			    <? } ?>
			    <? if ($_POST['address_3']) { ?>
			      <TR>
				<TD width="50">&nbsp;
				</TD>
				<TD align="left">&nbsp;
				</TD>
				<TD align="left">
				  <?= $_POST['address_3'] ?>
				</TD>
			      </TR>
			    <? } ?>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				City :
			      </TD>
			      <TD align="left">
				<?= $_POST['city'] ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				State or Province :
			      </TD>
			      <TD align="left">
				<?= $state ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				Country :
			      </TD>
			      <TD align="left">
				<?= $_POST['country'] ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				Zip Code:
			      </TD>
			      <TD align="left">
				<?= $_POST['postal_code'] ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				Email address :
			      </TD>
			      <TD align="left">
				<?= $_POST['email'] ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				Phone :
			      </TD>
			      <TD align="left">
				<?= $_POST['phone'] ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				Keep me informed :
			      </TD>
			      <TD align="left">
				<?= $keep_informed ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD colspan="3" height="10">&nbsp;
			      </TD>
			    </TR>
			    <TR>
			      <TD colspan="3" align="left">
			        <B>Where you are registered to vote:<B>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				County or parish of registration :
			      </TD>
			      <TD align="left">
				<?= $_POST['reg_county'] ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				State or Territory of registration :
			      </TD>
			      <TD align="left">
				<?= $reg_state ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				Registered to Vote :
			      </TD>
			      <TD align="left">
				<? if ($_POST['not_registered']=="true") { echo "No"; } else { echo "Yes"; } ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD colspan="3" height="10">&nbsp;
			      </TD>
			    </TR>
			    <TR>
			      <TD colspan="3" align="left">
			        <B>Additional information used to identify you:<B>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				Place of birth:
			      </TD>
			      <TD align="left">
				<?= $_POST['birth_city'] ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				State of birth:
			      </TD>
			      <TD align="left">
				<?= $birth_state ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				Country of birth:
			      </TD>
			      <TD align="left">
				<?= $birth_country ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				Birth date :
			      </TD>
			      <TD align="left">
				<?= $_POST['birth_date'] ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				Secret Question :
			      </TD>
			      <TD align="left">
				<?= $_POST['question'] ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD width="50">&nbsp;
			      </TD>
			      <TD align="left">
				Secret Response :
			      </TD>
			      <TD align="left">
				<?= $_POST['response'] ?>
			      </TD>
			    </TR>
			  </TBODY>
			</TABLE>

			<TABLE border="0" bordercolor="green" width="550" cellpadding="0" cellspacing="0" class="putcenter">
			  <TBODY>
			    <TR>
			      <TD align="center">
				<INPUT type="submit" name="submit" value="Back" class="Button" onClick="return GoBack();">
			      </TD>
			      <TD align="center">
				<INPUT type="submit" name="submit" value="Submit Information" class="Button" onClick="return GoFoward();">
			      </TD>
			    </TR>
			  </TBODY>
			</TABLE>
		    <? } ?>
      </FORM>
    <?php include("bottom.htm"); ?>
  </BODY>
</HTML>

