<?
    require 'vars.php';
    require 'mgmail.php';

    header("Cache-control: private");

    $title = $_POST['title'] ? str_replace("'", "''", $_POST['title']) : "%";
    $first_name = $_POST['first_name'] ? str_replace("'", "''", $_POST['first_name']) : "";
    $middle_name = $_POST['middle_name'] ? str_replace("'", "''", $_POST['middle_name']) : "%";
    $last_name = $_POST['last_name'] ? str_replace("'", "''", $_POST['last_name']) : "";
    $suffix = $_POST['suffix'] ? str_replace("'", "''", $_POST['suffix']) : "%";
    $address_1 = $_POST['address_1'] ? str_replace("'", "''", $_POST['address_1']) : "%";
    $address_2 = $_POST['address_2'] ? str_replace("'", "''", $_POST['address_2']) : "%";
    $address_3 = $_POST['address_3'] ? str_replace("'", "''", $_POST['address_3']) : "%";
    $city = $_POST['city'] ? str_replace("'", "''", $_POST['city']) : "%";
    $state = $_POST['state'] ? strtoupper(str_replace("'", "''", $_POST['state'])) : "%";
    $country = $_POST['country'] ? str_replace("'", "''", $_POST['country']) : "%";
    $postal_code = $_POST['postal_code'] ? str_replace("'", "''", $_POST['postal_code']."%") : "%";
    $email = $_POST['email'] ? str_replace("'", "''", $_POST['email']) : "%";
    $phone = $_POST['phone'] ? str_replace("'", "''", $_POST['phone']) : "%";
    $reg_county = $_POST['reg_county'] ? str_replace("'", "''", $_POST['reg_county']."%") : "%";
    $reg_state = $_POST['reg_state'] ? strtoupper(str_replace("'", "''", $_POST['reg_state'])) : "%";
    $birth_city = $_POST['birth_city'] ? str_replace("'", "''", $_POST['birth_city']) : "%";
    $birth_state = $_POST['birth_state'] ? strtoupper(str_replace("'", "''", $_POST['birth_state'])) : "%";
    $birth_country = $_POST['birth_country'] ? strtoupper(str_replace("'", "''", $_POST['birth_country'])) : "%";
    if ($_POST['birth_date']) {
      list($m,$d,$y) = explode("/", $_POST['birth_date']);
      if ($y < 100) { $y = $y + 1900; }
      $m = substr("0".$m, -2);
      $d = substr("0".$d, -2);
      $birth_date = $y."-".$m."-".$d;
      $birth_date = str_replace("'","",$birth_date);
    } else {
      $birth_date = "";
    }
    $ssn = $_POST['ssn'] ? str_replace("'", "''", $_POST['ssn']) : "%";

    mysql_connect ($sql_host, $sql_user, $sql_pass);

    mysql_select_db ($sql_db);

    $result = mysql_query ("SELECT voter_id,password,email,question,response
			    FROM $voter_table
			    WHERE title LIKE '$title' AND first_name LIKE '$first_name' AND middle_name LIKE '$middle_name' AND last_name LIKE '$last_name' AND suffix LIKE '$suffix' AND city LIKE '$city' AND state LIKE '$state' AND country LIKE '$country' AND postal_code LIKE '$postal_code' AND email LIKE '$email' AND phone LIKE '$phone' AND reg_county LIKE '$reg_county' AND reg_state LIKE '$reg_state' AND birth_city LIKE '$birth_city' AND birth_state LIKE '$birth_state' AND birth_country LIKE '$birth_country' AND birth_date LIKE '$birth_date' AND ssn LIKE '$ssn'") or die(mysql_error());
    $found = mysql_num_rows($result);

    $heading = "Search Results";

    if ($found == 1) {
	$row = mysql_fetch_array($result, MYSQL_ASSOC);
	    $voter_id = $row['voter_id'];
	    $password = $row['password'];
	    $email = $row['email'];
	    $question = $row['question'];
	    $response = $row['response'];

	if ($_POST['submit'] == "Login") {
	    if ($_POST['response'] == $response) {
		header("Location: $login_url?voter_id=$voter_id&password=$password");
		exit();
	    } else {
		$heading = "Response incorrect";
	    }
	}
    }

    mysql_free_result($result);

    // Mail password
    if (($found == 1) && ($_POST['submit'] == "Search")) {
        $hdrs = "From: info@philadelphiaII.us\r\n"
	        ."Reply-To: info@philadelphiaII.us\r\n";

        $msg = "Thank you for registering to vote in the PhiladelphiaII national election on the National Initiative for Democracy. Below is your:
		    Voter ID:  $voter_id
		    Password:  $password

	    For your convenience, you may use this url:
		    $login_url?voter_id=$voter_id&password=$password

	    Click on the above link or copy and paste the Voter ID and the Password into the proper box on the login form at http://www.votep2.us

	    Be sure to save your Voter ID and Password in a safe place to protect your vote. In the event you wish to change your vote, which you can do at any time, just log in using your Voter ID and Password. Your last vote is the one that will go into the final tally.
	    ";

        mgmail($email, "Your PhiladelphiaII Voter Registration", $msg, $hdrs);
	$password_mailed = 1;
    }

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
    <TITLE>
      Philadelphia II - Registration Search Results    </TITLE>
    <SCRIPT language="JavaScript" type="text/javascript">

var submitted = false

function GoBack()
{
    document.frmMemberInfo.action = "";

    if ( ! submitted )
    {
        submitted = true;
        document.frmMemberInfo.action = "EditUserPassSearch.php";
        return true;
    }
    return false;
}

function GoFoward()
{
    document.frmMemberInfo.action = "";

    if ( ! submitted )
    {
        submitted = true;
        document.frmMemberInfo.action = "UserPassSearch.php";
	document.frmMemberInfo.confirm.value = "yes";
        return true;
    }
    return false;
}

    </SCRIPT>
    <STYLE type="text/css">
    <--
      table.c1 {text-align: center}
      .Button {color: #000066; font-size: 150%; font-weight: bold; background-color: white; border: outset #000066 }
.copywrite {color: #000000; font-family: Verdana; font-size: 7pt; margin-left: 10;
		     margin-right: 10 }
    -->
    </STYLE>
  </HEAD>
  <BODY>
    <CENTER>
      <FORM method="post" id="frmMemberInfo" name="frmMemberInfo" action="UserPassSearch.php">
	<?
	  foreach ($_POST as $key => $value) {
	    echo "        <input type=\"hidden\" id=\"$key\" name=\"$key\" value=\"".stripslashes($value)."\">\n";
	  }
	  echo "      <input type=\"hidden\" id=\"confirm\" name=\"confirm\" value=\"no\">\n";
	?>
	<TABLE border="3" bordercolor="#000000" bgcolor="#ffffff" width="550" cellpadding="0" cellspacing="0" class="c1">
	  <TBODY>
	    <TR>
	      <TD>
		<TABLE border="0" cellspacing="0" cellpadding="0">
		  <TBODY>
		    <TR>
		      <TD colspan="3">
			<IMG src="images/logo.jpg" border="0" width="647" height="108">
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10">&nbsp;
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" align="center">
			<B><?= $heading ?></B>
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10">&nbsp;
		      </TD>
		    </TR>
		    <TR>
		      <TD>
			<TABLE width="100%" class="c1">
			  <TBODY>
			    <TR>
			      <TD colspan="3" align="left">
				<? if ($found == 1) { ?>
				Your password has been sent to your email address on record.<BR>
				<BR>
				You may log in by answering your secret question:<BR><BR>
				<CENTER>
				  <?= $question ?>:
				  <INPUT type="text" size=40 id="response" name="response" value=""><BR><BR>

				  <INPUT type="submit" name="submit" value="Login" class="Button"><BR><BR>
				<? } else { if ($found == 0) { ?>
				No matches were found.  Please go back and make your search more general.<BR><BR>
				<? } else { ?>
				Multiple matches were found.  Please go back and make your search more specific.<BR><BR>
				<? } ?>
			      </TD>
			    </TR>
			    <TR>
			      <TD align="center">
				<INPUT type="submit" name="submit" value="Back" class="Button" onClick="return GoBack();">
			      </TD>
			    </TR>
			    <TR>
			      <TD>
				<? } ?>
				<BR>
				If you have any questions or need help, please email us at <A href="mailto:info@philadelphiaii.us">info@philadelphiatwo.org.<A><BR><BR>
			      </TD>
			    </TR>
			  </TBODY>
			</TABLE>
		      </TD>
		    </TR>
		    <TR>
		      <TD><div align="center"><span class="copywrite">&copy; <a href="http://philadelphiatwo.org">Philadelphia II</a> All rights reserved. </span> </div></TD>
		    </TR>
		  </TBODY>
		</TABLE>
	      </TD>
	    </TR>
	  </TBODY>
	</TABLE>
      </FORM>
    </CENTER>
  </BODY>
</HTML>

