<?
$hdrs = "From: <info@philadelphiaii.us>\r\n";
$msg = "Dear %NAME%

Thank you for voting in the election of the National Initiative
for Democracy.

Please save the email we sent with your username and password should
you care to revisit your ballot before the election closes or to
update your information.

If you wish to make a tax-deductible contribution to help fund this
effort, The Democracy Foundation, 501(c)(3) a non-profit orginization
is accepting donations.

Web site: https://demofound.org/donate.htm

";
?>
