<?
require 'vars.php';

// session check
session_start();
header("Cache-control: private");
if (!session_is_registered("SESSION"))
{
	// if session check fails, invoke error handler
	header("Location: InvalidLogin.php?e=2");
	exit();
}

$voter_id = $_SESSION["voter_id"];
$password = $_SESSION["password"];
$name = $_SESSION["name"];

if ($_POST['submit'] == "Back") {
    // we're coming back here to fix errors
    // don't reread data from database
	$title = $_POST['title'];
	$first_name = $_POST['first_name'];
	$middle_name = $_POST['middle_name'];
	$last_name = $_POST['last_name'];
	$suffix = $_POST['suffix'];
	$address_1 = $_POST['address_1'];
	$address_2 = $_POST['address_2'];
	$address_3 = $_POST['address_3'];
	$city = $_POST['city'];
	$state = $_POST['state'];
	$country = $_POST['country'];
	$postal_code = $_POST['postal_code'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$sel_keep_informed = ($_POST['keep_informed'] == "true") ? "checked" : "";
	$reg_county = $_POST['reg_county'];
	$reg_state = $_POST['reg_state'];
	$sel_not_registered = ($_POST['not_registered']=="true") ? "checked" : "";
	$birth_city = $_POST['birth_city'];
	$birth_state = $_POST['birth_state'];
	$birth_country = $_POST['birth_country'];
	$birth_date = $_POST['birth_date'];
	$question = $_POST['question'];
	$response = $_POST['response'];

} else {
    // read user's data from database
    mysql_connect ($sql_host, $sql_user, $sql_pass);

    mysql_select_db ($sql_db);

    $result = mysql_query ("SELECT *
			    FROM $voter_table
			    WHERE voter_id='$voter_id' and password='$password'") or die(mysql_error());

    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
	$title = $row['title'];
	$first_name = $row['first_name'];
	$middle_name = $row['middle_name'];
	$last_name = $row['last_name'];
	$suffix = $row['suffix'];
	$address_1 = $row['address_1'];
	$address_2 = $row['address_2'];
	$address_3 = $row['address_3'];
	$city = $row['city'];
	$state = $row['state'];
	$country = $row['country'];
	$postal_code = $row['postal_code'];
	$email = $row['email'];
	$phone = $row['phone'];
	$sel_keep_informed = ($row['keep_informed'] == "yes") ? "checked" : "";
	$reg_county = $row['reg_county'];
	$reg_state = $row['reg_state'];
	$sel_not_registered = ($row['registered'] == "no") ? "checked" : "";
	$birth_city = $row['birth_city'];
	$birth_state = $row['birth_state'];
	$birth_country = $row['birth_country'];
	$birth_date = substr($row['birth_date'],5,2)."/".substr($row['birth_date'],8,2)."/".substr($row['birth_date'],0,4);
	$question = $row['question'];
	$response = $row['response'];
    }

    mysql_free_result($result);
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<SCRIPT language="JavaScript" type="text/javascript">

function check_date(field){
  var checkstr = "0123456789";
  var DateField = field;
  var Datevalue = "";
  var DateTemp = "";
  var separator = ".";
  var seperator = "/";
  var month;
  var day;
  var year;
  var leap = 0;
  var err = 0;
  var i;
  err = 0;
  DateValue = DateField.value;
  var x = DateValue;
  var arr = x.split("/");

  if ( arr.length == 3){
        if (DateValue.length != 10){

                if (arr[0].length != 2 ){
                    arr[0]= "0" + arr[0];
                    }
                if (arr[1].length != 2 ){
                    arr[1]= "0" + arr[1];
                    }
                if (arr[2].length == 2 ){
                    arr[2]= "19" + arr[2];
                    }

                DateValue = arr[0] + "/" + arr[1] + "/" + arr[2] ;
           }
         }
   /* Delete all chars except 0..9 */
   for (i = 0; i < DateValue.length; i++) {
      if (checkstr.indexOf(DateValue.substr(i,1)) >= 0) {
         DateTemp = DateTemp + DateValue.substr(i,1);
      }
   }
   DateValue = DateTemp;
   /* Always change date to 8 digits - string*/
   /* if year is entered as 2-digit / always assume 20xx */
  /* if (DateValue.length == 6) {
      DateValue = DateValue.substr(0,4) + '19' + DateValue.substr(4,2); }
   */

   if (DateValue.length != 8) {
   alert("Please enter your birth date in mm/dd/yyyy format.");
      DateField.select();
      DateField.focus();
      return false;
      //err = 19;
      }
   /* year is wrong if year = 0000 */
   year = DateValue.substr(4,4);
   if (year == 0) {
      err = 20;
   }

    year = DateValue.substr(4,4);
      month = DateValue.substr(0,2);
     day = DateValue.substr(2,2);

    var d = new Date();
   if ((d.getFullYear() - year < 18)){ err = 101;}

   if ((d.getFullYear() - year == 18)){
      if( parseInt(month) > (d.getMonth()+ 1)){err = 101;}
      if ( parseInt(month) == (d.getMonth()+ 1)){
        if( parseInt(day) > d.getDate()) {err = 101;}
      }
   }

   /* Validation of month*/
   if ((month < 1) || (month > 12)) {
      err = 21;
   }

   /* Validation of day*/
   if (day < 1) {
     err = 22;
   }
   /* Validation leap-year / february / day */
   if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0)) {
      leap = 1;
   }
   if ((month == 2) && (leap == 1) && (day > 29)) {
      err = 23;
   }
   if ((month == 2) && (leap != 1) && (day > 28)) {
      err = 24;
   }
   /* Validation of other months */
   if ((day > 31) && ((month == "01") || (month == "03") || (month == "05") || (month == "07") || (month == "08") || (month == "10") || (month == "12"))) {
      err = 25;
   }
   if ((day > 30) && ((month == "04") || (month == "06") || (month == "09") || (month == "11"))) {
      err = 26;
   }
   /* if 00 ist entered, no error, deleting the entry */
   if ((day == 0) && (month == 0) && (year == 00)) {
      err = 0; day = ""; month = ""; year = ""; seperator = "";
   }
   /* if no error, write the completed date to Input-Field (e.g. 13.12.2001) */

   if (err == 0) {
      DateField.value =  month + seperator + day + seperator + year;
   }
   /* Error-message if err != 0 */
   else {
     if(err == 101){
       alert("You must be at least 18 years old to participate.");
       }
     else{
      alert("Your birth date is incorrect.");
      }

      DateField.select();
      DateField.focus();
      return false;
   }
   return true;
}

function Reload()
{
    document.frmMemberInfo.action = "EditVoterInfo.php";
}

function Validate(form)
{
    if ( trim( form.first_name.value ) == "" )
    {
	alert("Your first name cannot not be left blank.\nPlease enter your first name.");
	form.first_name.focus();
        return false;
    }

    if ( trim( form.last_name.value ) == "" )
    {
        alert("Your last name cannot be left blank.\nPlease enter your last name.");
        form.last_name.focus();
        return false;
    }

    if ( trim( form.address_1.value ) == "" )
    {
        alert("The first line of your address cannot be left blank.\nPlease enter the first line of your address.");
        form.address_1.focus();
        return false;
    }

    if ( trim( form.city.value ) == "" )
    {
        alert("Your city cannot be left blank.\nPlease enter your city.");
        form.city.focus();
        return false;
    }

    form.state.value = trim (form.state.value);
    if ( ( form.state.value == "" ) &&
	 ( (form.country.value == "US") ||
	   (form.country.value == "CA") ) )
    {
        alert("Your state or province cannot be left blank.");
        form.city.focus();
        return false;
    }

    if ( (form.country.value == "US") )
    {
	form.state.value = form.state.value.toUpperCase();
    }

    if ( (form.country.value == "US") &&
	 ( (form.state.value != "AL") &&
	   (form.state.value != "AK") &&
	   (form.state.value != "AZ") &&
	   (form.state.value != "AR") &&
	   (form.state.value != "CA") &&
	   (form.state.value != "CO") &&
	   (form.state.value != "CT") &&
	   (form.state.value != "DE") &&
	   (form.state.value != "DC") &&
	   (form.state.value != "FL") &&
	   (form.state.value != "GA") &&
	   (form.state.value != "HI") &&
	   (form.state.value != "ID") &&
	   (form.state.value != "IL") &&
	   (form.state.value != "IN") &&
	   (form.state.value != "IA") &&
	   (form.state.value != "KS") &&
	   (form.state.value != "KY") &&
	   (form.state.value != "LA") &&
	   (form.state.value != "ME") &&
	   (form.state.value != "MD") &&
	   (form.state.value != "MA") &&
	   (form.state.value != "MI") &&
	   (form.state.value != "MN") &&
	   (form.state.value != "MS") &&
	   (form.state.value != "MO") &&
	   (form.state.value != "MT") &&
	   (form.state.value != "NE") &&
	   (form.state.value != "NV") &&
	   (form.state.value != "NH") &&
	   (form.state.value != "NJ") &&
	   (form.state.value != "NM") &&
	   (form.state.value != "NY") &&
	   (form.state.value != "NC") &&
	   (form.state.value != "ND") &&
	   (form.state.value != "OH") &&
	   (form.state.value != "OK") &&
	   (form.state.value != "OR") &&
	   (form.state.value != "PA") &&
	   (form.state.value != "RI") &&
	   (form.state.value != "SC") &&
	   (form.state.value != "SD") &&
	   (form.state.value != "TN") &&
	   (form.state.value != "TX") &&
	   (form.state.value != "UT") &&
	   (form.state.value != "VT") &&
	   (form.state.value != "VA") &&
	   (form.state.value != "WA") &&
	   (form.state.value != "WV") &&
	   (form.state.value != "WI") &&
	   (form.state.value != "WY") &&
	   (form.state.value != "AS") &&
	   (form.state.value != "FM") &&
	   (form.state.value != "GU") &&
	   (form.state.value != "MP") &&
	   (form.state.value != "PR") &&
	   (form.state.value != "VI") ) )
    {
        alert("Please enter a valid 2-letter US state or territory abbreviation for your current address");
        form.state.focus();
        return false;
    }

    if ( is_email_valid( form.email.value ) == false )
    {
	form.email.focus();
	return false;
    }

    if ( trim( form.phone.value ) == "" )
    {
        alert("Your Phone cannot not be left blank.");
        form.phone.focus();
        return false;
    }

    var str = form.phone.value;
    var newLength = str.length;
    var aChar;
    if(newLength < 10)
    {
	alert ("Please enter your phone number with areacode and/or country code\nFor example 202-555-1212 or +33.98.78.54.32.10");
	form.phone.focus();
	return false;
    }

    for(var i = 0; i < newLength; i++)
    {
	aChar = str.charAt(i);

	if (
	    (aChar != '1') &&
	    (aChar != '2') &&
	    (aChar != '3') &&
	    (aChar != '4') &&
	    (aChar != '5') &&
	    (aChar != '6') &&
	    (aChar != '7') &&
	    (aChar != '8') &&
	    (aChar != '9') &&
	    (aChar != '0') &&
	    (aChar != '.') &&
	    (aChar != '-') &&
	    (aChar != '+') &&
	    (aChar != '*') &&
	    (aChar != '#') &&
	    (aChar != '(') &&
	    (aChar != ' ') &&
	    (aChar != ')')
	    )
	{
	    alert ("Your phone number has invalid characters.\nPlease enter a valid phone number!");
	    form.phone.focus();
	    return false;
	}
    }

    if ( form.reg_state.value == "0" && form.not_registered.checked )
    {
        alert("Please enter the state or territory and county/parish if applicable\nwhere you reside and we will help you to register to vote.  You may\nvote in this election, but your vote will not be counted until you are\nregistered locally.");
        form.city.focus();
        return false;
    }

    if ( form.reg_state.value == "0" )
    {
        alert("Your state of voter registration cannot be left blank.\nPlease select your state.");
        form.city.focus();
        return false;
    }

    form.birth_city.value = trim (form.birth_city.value);
    if ( form.birth_city.value == "" )
    {
	alert("Your Birth city, town cannot be blank.");
	form.birth_city.focus();
	return false;
    }

    form.birth_country.value = trim (form.birth_country.value);
    form.birth_state.value = trim (form.birth_state.value);

    if ( form.birth_country.value == "" )
    {
	alert("Your country can not be blank.");
	form.birth_country.focus();
	return false;
    }

    if ( (form.birth_country.value == "US") || (form.birth_country.value == "USA") )
    {
	form.birth_state.value = form.birth_state.value.toUpperCase();
    }

    if ( ( (form.birth_country.value == "US") || (form.birth_country.value == "USA")) &&
	 ( (form.birth_state.value != "AL") &&
	   (form.birth_state.value != "AK") &&
	   (form.birth_state.value != "AZ") &&
	   (form.birth_state.value != "AR") &&
	   (form.birth_state.value != "CA") &&
	   (form.birth_state.value != "CO") &&
	   (form.birth_state.value != "CT") &&
	   (form.birth_state.value != "DE") &&
	   (form.birth_state.value != "DC") &&
	   (form.birth_state.value != "FL") &&
	   (form.birth_state.value != "GA") &&
	   (form.birth_state.value != "HI") &&
	   (form.birth_state.value != "ID") &&
	   (form.birth_state.value != "IL") &&
	   (form.birth_state.value != "IN") &&
	   (form.birth_state.value != "IA") &&
	   (form.birth_state.value != "KS") &&
	   (form.birth_state.value != "KY") &&
	   (form.birth_state.value != "LA") &&
	   (form.birth_state.value != "ME") &&
	   (form.birth_state.value != "MD") &&
	   (form.birth_state.value != "MA") &&
	   (form.birth_state.value != "MI") &&
	   (form.birth_state.value != "MN") &&
	   (form.birth_state.value != "MS") &&
	   (form.birth_state.value != "MO") &&
	   (form.birth_state.value != "MT") &&
	   (form.birth_state.value != "NE") &&
	   (form.birth_state.value != "NV") &&
	   (form.birth_state.value != "NH") &&
	   (form.birth_state.value != "NJ") &&
	   (form.birth_state.value != "NM") &&
	   (form.birth_state.value != "NY") &&
	   (form.birth_state.value != "NC") &&
	   (form.birth_state.value != "ND") &&
	   (form.birth_state.value != "OH") &&
	   (form.birth_state.value != "OK") &&
	   (form.birth_state.value != "OR") &&
	   (form.birth_state.value != "PA") &&
	   (form.birth_state.value != "RI") &&
	   (form.birth_state.value != "SC") &&
	   (form.birth_state.value != "SD") &&
	   (form.birth_state.value != "TN") &&
	   (form.birth_state.value != "TX") &&
	   (form.birth_state.value != "UT") &&
	   (form.birth_state.value != "VT") &&
	   (form.birth_state.value != "VA") &&
	   (form.birth_state.value != "WA") &&
	   (form.birth_state.value != "WV") &&
	   (form.birth_state.value != "WI") &&
	   (form.birth_state.value != "WY") &&
	   (form.birth_state.value != "AS") &&
	   (form.birth_state.value != "FM") &&
	   (form.birth_state.value != "GU") &&
	   (form.birth_state.value != "MP") &&
	   (form.birth_state.value != "PR") &&
	   (form.birth_state.value != "VI") ) )
    {
        alert("Please enter a valid 2-letter US state or territory abbreviation for your birth state");
        form.birth_state.focus();
        return false;
    }

    if ( trim(form.birth_date.value) == "" )
    {
        alert("Your birth date is incorrect.");
        form.birth_date.focus();
        return false;
    }

    if ( ! check_date(form.birth_date) )
    {
        return false;
    }

    if ( form.selQuestion.value == "0" )
    {
        alert("You must select a question.");
        form.selQuestion.focus();
        return false;
    }

    form.question.value = form.selQuestion.options[form.selQuestion.selectedIndex].text;

    form.response.value = trim( form.response.value );
    if ( form.response.value == "")
    {
        alert("You must give a response to your question.");
        form.response.focus();
        return false;
    }

//    form.reg_state.value = form.reg_state.options[form.reg_state.selectedIndex].text;

    var str = form.password.value;
    var newLength = str.length;
    var aChar;
    var alpha;
    if(newLength < 6)
    {
	alert("Your new password must be at least 6 characters\nand contain at least one non-alphabetic character.");
	return false;
    }
    var i;
    for(i = 0; i < newLength; i++)
    {
	aChar = str.charAt(i);
	if ((aChar<'a' || aChar>'z') && (aChar<'A' || aChar>'Z'))
	{
	    break;
	}
    }
    if (i == newLength) {
	alert("Your new password must be at least 6 characters\nand contain at least one non-alphabetic character.");
	return false;
    }


    form.action="ConfirmUpdateVoterInfo.php";

    return true;
}

function is_email_valid( test_email )
{
    var email = trim( test_email );

    var len = email.length;
    if ( len == 0 )
    {   /* j@e.x */
        alert("Your email address cannot not be left blank.\nPlease enter your email address. ");
        return false;
    }

    if ( len < 5 )
    {   /* j@e.x */
        alert("Your email address does not have enough characters.\nPlease enter a valid email address.");
        return false;
    }

    var pos = email.indexOf("@");
    if ( pos == -1 )
    {
        alert("Your email address does not have an @ (at) character.\nPlease enter a valid email address.");
        return false;
    }
    if ( pos == 0 )
    {
        alert("Your email address should not have the @ (at) sign as the first character.\nPlease enter a valid email address.");
        return false;
    }
    var dom = email.substr(pos+1);
    if ( (pos == len - 1) || (dom.indexOf(".") < 1) || (dom.substr(-1,1) == ".") )
    {
        alert("Your email address must have a valid domain name after the @.\nPlease enter a valid email address.");
        return false;
    }
    if ( email.indexOf("@", pos + 1) != -1 )
    {
        alert("Your email address has more than one @ (at) character.\nPlease enter a valid email address.");
        return false;
    }
    var invalid_chars = "*|,\":<>[]{}`\';()&$#%";
    for ( var i = 0; i < len; i++ )
    {
        if ( invalid_chars.indexOf(email.charAt(i)) != -1 )
        {
            alert("Your email address has invalid characters.\nPlease enter a valid email address.");
            return false;
        }
    }
}

function trim( str )
{
    var trimmed = str;
    while ( trimmed.substr(0, 1) == ' ' )
        trimmed = trimmed.substr(1);
    while ( trimmed.substr(trimmed.length - 1, trimmed.length) == ' ' )
        trimmed = trimmed.substr( 0, trimmed.length - 1 );
    return trimmed;
}


//function GoBack()
//{
//    document.frmMemberInfo.action = "SearchCriteria.php";
//}

</SCRIPT>

<HTML>
  <HEAD>
    <TITLE>Philadelphia II - Edit Voter Registration</TITLE>
    <link href="style.css" rel="stylesheet" type="text/css">
  </HEAD>
  <BODY>
    <?php include("menu-user.htm"); ?>
	<?php include("top.htm"); ?>
	<h1>Your Information</h1>
      <FORM method="post" id="frmMemberInfo" name="frmMemberInfo" action="ConfirmUpdateVoterInfo.php">

		<TABLE class="c1">
		  <TBODY>
		    <TR>
		      <TD align="left" colspan="3" height="10">This information is being collected on a secure site and
			      will  be used to validate your government registration and your right to vote.  Please see our <a href="Privacy_Policy.php">privacy policy</a>. All information will be validated manually, please be as accurate as you can to help us in this monumental effort.</TD>
		    </TR>
		      <TR>
		    <TD align="left" colspan="2">
			<B>Your name as you registered to vote:</B>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" width="250">
			Title
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="title" size="40" name="title" value="<?= $title ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right">
			* First name
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="first_name" size="40" name="first_name" value="<?= $first_name?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right">
			Middle name or initial
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="middle_name" size="40" name="middle_name" value="<?=$middle_name ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right">
			* Last name
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="last_name" size="40" name="last_name" value="<?= $last_name ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right">
			Suffix
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="suffix" size="40" name="suffix" value="<?= $suffix ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right">
			* Address
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="address_1" size="40" name="address_1" value="<?= $address_1 ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="left">
		      <TD align="left">
			<INPUT type="text" id="address_2" size="40" name="address_2" value="<?= $address_2 ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="left">&nbsp;
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="address_3" size="40" name="address_3" value="<?= $addres_3 ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right">
			* City
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="city" size="40" name="city" value="<?= $city ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right">
			* State or Territory
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="state" size="40" name="state" value="<?= $state ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right">
			* Country
		      </TD>
		      <TD align="left">
			<SELECT name="country">
			<OPTION value="US" <?= $country=="US"?"SELECTED":"" ?>>
			  UNITED STATES OF AMERICA
			</OPTION>
			<OPTION value="AF" <?= $country=="AF"?"SELECTED":"" ?>>
			  AFGHANISTAN
			</OPTION>
			<OPTION value="AL" <?= $country=="AL"?"SELECTED":"" ?>>
			  ALBANIA
			</OPTION>
			<OPTION value="DZ" <?= $country=="DZ"?"SELECTED":"" ?>>
			  ALGERIA
			</OPTION>
			<OPTION value="AS" <?= $country=="AS"?"SELECTED":"" ?>>
			  AMERICAN SAMOA
			</OPTION>
			<OPTION value="AD" <?= $country=="AD"?"SELECTED":"" ?>>
			  ANDORRA
			</OPTION>
			<OPTION value="AO" <?= $country=="AO"?"SELECTED":"" ?>>
			  ANGOLA
			</OPTION>
			<OPTION value="AI" <?= $country=="AI"?"SELECTED":"" ?>>
			  ANGUILLA
			</OPTION>
			<OPTION value="AQ" <?= $country=="AQ"?"SELECTED":"" ?>>
			  ANTARCTICA
			</OPTION>
			<OPTION value="AG" <?= $country=="AG"?"SELECTED":"" ?>>
			  ANTIGUA AND BARBUDA
			</OPTION>
			<OPTION value="AR" <?= $country=="AR"?"SELECTED":"" ?>>
			  ARGENTINA
			</OPTION>
			<OPTION value="AM" <?= $country=="AM"?"SELECTED":"" ?>>
			  ARMENIA
			</OPTION>
			<OPTION value="AW" <?= $country=="AW"?"SELECTED":"" ?>>
			  ARUBA
			</OPTION>
			<OPTION value="AU" <?= $country=="AU"?"SELECTED":"" ?>>
			  AUSTRALIA
			</OPTION>
			<OPTION value="AT" <?= $country=="AT"?"SELECTED":"" ?>>
			  AUSTRIA
			</OPTION>
			<OPTION value="AZ" <?= $country=="AZ"?"SELECTED":"" ?>>
			  AZERBAIJAN
			</OPTION>
			<OPTION value="BS" <?= $country=="BS"?"SELECTED":"" ?>>
			  BAHAMAS
			</OPTION>
			<OPTION value="BH" <?= $country=="BH"?"SELECTED":"" ?>>
			  BAHRAIN
			</OPTION>
			<OPTION value="BD" <?= $country=="BD"?"SELECTED":"" ?>>
			  BANGLADESH
			</OPTION>
			<OPTION value="BB" <?= $country=="BB"?"SELECTED":"" ?>>
			  BARBADOS
			</OPTION>
			<OPTION value="BY" <?= $country=="BY"?"SELECTED":"" ?>>
			  BELARUS
			</OPTION>
			<OPTION value="BE" <?= $country=="BE"?"SELECTED":"" ?>>
			  BELGIUM
			</OPTION>
			<OPTION value="BZ" <?= $country=="BZ"?"SELECTED":"" ?>>
			  BELIZE
			</OPTION>
			<OPTION value="BJ" <?= $country=="BJ"?"SELECTED":"" ?>>
			  BENIN
			</OPTION>
			<OPTION value="BM" <?= $country=="BM"?"SELECTED":"" ?>>
			  BERMUDA
			</OPTION>
			<OPTION value="BT" <?= $country=="BT"?"SELECTED":"" ?>>
			  BHUTAN
			</OPTION>
			<OPTION value="BO" <?= $country=="BO"?"SELECTED":"" ?>>
			  BOLIVIA
			</OPTION>
			<OPTION value="BA" <?= $country=="BA"?"SELECTED":"" ?>>
			  BOSNIA AND HERZEGOVINA
			</OPTION>
			<OPTION value="BW" <?= $country=="BW"?"SELECTED":"" ?>>
			  BOTSWANA
			</OPTION>
			<OPTION value="BV" <?= $country=="BV"?"SELECTED":"" ?>>
			  BOUVET ISLAND
			</OPTION>
			<OPTION value="BR" <?= $country=="BR"?"SELECTED":"" ?>>
			  BRAZIL
			</OPTION>
			<OPTION value="IO" <?= $country=="IO"?"SELECTED":"" ?>>
			  BRITISH INDIAN OCEAN TERRITORY
			</OPTION>
			<OPTION value="BN" <?= $country=="BN"?"SELECTED":"" ?>>
			  BRUNEI DARUSSALAM
			</OPTION>
			<OPTION value="BG" <?= $country=="BG"?"SELECTED":"" ?>>
			  BULGARIA
			</OPTION>
			<OPTION value="BF" <?= $country=="BF"?"SELECTED":"" ?>>
			  BURKINA FASO
			</OPTION>
			<OPTION value="BI" <?= $country=="BI"?"SELECTED":"" ?>>
			  BURUNDI
			</OPTION>
			<OPTION value="KH" <?= $country=="KH"?"SELECTED":"" ?>>
			  CAMBODIA
			</OPTION>
			<OPTION value="CM" <?= $country=="CM"?"SELECTED":"" ?>>
			  CAMEROON
			</OPTION>
			<OPTION value="CA" <?= $country=="CA"?"SELECTED":"" ?>>
			  CANADA
			</OPTION>
			<OPTION value="CV" <?= $country=="CV"?"SELECTED":"" ?>>
			  CAPE VERDE
			</OPTION>
			<OPTION value="KY" <?= $country=="KY"?"SELECTED":"" ?>>
			  CAYMAN ISLANDS
			</OPTION>
			<OPTION value="CF" <?= $country=="CF"?"SELECTED":"" ?>>
			  CENTRAL AFRICAN REPUBLIC
			</OPTION>
			<OPTION value="TD" <?= $country=="TD"?"SELECTED":"" ?>>
			  CHAD
			</OPTION>
			<OPTION value="CL" <?= $country=="CL"?"SELECTED":"" ?>>
			  CHILE
			</OPTION>
			<OPTION value="CN" <?= $country=="CN"?"SELECTED":"" ?>>
			  CHINA
			</OPTION>
			<OPTION value="CX" <?= $country=="CX"?"SELECTED":"" ?>>
			  CHRISTMAS ISLAND
			</OPTION>
			<OPTION value="CC" <?= $country=="CC"?"SELECTED":"" ?>>
			  COCOS (KEELING) ISLANDS
			</OPTION>
			<OPTION value="CO" <?= $country=="CO"?"SELECTED":"" ?>>
			  COLOMBIA
			</OPTION>
			<OPTION value="KM" <?= $country=="KM"?"SELECTED":"" ?>>
			  COMOROS
			</OPTION>
			<OPTION value="CG" <?= $country=="CG"?"SELECTED":"" ?>>
			  CONGO
			</OPTION>
			<OPTION value="CD" <?= $country=="CD"?"SELECTED":"" ?>>
			  CONGO, THE DEMOCRATIC REPUBLIC OF THE
			</OPTION>
			<OPTION value="CK" <?= $country=="CK"?"SELECTED":"" ?>>
			  COOK ISLANDS
			</OPTION>
			<OPTION value="CR" <?= $country=="CR"?"SELECTED":"" ?>>
			  COSTA RICA
			</OPTION>
			<OPTION value="CI" <?= $country=="CI"?"SELECTED":"" ?>>
			  C�TE D'IVOIRE
			</OPTION>
			<OPTION value="HR" <?= $country=="HR"?"SELECTED":"" ?>>
			  CROATIA
			</OPTION>
			<OPTION value="CU" <?= $country=="CU"?"SELECTED":"" ?>>
			  CUBA
			</OPTION>
			<OPTION value="CY" <?= $country=="CY"?"SELECTED":"" ?>>
			  CYPRUS
			</OPTION>
			<OPTION value="CZ" <?= $country=="CZ"?"SELECTED":"" ?>>
			  CZECH REPUBLIC
			</OPTION>
			<OPTION value="DK" <?= $country=="DK"?"SELECTED":"" ?>>
			  DENMARK
			</OPTION>
			<OPTION value="DJ" <?= $country=="DJ"?"SELECTED":"" ?>>
			  DJIBOUTI
			</OPTION>
			<OPTION value="DM" <?= $country=="DM"?"SELECTED":"" ?>>
			  DOMINICA
			</OPTION>
			<OPTION value="DO" <?= $country=="DO"?"SELECTED":"" ?>>
			  DOMINICAN REPUBLIC
			</OPTION>
			<OPTION value="EC" <?= $country=="EC"?"SELECTED":"" ?>>
			  ECUADOR
			</OPTION>
			<OPTION value="EG" <?= $country=="EG"?"SELECTED":"" ?>>
			  EGYPT
			</OPTION>
			<OPTION value="SV" <?= $country=="SV"?"SELECTED":"" ?>>
			  EL SALVADOR
			</OPTION>
			<OPTION value="GQ" <?= $country=="GQ"?"SELECTED":"" ?>>
			  EQUATORIAL GUINEA
			</OPTION>
			<OPTION value="ER" <?= $country=="ER"?"SELECTED":"" ?>>
			  ERITREA
			</OPTION>
			<OPTION value="EE" <?= $country=="EE"?"SELECTED":"" ?>>
			  ESTONIA
			</OPTION>
			<OPTION value="ET" <?= $country=="ET"?"SELECTED":"" ?>>
			  ETHIOPIA
			</OPTION>
			<OPTION value="FK" <?= $country=="FK"?"SELECTED":"" ?>>
			  FALKLAND ISLANDS (MALVINAS)
			</OPTION>
			<OPTION value="FO" <?= $country=="FO"?"SELECTED":"" ?>>
			  FAROE ISLANDS
			</OPTION>
			<OPTION value="FJ" <?= $country=="FJ"?"SELECTED":"" ?>>
			  FIJI
			</OPTION>
			<OPTION value="FI" <?= $country=="FI"?"SELECTED":"" ?>>
			  FINLAND
			</OPTION>
			<OPTION value="FR" <?= $country=="FR"?"SELECTED":"" ?>>
			  FRANCE
			</OPTION>
			<OPTION value="GF" <?= $country=="GF"?"SELECTED":"" ?>>
			  FRENCH GUIANA
			</OPTION>
			<OPTION value="PF" <?= $country=="PF"?"SELECTED":"" ?>>
			  FRENCH POLYNESIA
			</OPTION>
			<OPTION value="TF" <?= $country=="TF"?"SELECTED":"" ?>>
			  FRENCH SOUTHERN TERRITORIES
			</OPTION>
			<OPTION value="GA" <?= $country=="GA"?"SELECTED":"" ?>>
			  GABON
			</OPTION>
			<OPTION value="GM" <?= $country=="GM"?"SELECTED":"" ?>>
			  GAMBIA
			</OPTION>
			<OPTION value="GE" <?= $country=="GE"?"SELECTED":"" ?>>
			  GEORGIA
			</OPTION>
			<OPTION value="DE" <?= $country=="DE"?"SELECTED":"" ?>>
			  GERMANY
			</OPTION>
			<OPTION value="GH" <?= $country=="GH"?"SELECTED":"" ?>>
			  GHANA
			</OPTION>
			<OPTION value="GI" <?= $country=="GI"?"SELECTED":"" ?>>
			  GIBRALTAR
			</OPTION>
			<OPTION value="GR" <?= $country=="GR"?"SELECTED":"" ?>>
			  GREECE
			</OPTION>
			<OPTION value="GL" <?= $country=="GL"?"SELECTED":"" ?>>
			  GREENLAND
			</OPTION>
			<OPTION value="GD" <?= $country=="GD"?"SELECTED":"" ?>>
			  GRENADA
			</OPTION>
			<OPTION value="GP" <?= $country=="GP"?"SELECTED":"" ?>>
			  GUADELOUPE
			</OPTION>
			<OPTION value="GU" <?= $country=="GU"?"SELECTED":"" ?>>
			  GUAM
			</OPTION>
			<OPTION value="GT" <?= $country=="GT"?"SELECTED":"" ?>>
			  GUATEMALA
			</OPTION>
			<OPTION value="GN" <?= $country=="GN"?"SELECTED":"" ?>>
			  GUINEA
			</OPTION>
			<OPTION value="GW" <?= $country=="GW"?"SELECTED":"" ?>>
			  GUINEA-BISSAU
			</OPTION>
			<OPTION value="GY" <?= $country=="GY"?"SELECTED":"" ?>>
			  GUYANA
			</OPTION>
			<OPTION value="HT" <?= $country=="HT"?"SELECTED":"" ?>>
			  HAITI
			</OPTION>
			<OPTION value="HM" <?= $country=="HM"?"SELECTED":"" ?>>
			  HEARD ISLAND AND MCDONALD ISLANDS
			</OPTION>
			<OPTION value="HN" <?= $country=="HN"?"SELECTED":"" ?>>
			  HONDURAS
			</OPTION>
			<OPTION value="HK" <?= $country=="HK"?"SELECTED":"" ?>>
			  HONG KONG
			</OPTION>
			<OPTION value="HU" <?= $country=="HU"?"SELECTED":"" ?>>
			  HUNGARY
			</OPTION>
			<OPTION value="IS" <?= $country=="IS"?"SELECTED":"" ?>>
			  ICELAND
			</OPTION>
			<OPTION value="IN" <?= $country=="IN"?"SELECTED":"" ?>>
			  INDIA
			</OPTION>
			<OPTION value="ID" <?= $country=="ID"?"SELECTED":"" ?>>
			  INDONESIA
			</OPTION>
			<OPTION value="IR" <?= $country=="IR"?"SELECTED":"" ?>>
			  IRAN, ISLAMIC REPUBLIC OF
			</OPTION>
			<OPTION value="IQ" <?= $country=="IQ"?"SELECTED":"" ?>>
			  IRAQ
			</OPTION>
			<OPTION value="IE" <?= $country=="IE"?"SELECTED":"" ?>>
			  IRELAND
			</OPTION>
			<OPTION value="IL" <?= $country=="IL"?"SELECTED":"" ?>>
			  ISRAEL
			</OPTION>
			<OPTION value="IT" <?= $country=="IT"?"SELECTED":"" ?>>
			  ITALY
			</OPTION>
			<OPTION value="JM" <?= $country=="JM"?"SELECTED":"" ?>>
			  JAMAICA
			</OPTION>
			<OPTION value="JP" <?= $country=="JP"?"SELECTED":"" ?>>
			  JAPAN
			</OPTION>
			<OPTION value="JO" <?= $country=="JO"?"SELECTED":"" ?>>
			  JORDAN
			</OPTION>
			<OPTION value="KZ" <?= $country=="KZ"?"SELECTED":"" ?>>
			  KAZAKHSTAN
			</OPTION>
			<OPTION value="KE" <?= $country=="KE"?"SELECTED":"" ?>>
			  KENYA
			</OPTION>
			<OPTION value="KI" <?= $country=="KI"?"SELECTED":"" ?>>
			  KIRIBATI
			</OPTION>
			<OPTION value="KP" <?= $country=="KP"?"SELECTED":"" ?>>
			  KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF
			</OPTION>
			<OPTION value="KR" <?= $country=="KR"?"SELECTED":"" ?>>
			  KOREA, REPUBLIC OF
			</OPTION>
			<OPTION value="KW" <?= $country=="KW"?"SELECTED":"" ?>>
			  KUWAIT
			</OPTION>
			<OPTION value="KG" <?= $country=="KG"?"SELECTED":"" ?>>
			  KYRGYZSTAN
			</OPTION>
			<OPTION value="LA" <?= $country=="LA"?"SELECTED":"" ?>>
			  LAO PEOPLE'S DEMOCRATIC REPUBLIC
			</OPTION>
			<OPTION value="LV" <?= $country=="LV"?"SELECTED":"" ?>>
			  LATVIA
			</OPTION>
			<OPTION value="LB" <?= $country=="LB"?"SELECTED":"" ?>>
			  LEBANON
			</OPTION>
			<OPTION value="LS" <?= $country=="LS"?"SELECTED":"" ?>>
			  LESOTHO
			</OPTION>
			<OPTION value="LR" <?= $country=="LR"?"SELECTED":"" ?>>
			  LIBERIA
			</OPTION>
			<OPTION value="LY" <?= $country=="LY"?"SELECTED":"" ?>>
			  LIBYAN ARAB JAMAHIRIYA
			</OPTION>
			<OPTION value="LI" <?= $country=="LI"?"SELECTED":"" ?>>
			  LIECHTENSTEIN
			</OPTION>
			<OPTION value="LT" <?= $country=="LT"?"SELECTED":"" ?>>
			  LITHUANIA
			</OPTION>
			<OPTION value="LU" <?= $country=="LU"?"SELECTED":"" ?>>
			  LUXEMBOURG
			</OPTION>
			<OPTION value="MO" <?= $country=="MO"?"SELECTED":"" ?>>
			  MACAO
			</OPTION>
			<OPTION value="MK" <?= $country=="MK"?"SELECTED":"" ?>>
			  MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF
			</OPTION>
			<OPTION value="MG" <?= $country=="MG"?"SELECTED":"" ?>>
			  MADAGASCAR
			</OPTION>
			<OPTION value="MW" <?= $country=="MW"?"SELECTED":"" ?>>
			  MALAWI
			</OPTION>
			<OPTION value="MY" <?= $country=="MY"?"SELECTED":"" ?>>
			  MALAYSIA
			</OPTION>
			<OPTION value="MV" <?= $country=="MV"?"SELECTED":"" ?>>
			  MALDIVES
			</OPTION>
			<OPTION value="ML" <?= $country=="ML"?"SELECTED":"" ?>>
			  MALI
			</OPTION>
			<OPTION value="MT" <?= $country=="MT"?"SELECTED":"" ?>>
			  MALTA
			</OPTION>
			<OPTION value="MH" <?= $country=="MH"?"SELECTED":"" ?>>
			  MARSHALL ISLANDS
			</OPTION>
			<OPTION value="MQ" <?= $country=="MQ"?"SELECTED":"" ?>>
			  MARTINIQUE
			</OPTION>
			<OPTION value="MR" <?= $country=="MR"?"SELECTED":"" ?>>
			  MAURITANIA
			</OPTION>
			<OPTION value="MU" <?= $country=="MU"?"SELECTED":"" ?>>
			  MAURITIUS
			</OPTION>
			<OPTION value="YT" <?= $country=="YT"?"SELECTED":"" ?>>
			  MAYOTTE
			</OPTION>
			<OPTION value="MX" <?= $country=="MX"?"SELECTED":"" ?>>
			  MEXICO
			</OPTION>
			<OPTION value="FM" <?= $country=="FM"?"SELECTED":"" ?>>
			  MICRONESIA, FEDERATED STATES OF
			</OPTION>
			<OPTION value="MD" <?= $country=="MD"?"SELECTED":"" ?>>
			  MOLDOVA, REPUBLIC OF
			</OPTION>
			<OPTION value="MC" <?= $country=="MC"?"SELECTED":"" ?>>
			  MONACO
			</OPTION>
			<OPTION value="MN" <?= $country=="MN"?"SELECTED":"" ?>>
			  MONGOLIA
			</OPTION>
			<OPTION value="MS" <?= $country=="MS"?"SELECTED":"" ?>>
			  MONTSERRAT
			</OPTION>
			<OPTION value="MA" <?= $country=="MA"?"SELECTED":"" ?>>
			  MOROCCO
			</OPTION>
			<OPTION value="MZ" <?= $country=="MZ"?"SELECTED":"" ?>>
			  MOZAMBIQUE
			</OPTION>
			<OPTION value="MM" <?= $country=="MM"?"SELECTED":"" ?>>
			  MYANMAR
			</OPTION>
			<OPTION value="NA" <?= $country=="NA"?"SELECTED":"" ?>>
			  NAMIBIA
			</OPTION>
			<OPTION value="NR" <?= $country=="NR"?"SELECTED":"" ?>>
			  NAURU
			</OPTION>
			<OPTION value="NP" <?= $country=="NP"?"SELECTED":"" ?>>
			  NEPAL
			</OPTION>
			<OPTION value="NL" <?= $country=="NL"?"SELECTED":"" ?>>
			  NETHERLANDS
			</OPTION>
			<OPTION value="AN" <?= $country=="AN"?"SELECTED":"" ?>>
			  NETHERLANDS ANTILLES
			</OPTION>
			<OPTION value="NC" <?= $country=="NC"?"SELECTED":"" ?>>
			  NEW CALEDONIA
			</OPTION>
			<OPTION value="NZ" <?= $country=="NZ"?"SELECTED":"" ?>>
			  NEW ZEALAND
			</OPTION>
			<OPTION value="NI" <?= $country=="NI"?"SELECTED":"" ?>>
			  NICARAGUA
			</OPTION>
			<OPTION value="NE" <?= $country=="NE"?"SELECTED":"" ?>>
			  NIGER
			</OPTION>
			<OPTION value="NG" <?= $country=="NG"?"SELECTED":"" ?>>
			  NIGERIA
			</OPTION>
			<OPTION value="NU" <?= $country=="NU"?"SELECTED":"" ?>>
			  NIUE
			</OPTION>
			<OPTION value="NF" <?= $country=="NF"?"SELECTED":"" ?>>
			  NORFOLK ISLAND
			</OPTION>
			<OPTION value="MP" <?= $country=="MP"?"SELECTED":"" ?>>
			  NORTHERN MARIANA ISLANDS
			</OPTION>
			<OPTION value="NO" <?= $country=="NO"?"SELECTED":"" ?>>
			  NORWAY
			</OPTION>
			<OPTION value="OM" <?= $country=="OM"?"SELECTED":"" ?>>
			  OMAN
			</OPTION>
			<OPTION value="PK" <?= $country=="PK"?"SELECTED":"" ?>>
			  PAKISTAN
			</OPTION>
			<OPTION value="PW" <?= $country=="PW"?"SELECTED":"" ?>>
			  PALAU
			</OPTION>
			<OPTION value="PS" <?= $country=="PS"?"SELECTED":"" ?>>
			  PALESTINIAN TERRITORY, OCCUPIED
			</OPTION>
			<OPTION value="PA" <?= $country=="PA"?"SELECTED":"" ?>>
			  PANAMA
			</OPTION>
			<OPTION value="PG" <?= $country=="PG"?"SELECTED":"" ?>>
			  PAPUA NEW GUINEA
			</OPTION>
			<OPTION value="PY" <?= $country=="PY"?"SELECTED":"" ?>>
			  PARAGUAY
			</OPTION>
			<OPTION value="PE" <?= $country=="PE"?"SELECTED":"" ?>>
			  PERU
			</OPTION>
			<OPTION value="PH" <?= $country=="PH"?"SELECTED":"" ?>>
			  PHILIPPINES
			</OPTION>
			<OPTION value="PN" <?= $country=="PN"?"SELECTED":"" ?>>
			  PITCAIRN
			</OPTION>
			<OPTION value="PL" <?= $country=="PL"?"SELECTED":"" ?>>
			  POLAND
			</OPTION>
			<OPTION value="PT" <?= $country=="PT"?"SELECTED":"" ?>>
			  PORTUGAL
			</OPTION>
			<OPTION value="PR" <?= $country=="PR"?"SELECTED":"" ?>>
			  PUERTO RICO
			</OPTION>
			<OPTION value="QA" <?= $country=="QA"?"SELECTED":"" ?>>
			  QATAR
			</OPTION>
			<OPTION value="RE" <?= $country=="RE"?"SELECTED":"" ?>>
			  R�UNION
			</OPTION>
			<OPTION value="RO" <?= $country=="RO"?"SELECTED":"" ?>>
			  ROMANIA
			</OPTION>
			<OPTION value="RU" <?= $country=="RU"?"SELECTED":"" ?>>
			  RUSSIAN FEDERATION
			</OPTION>
			<OPTION value="RW" <?= $country=="RW"?"SELECTED":"" ?>>
			  RWANDA
			</OPTION>
			<OPTION value="SH" <?= $country=="SH"?"SELECTED":"" ?>>
			  SAINT HELENA
			</OPTION>
			<OPTION value="KN" <?= $country=="KN"?"SELECTED":"" ?>>
			  SAINT KITTS AND NEVIS
			</OPTION>
			<OPTION value="LC" <?= $country=="LC"?"SELECTED":"" ?>>
			  SAINT LUCIA
			</OPTION>
			<OPTION value="PM" <?= $country=="PM"?"SELECTED":"" ?>>
			  SAINT PIERRE AND MIQUELON
			</OPTION>
			<OPTION value="VC" <?= $country=="VC"?"SELECTED":"" ?>>
			  SAINT VINCENT AND THE GRENADINES
			</OPTION>
			<OPTION value="WS" <?= $country=="WS"?"SELECTED":"" ?>>
			  SAMOA
			</OPTION>
			<OPTION value="SM" <?= $country=="SM"?"SELECTED":"" ?>>
			  SAN MARINO
			</OPTION>
			<OPTION value="ST" <?= $country=="ST"?"SELECTED":"" ?>>
			  SAO TOME AND PRINCIPE
			</OPTION>
			<OPTION value="SA" <?= $country=="SA"?"SELECTED":"" ?>>
			  SAUDI ARABIA
			</OPTION>
			<OPTION value="SN" <?= $country=="SN"?"SELECTED":"" ?>>
			  SENEGAL
			</OPTION>
			<OPTION value="SC" <?= $country=="SC"?"SELECTED":"" ?>>
			  SEYCHELLES
			</OPTION>
			<OPTION value="SL" <?= $country=="SL"?"SELECTED":"" ?>>
			  SIERRA LEONE
			</OPTION>
			<OPTION value="SG" <?= $country=="SG"?"SELECTED":"" ?>>
			  SINGAPORE
			</OPTION>
			<OPTION value="SK" <?= $country=="SK"?"SELECTED":"" ?>>
			  SLOVAKIA
			</OPTION>
			<OPTION value="SI" <?= $country=="SI"?"SELECTED":"" ?>>
			  SLOVENIA
			</OPTION>
			<OPTION value="SB" <?= $country=="SB"?"SELECTED":"" ?>>
			  SOLOMON ISLANDS
			</OPTION>
			<OPTION value="SO" <?= $country=="SO"?"SELECTED":"" ?>>
			  SOMALIA
			</OPTION>
			<OPTION value="ZA" <?= $country=="ZA"?"SELECTED":"" ?>>
			  SOUTH AFRICA
			</OPTION>
			<OPTION value="GS" <?= $country=="GS"?"SELECTED":"" ?>>
			  SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS
			</OPTION>
			<OPTION value="ES" <?= $country=="ES"?"SELECTED":"" ?>>
			  SPAIN
			</OPTION>
			<OPTION value="LK" <?= $country=="LK"?"SELECTED":"" ?>>
			  SRI LANKA
			</OPTION>
			<OPTION value="SD" <?= $country=="SD"?"SELECTED":"" ?>>
			  SUDAN
			</OPTION>
			<OPTION value="SR" <?= $country=="SR"?"SELECTED":"" ?>>
			  SURINAME
			</OPTION>
			<OPTION value="SJ" <?= $country=="SJ"?"SELECTED":"" ?>>
			  SVALBARD AND JAN MAYEN
			</OPTION>
			<OPTION value="SZ" <?= $country=="SZ"?"SELECTED":"" ?>>
			  SWAZILAND
			</OPTION>
			<OPTION value="SE" <?= $country=="SE"?"SELECTED":"" ?>>
			  SWEDEN
			</OPTION>
			<OPTION value="CH" <?= $country=="CH"?"SELECTED":"" ?>>
			  SWITZERLAND
			</OPTION>
			<OPTION value="SY" <?= $country=="SY"?"SELECTED":"" ?>>
			  SYRIAN ARAB REPUBLIC
			</OPTION>
			<OPTION value="TW" <?= $country=="TW"?"SELECTED":"" ?>>
			  TAIWAN, PROVINCE OF CHINA
			</OPTION>
			<OPTION value="TJ" <?= $country=="TJ"?"SELECTED":"" ?>>
			  TAJIKISTAN
			</OPTION>
			<OPTION value="TZ" <?= $country=="TZ"?"SELECTED":"" ?>>
			  TANZANIA, UNITED REPUBLIC OF
			</OPTION>
			<OPTION value="TH" <?= $country=="TH"?"SELECTED":"" ?>>
			  THAILAND
			</OPTION>
			<OPTION value="TG" <?= $country=="TG"?"SELECTED":"" ?>>
			  TOGO
			</OPTION>
			<OPTION value="TK" <?= $country=="TK"?"SELECTED":"" ?>>
			  TOKELAU
			</OPTION>
			<OPTION value="TO" <?= $country=="TO"?"SELECTED":"" ?>>
			  TONGA
			</OPTION>
			<OPTION value="TT" <?= $country=="TT"?"SELECTED":"" ?>>
			  TRINIDAD AND TOBAGO
			</OPTION>
			<OPTION value="TN" <?= $country=="TN"?"SELECTED":"" ?>>
			  TUNISIA
			</OPTION>
			<OPTION value="TR" <?= $country=="TR"?"SELECTED":"" ?>>
			  TURKEY
			</OPTION>
			<OPTION value="TM" <?= $country=="TM"?"SELECTED":"" ?>>
			  TURKMENISTAN
			</OPTION>
			<OPTION value="TC" <?= $country=="TC"?"SELECTED":"" ?>>
			  TURKS AND CAICOS ISLANDS
			</OPTION>
			<OPTION value="TV" <?= $country=="TV"?"SELECTED":"" ?>>
			  TUVALU
			</OPTION>
			<OPTION value="UG" <?= $country=="UG"?"SELECTED":"" ?>>
			  UGANDA
			</OPTION>
			<OPTION value="UA" <?= $country=="UA"?"SELECTED":"" ?>>
			  UKRAINE
			</OPTION>
			<OPTION value="AE" <?= $country=="AE"?"SELECTED":"" ?>>
			  UNITED ARAB EMIRATES
			</OPTION>
			<OPTION value="GB" <?= $country=="GB"?"SELECTED":"" ?>>
			  UNITED KINGDOM
			</OPTION>
			<OPTION value="US" <?= $country=="US"?"SELECTED":"" ?>>
			  UNITED STATES OF AMERICA
			</OPTION>
			<OPTION value="UM" <?= $country=="UM"?"SELECTED":"" ?>>
			  UNITED STATES MINOR OUTLYING ISLANDS
			</OPTION>
			<OPTION value="UY" <?= $country=="UY"?"SELECTED":"" ?>>
			  URUGUAY
			</OPTION>
			<OPTION value="UZ" <?= $country=="UZ"?"SELECTED":"" ?>>
			  UZBEKISTAN
			</OPTION>
			<OPTION value="VU" <?= $country=="VU"?"SELECTED":"" ?>>
			  VANUATU
			</OPTION>
			<OPTION value="VA" <?= $country=="VA"?"SELECTED":"" ?>>
			  VATICAN CITY STATE
			</OPTION>
			<OPTION value="VE" <?= $country=="VE"?"SELECTED":"" ?>>
			  VENEZUELA
			</OPTION>
			<OPTION value="VN" <?= $country=="VN"?"SELECTED":"" ?>>
			  VIET NAM
			</OPTION>
			<OPTION value="VG" <?= $country=="VG"?"SELECTED":"" ?>>
			  VIRGIN ISLANDS, BRITISH
			</OPTION>
			<OPTION value="VI" <?= $country=="VI"?"SELECTED":"" ?>>
			  VIRGIN ISLANDS, U.S.
			</OPTION>
			<OPTION value="WF" <?= $country=="WF"?"SELECTED":"" ?>>
			  WALLIS AND FUTUNA
			</OPTION>
			<OPTION value="EH" <?= $country=="EH"?"SELECTED":"" ?>>
			  WESTERN SAHARA
			</OPTION>
			<OPTION value="YE" <?= $country=="YE"?"SELECTED":"" ?>>
			  YEMEN
			</OPTION>
			<OPTION value="YU" <?= $country=="YU"?"SELECTED":"" ?>>
			  YUGOSLAVIA
			</OPTION>
			<OPTION value="CD" <?= $country=="CD"?"SELECTED":"" ?>>
			  ZAIRE
			</OPTION>
			<OPTION value="ZM" <?= $country=="ZM"?"SELECTED":"" ?>>
			  ZAMBIA
			</OPTION>
			<OPTION value="ZW" <?= $country=="ZW"?"SELECTED":"" ?>>
			  ZIMBABWE
			</OPTION>
			</SELECT>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right">
			Postal/Zip Code
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="postal_code" size="10" maxlength="10" name="postal_code" value="<?= $postal_code ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" valign="middle" height="2">
			<A name="Note1back">&nbsp;* Email address</A>
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="email" size="40" name="email" value="<?= $email ?>">
		      </TD>
		      <TD align="left" width="10">
			<A href="#Note1" class="c2" tabindex="-1">Note&nbsp;1</A>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" valign="middle" height="2">
			* Phone number
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="phone" size="40" name="phone" value="<?= $phone ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="left" colspan="2">
			<CENTER><INPUT type="checkbox" id="keep_informed" name="keep_informed" value="true" <?= $sel_keep_informed ?>> Keep me informed on the progress of this election</CENTER>
		      </TD>
		    </TR>
		    <TR>
		      <TD align=left colspan=2>
			<BR><B>The place you are registered to vote (or if not registered, the last place that you resided at in the US):</B>
		      </TD>
		    </TR>
		    <TR>
		      <TD valign="middle" height="2" align="right">
			<A name="Note1back">County or Parish</A>
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="reg_county" size="40" name="reg_county" value="<?= $reg_county ?>">
		      </TD>
		      <TD align="left" width="10">
			<A href="#Note2" class="c2" tabindex="-1">Note&nbsp;2</A>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right">
			* State
		      </TD>
		      <TD align="left">
			<SELECT name="reg_state">
			  <OPTION value="0">
			    --select--
			  </OPTION>
			  <OPTION value="AL" <?= $reg_state=="AL"?"SELECTED":"" ?>>
			    Alabama
			  </OPTION>
			  <OPTION value="AK" <?= $reg_state=="AK"?"SELECTED":"" ?>>
			    Alaska
			  </OPTION>
			  <OPTION value="AZ" <?= $reg_state=="AZ"?"SELECTED":"" ?>>
			    Arizona
			  </OPTION>
			  <OPTION value="AR" <?= $reg_state=="AR"?"SELECTED":"" ?>>
			    Arkansas
			  </OPTION>
			  <OPTION value="CA" <?= $reg_state=="CA"?"SELECTED":"" ?>>
			    California
			  </OPTION>
			  <OPTION value="CO" <?= $reg_state=="CO"?"SELECTED":"" ?>>
			    Colorado
			  </OPTION>
			  <OPTION value="CT" <?= $reg_state=="CT"?"SELECTED":"" ?>>
			    Connecticut
			  </OPTION>
			  <OPTION value="DE" <?= $reg_state=="DE"?"SELECTED":"" ?>>
			    Delaware
			  </OPTION>
			  <OPTION value="DC" <?= $reg_state=="DC"?"SELECTED":"" ?>>
			    District Of Columbia
			  </OPTION>
			  <OPTION value="FL" <?= $reg_state=="FL"?"SELECTED":"" ?>>
			    Florida
			  </OPTION>
			  <OPTION value="GA" <?= $reg_state=="GA"?"SELECTED":"" ?>>
			    Georgia
			  </OPTION>
			  <OPTION value="HI" <?= $reg_state=="HI"?"SELECTED":"" ?>>
			    Hawaii
			  </OPTION>
			  <OPTION value="ID" <?= $reg_state=="ID"?"SELECTED":"" ?>>
			    Idaho
			  </OPTION>
			  <OPTION value="IL" <?= $reg_state=="IL"?"SELECTED":"" ?>>
			    Illinois
			  </OPTION>
			  <OPTION value="IN" <?= $reg_state=="IN"?"SELECTED":"" ?>>
			    Indiana
			  </OPTION>
			  <OPTION value="IA" <?= $reg_state=="IA"?"SELECTED":"" ?>>
			    Iowa
			  </OPTION>
			  <OPTION value="KS" <?= $reg_state=="KS"?"SELECTED":"" ?>>
			    Kansas
			  </OPTION>
			  <OPTION value="KY" <?= $reg_state=="KY"?"SELECTED":"" ?>>
			    Kentucky
			  </OPTION>
			  <OPTION value="LA" <?= $reg_state=="LA"?"SELECTED":"" ?>>
			    Louisiana
			  </OPTION>
			  <OPTION value="ME" <?= $reg_state=="ME"?"SELECTED":"" ?>>
			    Maine
			  </OPTION>
			  <OPTION value="MD" <?= $reg_state=="MD"?"SELECTED":"" ?>>
			    Maryland
			  </OPTION>
			  <OPTION value="MA" <?= $reg_state=="MA"?"SELECTED":"" ?>>
			    Massachusetts
			  </OPTION>
			  <OPTION value="MI" <?= $reg_state=="MI"?"SELECTED":"" ?>>
			    Michigan
			  </OPTION>
			  <OPTION value="MN" <?= $reg_state=="MN"?"SELECTED":"" ?>>
			    Minnesota
			  </OPTION>
			  <OPTION value="MS" <?= $reg_state=="MS"?"SELECTED":"" ?>>
			    Mississippi
			  </OPTION>
			  <OPTION value="MO" <?= $reg_state=="MO"?"SELECTED":"" ?>>
			    Missouri
			  </OPTION>
			  <OPTION value="MT" <?= $reg_state=="MT"?"SELECTED":"" ?>>
			    Montana
			  </OPTION>
			  <OPTION value="NE" <?= $reg_state=="NE"?"SELECTED":"" ?>>
			    Nebraska
			  </OPTION>
			  <OPTION value="NV" <?= $reg_state=="NV"?"SELECTED":"" ?>>
			    Nevada
			  </OPTION>
			  <OPTION value="NH" <?= $reg_state=="NH"?"SELECTED":"" ?>>
			    New Hampshire
			  </OPTION>
			  <OPTION value="NJ" <?= $reg_state=="NJ"?"SELECTED":"" ?>>
			    New Jersey
			  </OPTION>
			  <OPTION value="NM" <?= $reg_state=="NM"?"SELECTED":"" ?>>
			    New Mexico
			  </OPTION>
			  <OPTION value="NY" <?= $reg_state=="NY"?"SELECTED":"" ?>>
			    New York
			  </OPTION>
			  <OPTION value="NC" <?= $reg_state=="NC"?"SELECTED":"" ?>>
			    North Carolina
			  </OPTION>
			  <OPTION value="ND" <?= $reg_state=="ND"?"SELECTED":"" ?>>
			    North Dakota
			  </OPTION>
			  <OPTION value="OH" <?= $reg_state=="OH"?"SELECTED":"" ?>>
			    Ohio
			  </OPTION>
			  <OPTION value="OK" <?= $reg_state=="OK"?"SELECTED":"" ?>>
			    Oklahoma
			  </OPTION>
			  <OPTION value="OR" <?= $reg_state=="OR"?"SELECTED":"" ?>>
			    Oregon
			  </OPTION>
			  <OPTION value="PA" <?= $reg_state=="PA"?"SELECTED":"" ?>>
			    Pennsylvania
			  </OPTION>
			  <OPTION value="RI" <?= $reg_state=="RI"?"SELECTED":"" ?>>
			    Rhode Island
			  </OPTION>
			  <OPTION value="SC" <?= $reg_state=="SC"?"SELECTED":"" ?>>
			    South Carolina
			  </OPTION>
			  <OPTION value="SD" <?= $reg_state=="SD"?"SELECTED":"" ?>>
			    South Dakota
			  </OPTION>
			  <OPTION value="TN" <?= $reg_state=="TN"?"SELECTED":"" ?>>
			    Tennessee
			  </OPTION>
			  <OPTION value="TX" <?= $reg_state=="TX"?"SELECTED":"" ?>>
			    Texas
			  </OPTION>
			  <OPTION value="UT" <?= $reg_state=="UT"?"SELECTED":"" ?>>
			    Utah
			  </OPTION>
			  <OPTION value="VT" <?= $reg_state=="VT"?"SELECTED":"" ?>>
			    Vermont
			  </OPTION>
			  <OPTION value="VA" <?= $reg_state=="VA"?"SELECTED":"" ?>>
			    Virginia
			  </OPTION>
			  <OPTION value="WA" <?= $reg_state=="WA"?"SELECTED":"" ?>>
			    Washington
			  </OPTION>
			  <OPTION value="WV" <?= $reg_state=="WV"?"SELECTED":"" ?>>
			    West Virginia
			  </OPTION>
			  <OPTION value="WI" <?= $reg_state=="WI"?"SELECTED":"" ?>>
			    Wisconsin
			  </OPTION>
			  <OPTION value="WY" <?= $reg_state=="WY"?"SELECTED":"" ?>>
			    Wyoming
			  </OPTION>
			  <OPTION value="AS" <?= $reg_state=="AS"?"SELECTED":"" ?>>
			    American Samoa
			  </OPTION>
			  <OPTION value="AE" <?= $reg_state=="AE"?"SELECTED":"" ?>>
			    Armed Forces Other
			  </OPTION>
			  <OPTION value="AA" <?= $reg_state=="AA"?"SELECTED":"" ?>>
			    Armed Forces Americas
			  </OPTION>
			  <OPTION value="AP" <?= $reg_state=="AP"?"SELECTED":"" ?>>
			    Armed Forces Pacific
			  </OPTION>
			  <OPTION value="FM" <?= $reg_state=="FM"?"SELECTED":"" ?>>
			    Federated States of Micronesia
			  </OPTION>
			  <OPTION value="GU" <?= $reg_state=="GU"?"SELECTED":"" ?>>
			    Guam
			  </OPTION>
			  <OPTION value="MH" <?= $reg_state=="MH"?"SELECTED":"" ?>>
			    Marshall Islands
			  </OPTION>
			  <OPTION value="MP" <?= $reg_state=="MP"?"SELECTED":"" ?>>
			    Northern Mariana Islands
			  </OPTION>
			  <OPTION value="PW" <?= $reg_state=="PW"?"SELECTED":"" ?>>
			    Palau
			  </OPTION>
			  <OPTION value="PR" <?= $reg_state=="PR"?"SELECTED":"" ?>>
			    Puerto Rico
			  </OPTION>
			  <OPTION value="VI" <?= $reg_state=="VI"?"SELECTED":"" ?>>
			    Virgin Islands
			  </OPTION>
			</SELECT>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="left" colspan="2">
			<CENTER><INPUT type="checkbox" id="not_registered" name="not_registered" value="true" <?= $sel_not_registered ?>> I am <strong>not</strong> currently registered to vote</CENTER>
		      </TD>
		    </TR>
		    <TR>
		      <TD align=left colspan=2>
			<BR><B>Additional information used to validate your identity:</B>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right">
			* City, Town of birth
		      </TD>
		      <TD align="left">
			<INPUT type="text" size="40" id="birth_city" name="birth_city" value="<?= $birth_city ?>">
		      </TD>
		      <TD align="left" width="10">
			<A href="#Note3" class="c2" tabindex="-1">Note&nbsp;3</A>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right">
			* State of birth
		      </TD>
		      <TD align="left">
			<INPUT type="text" size="40" id="birth_state" name="birth_state" value="<?= $birth_state ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right">
			* Country of birth
		      </TD>
		      <TD align="left">
			<INPUT type="text" size="40" id="birth_country" name="birth_country" value="<?= $birth_country ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" valign="top">
			* Birth date
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="birth_date" name="birth_date" value="<?= $birth_date ?>">&nbsp;mm/dd/yyyy (e.g. 01/15/1944)
		      </TD>
		    </TR>
		    <TR>
		      <TD>
		      </TD>
		      <TD align="left">
			(must be at least 18 years old)
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" valign="top">
		       * Select a question
		      </TD>
		      <TD align="left">
			<INPUT type="hidden" id="question" name="question">
			<SELECT name="selQuestion">
			  <OPTION value="0">
			    --select--
			  </OPTION>
			  <OPTION value="1" <?= $question=="Favorite pet"?"SELECTED":"" ?>>
			    Favorite pet
			  </OPTION>
			  <OPTION value="2" <?= $question=="Favorite drink"?"SELECTED":"" ?>>
			    Favorite drink
			  </OPTION>
			  <OPTION value="3" <?= $question=="Favorite show"?"SELECTED":"" ?>>
			    Favorite show
			  </OPTION>
			  <OPTION value="4" <?= $question=="Mother's maiden name"?"SELECTED":"" ?>>
			    Mother's maiden name
			  </OPTION>
			  <OPTION value="5" <?= $question=="Place where you grew up"?"SELECTED":"" ?>>
			    Place where you grew up
			  </OPTION>
			  <OPTION value="6" <?= $question=="Childhood best friend"?"SELECTED":"" ?>>
			    Childhood best friend
			  </OPTION>
			  <OPTION value="7" <?= $question=="A password or phrase"?"SELECTED":"" ?>>
			    A password or phrase
			  </OPTION>
			</SELECT>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" valign="top">
			* Your response
		      </TD>
		      <TD align="left">
			<INPUT type="text" size=40 id="response" name="response" value="<?= $response ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" valign="top">
			* Your Password
		      </TD>
		      <TD align="left">
			<INPUT type="password" size=40 id="password" name="password" value="<?= $password ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10">
			&nbsp; * required field
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10">
			<TABLE>
			  <TBODY>
			    <TR>
			      <TD valign="top" nowrap>
				<A name="Note1">&nbsp;</A>
				<B>Note 1:</B>
			      </TD>
			      <TD>
				Your phone number and email are vital to process your registration. To certify your vote, we must validate your identity as a registered voter for government elections; this gives you the right to vote in this Philadelphia II Election. Validating your identity will permit us to count your vote in the final tally. <A href="#Note1back" class="c2">back</A>
			      </TD>
			     </TR>
			    </TBODY>
			  </TABLE>
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10">
			<TABLE>
			  <TBODY>
			    <TR>
			      <TD valign="top" nowrap>
				<A name="Note2">&nbsp;</A>
				<B>Note 2:</B>
			      </TD>
			      <TD>
				Enter your State (or US Territory) and County or Parish of residence (where you are registered to vote in government elections). If you are not registered in your county or parish or have just come of age to vote (18 years old.), please continue to register here. We will help you to register. If you live outside the United States, please enter here the State and County or Parish where you last voted in the United States. If you are a US Citizen who has never lived in the United States and you have just come of age to vote, please enter the State and County or Parish where one of your parents are registered to vote in US elections.<A href="#Note2back" class="c2">back</A>
			      </TD>
			     </TR>
			    </TBODY>
			  </TABLE>
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10" align="left">
			<TABLE>
			  <TBODY>
			    <TR>
			      <TD valign="top" nowrap>
				<A name="Note3">&nbsp;</A>
				<B>Note 3:</B>
			      </TD>
			      <TD>
				The place you were born is used to uniquely identify you within your voting district.  Enter the place you were born, for example the name of the city, town that is marked on your birth certificate.  Enter the two letter abbreviation for the state you were born in.  If you were not born in the United States or one of its territories, enter the name of the country you were born in.<A href="#Note3back" class="c2">back</A>
			      </TD>
			     </TR>
			    </TBODY>
			  </TABLE>
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10">&nbsp;
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" align="center">
			<INPUT type="submit" name="btnAddVoter" value="Continue" onClick="return Validate(this.form);" class="Button">
		      </TD>
		    </TR>
		  </TBODY>
		</TABLE>

      </FORM>
    <?php include("bottom.htm"); ?>
  </BODY>
</HTML>

