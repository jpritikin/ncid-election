<?
// Ballot.php - secure page

// session check
session_start();
header("Cache-control: private");
if (!session_is_registered("SESSION"))
{
	// if session check fails, invoke error handler
	header("Location: InvalidLogin.php?e=2");
	exit();
}

$vote = $_SESSION['vote'];
$name = $_SESSION['name'];

?>

<SCRIPT language="JavaScript" type="text/javascript">

function Validate(form)
{
    if (!(form.yes_vote.checked || form.no_vote.checked)) {
	alert("Please vote either yes or no");
	return false;
    }
    form.action = "ConfirmVote.php";
    return true;
}
</SCRIPT>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
    <TITLE>
      Philadelphia II - Ballot
    </TITLE>
    <link href="style.css" rel="stylesheet" type="text/css">
  </HEAD>
  <BODY>
    <?php include("menu-user.htm"); ?>
    <?php include("top.htm"); ?>

		      <FORM method="post" id="login" name="login" action="ConfirmVote.php">
				<h1>Cast Your Vote</h1>

				<DIV class="c3 putcenter">
				  <h2><STRONG>The National Initiative For Democracy</STRONG>
		          </h2>
				</DIV>
				<TABLE border="0" width="100%" height="100%" bordercolor="red">
				  <TBODY>
				    <TR>
				      <TD colspan="4" width="100%">
					    <p>AMENDS THE CONSTITUTION OF THE UNITED STATES by ratifying the <A href="Amendment.php" target="_blank">Democracy Amendment</A> which recognizes the right of the American People to exercise
					their lawmaking powers directly by means of Legislative Initiative; restricts the funding of initiative campaigns to natural persons; and acknowledges the
					constitutionality of the National Initiative for Democracy and the national election conducted by Philadelphia II; and</p>
					    <p>ENACTS A UNITED STATES FEDERAL STATUTE &ndash; the <A href="Act.php" target="_blank">Democracy Act</A> &ndash; which establishes legislative procedures to permit the American People to exercise their
					lawmaking powers and establishes an agency &ndash;The United States Electoral Trust &ndash; to administer these legislative procedures on behalf of the American
					People.						</p>					  </TD>
				    </TR>
				    <TR>
				      <TD colspan="4">
					<DIV class="c6">
					  <STRONG class="c5"><CENTER><EM>Do you wish to enact<BR>
					   The National Initiative For Democracy?<BR>
					  </EM></CENTER></STRONG>					</DIV>				      </TD>
				    </TR>
				    <TR>
				      <TD align="right" height="20" valign="middle">
					    <div align="center">
					      <INPUT type="radio" class="BigRadioButton" name="vote" id="yes_vote" value="YES" <? if ($vote == "YES") { echo 'checked="TRUE"'; } ?> >
					      <strong>Yes</strong>
						  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				          <INPUT type="radio" class="BigRadioButton" name="vote" id="no_vote" value="NO" <? if ($vote == "NO") { echo 'checked="TRUE"'; } ?>>
				          <strong>No</strong> </div></TD>
			        </TR>
				    <TR>
				      <TD class="c5" colspan="4" align="center">
					<? if ($vote) { echo "Your current choice is shown."; } ?>				      </TD>
				    </TR>
				    <TR>
				      <TD class="c4" colspan="4" align="center">
					<? if ($vote) { echo "Voting again will update your previous choice."; } ?>				      </TD>
				    </TR>
				  </TBODY>
				</TABLE>

			<div class="putcenter">
				<INPUT type="submit" value="Vote" name="submit" class="Button" onClick="return Validate(this.form);">
			</div>
		      </FORM>

	<?php include("bottom.htm"); ?>
  </BODY>
</HTML>

