<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>The National Initiative for Democracy - The Democracy Act</title>
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<?php
      if (session_is_registered("SESSION")) {
		include("menu-user.htm");
      } else {
	    include("menu-anon.htm");
	  }
	?>
<?php include("top.htm"); ?>
    <h1>The Democracy Amendment </h1>
    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td><center>
            September 17, 2002
            </center>
			<blockquote>
             <p><strong>Section 1.</strong> The sovereign authority and the legislative power of citizens of the
              United States to enact, repeal and amend public policy, laws, charters,
              and constitutions by local, state and national initiatives shall not be
              denied or abridged by the United States or any state.</p>
              <p> <strong>Section 2.</strong> The citizens of the United States
                hereby sanction the national election conducted by the nonprofit
                corporation Philadelphia II, permitting the enactment of this Article
                and the Democracy Act. </p>
              <p> <strong>Section 3.</strong> The United States Electoral
                Trust (hereinafter "Electoral Trust") is hereby created to administer
                the procedures established by this Article and the Democracy Act. A
                Board of Trustees and a Director shall govern the Electoral Trust. The
                Board of Trustees shall be composed of one member elected by the
                citizens of each state, the District of Columbia, Puerto Rico, and the
                Territories of the United States. An election shall be conducted every
                two years to elect members of the Board of Trustees. Immediately after
                the first election, the elected members shall be divided as equally as
                possible into two classes. The seats of the members of the first class
                shall be vacated at the expiration of the second year; the seats of the
                members of the second class shall be vacated at the expiration of the
                fourth year. All members of the Board of Trustees shall serve for four
                years except the members of the first class. In order to facilitate the
                initial election of members to the Board of Trustees, an Interim Board
                is appointed by the Democracy Act. A Director responsible for
                day-to-day operations shall be appointed by the majority of the members
                of the Board of Trustees, except that the first Director shall be
                appointed by the Board of Directors of Philadelphia II. </p>
              <p> <strong>Section 4.</strong> An initiative created under
                the authority of this Article that modifies a constitution or charter
                assumes the force of law when it is approved by more than half the
                registered voters of the relevant jurisdiction in each of two
                successive elections conducted by the Electoral Trust. If such
                initiative is approved in the first election, the second election shall
                occur no earlier than six months and no later than a year after the
                first election. An initiative created under the authority of this
                Article that enacts, modifies or repeals any statute assumes the force
                of law when approved by more than half the registered voters of the
                relevant jurisdiction participating in an election conducted by the
                Electoral Trust. </p>
              <p> <strong>Section 5.</strong> Only natural persons who are citizens of the United States may sponsor an initiative under the authority of this Article. </p>
              <p> <strong>Section 6.</strong> Only natural persons who are citizens of the United States  may
                contribute funds, services or property in support of or in opposition
                to a legislative initiative created under the authority of this
                Article. Contributions from corporations including, but not limited to,
                such incorporated entities as industry groups, labor unions, political
                parties, political action committees, organized religions and
                associations, are specifically prohibited. Such entities are also
                prohibited from coercing or inducing employees, clients, customers,
                members, or any other associated persons to support or oppose an
                initiative created under the authority of this Article. </p>
              <p> <strong>Section 7.</strong> The people shall have the
                power to enforce the provisions of this Article by appropriate
                legislation. No court in the United States may enjoin an initiative
                election except on grounds of fraud. </p>
            </blockquote>
          </center>
              <blockquote><ul><p>&nbsp;</p>
                  </ul>
              </blockquote></td>
        </tr>
        <tr>
          <td><table align="center">
              <tbody>
                <tr>
                  <?php if (session_is_registered("SESSION")) { ?>
        <td><form action="Ballot.php">
            <input name="submit" type="submit" class="Button" value="Return to your ballot">
          </form></td>
        <?php } else { ?>
        <td><form action="login.php">
            <input name="submit2" type="submit" class="Button" value="Return to login screen">
          </form></td>
        <td>&nbsp;</td>
        <td><form action="EditNewVoter.php">
            <input name="submit2" type="submit" class="Button" value="Register">
          </form></td>
        <?php } ?>
      </tr>
    </tbody>
  </table>
  </td>
  </tr>
  </tbody>
  </table>
  <!-- ==============================================================	-->
  <?php include("bottom.htm"); ?>
</body>
</html>
