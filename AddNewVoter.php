<?
require 'vars.php';
require 'mgmail.php';

header("Cache-control: private");

//
// Generates a random string with the specified length
// Includes: a-z, A-Z y 0-9
// written by demogracia at metropoliglobal dot com
//
$randStringList = 'BCDFGHJKLMNPRSTVWXYZ123456789bcdfghjklmnprstvwxyz';
function randString($length=16) {
    global $randStringList;
    mt_srand((double)microtime()*1000000);
    $newstring="";

    if($length>0){
    while(strlen($newstring)<$length){
        $newstring.=$randStringList{mt_rand(0,48)};
        }
    }
    return $newstring;
}

function mail_error($msg) {
    $hdrs = "From: AddNewVoter.php@votep2.us\r\n";

    mgmail("mgrant@grant.org", "Error while adding to database", $msg, $hdrs);

    echo "We're sorry, an error has occured while updating the database.  Your data has not been lost, it will be processed manually and you will receive an email shortly.<BR>";
    echo 'Click <A href="login.php">here</a> to return to the login page.';
    exit;
}

function clean($input) {
    $input = stripslashes($input);
    $input = str_replace("'", "''", $input);
    $input = str_replace("\\", "", $input);
    return($input);
}

foreach ($_POST as $key => $value) {
  $_POST[$key] = stripslashes($value);
}

if (!$_POST['first_name'] ||
    !$_POST['last_name'] ||
    !$_POST['address_1'] ||
    !$_POST['city'] ||
    !$_POST['country'] ||
    !$_POST['postal_code'] ||
    !$_POST['email'] ||
    !$_POST['phone'] ||
    !$_POST['reg_state'] ||
    !$_POST['birth_city'] ||
    !$_POST['birth_country'] ||
    !$_POST['birth_date'] ||
    !$_POST['question'] ||
    !$_POST['response'])
{
  // if any of the required fields are missing, go back to form page.
  // this can only happen if someone is messing around.
	header("Location: EditNewVoter.php");
	exit();
}

$title = clean($_POST['title']);
$first_name = clean($_POST['first_name']);
$middle_name = clean($_POST['middle_name']);
$last_name = clean($_POST['last_name']);
$suffix = clean($_POST['suffix']);
$address_1 = clean($_POST['address_1']);
$address_2 = clean($_POST['address_2']);
$address_3 = clean($_POST['address_3']);
$city = clean($_POST['city']);
$state = clean($_POST['state']);
$country = clean($_POST['country']);
$postal_code = clean($_POST['postal_code']);
$email = clean($_POST['email']);
$phone = clean($_POST['phone']);
$keep_informed = $_POST['keep_informed'] ? "Yes" : "No";
$reg_county = clean($_POST['reg_county']);
$reg_state = clean($_POST['reg_state']);
$registered = $_POST['not_registered']=="true" ? "No" : "Yes";
$birth_city = clean($_POST['birth_city']);
$birth_state = clean($_POST['birth_state']);
$birth_country = clean($_POST['birth_country']);
list($m,$d,$y) = explode("/", $_POST['birth_date']);
$birth_date = $y."/".$m."/".$d;
$birth_date = str_replace("'","",$birth_date);
$birth_date = str_replace("\\","",$birth_date);
$ssn = clean($_POST['ssn']);
$question = clean($_POST['question']);
$response = clean($_POST['response']);

$password = randString(8);

mysql_connect ($sql_host, $sql_user, $sql_pass);

mysql_select_db ($sql_db);

// Check if the "Mail me my password" button was pressed
if ($_POST['submit']=="Mail me my password") {
    $result = mysql_query ("SELECT voter_id,password,email
                            FROM $voter_table
                            WHERE first_name='$first_name' AND last_name='$last_name' AND birth_city='$birth_city' AND birth_date='$birth_date'") or die(mysql_error());

    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
	$voter_id = $row['voter_id'];
	$password = $row['password'];
	$email = $row['email'];

        $hdrs = "From: info@philadelphiaII.us\r\n"
	        ."Reply-To: info@philadelphiaII.us\r\n";

        $msg = "Thank you for registering to vote on The National Initiative for Democracy. Below is your:
		    Voter ID:  $voter_id
		    Password:  $password

	    For your convenience, you may use this url:
		    $login_url?voter_id=$voter_id&password=$password

	    Click on the above link or copy and paste the Voter ID and the Password into the proper boxes on the login form at http://www.votep2.us

	    Be sure to save your Voter ID and Password in a safe place to protect your vote. In the event you wish to change your vote, which you can do at any time, just log in using your Voter ID and Password. Your last vote is the one that will go into the final tally.
	    ";

        mgmail($email, "Your National Initiative Voter Registration", $msg, $hdrs);
    }

    mysql_free_result($result);

    header("Location: login.php");
    exit();
}

// Add new record

$query = "INSERT INTO $voter_table
          SET password='$password',title='$title',first_name='$first_name',middle_name='$middle_name',last_name='$last_name',suffix='$suffix',address_1='$address_1',address_2='$address_2',address_3='$address_3',city='$city',state='$state',country='$country',postal_code='$postal_code',email='$email',phone='$phone',keep_informed='$keep_informed',reg_county='$reg_county',reg_state='$reg_state',registered='$registered',birth_city='$birth_city',birth_state='$birth_state',birth_country='$birth_country',birth_date='$birth_date',ssn='$ssn',question='$question',response='$response',date_created=NOW()";

$result = mysql_query($query);
if (!$result) {
    mail_error("mysql_error: ".mysql_error()."\n\n".mysql_info()."\n\n".$query."\n");
    exit;
}

// Find out user's id
$voter_id = mysql_insert_id();

if (!$voter_id) {
    mail_error("mysql_error: ".mysql_error()."\n\n".mysql_info()."\n\n".$query."\n");
    exit;
}

 $name = $title." ".$first_name." ".$middle_name." ".$last_name." ".$suffix;
 $name=str_replace("  "," ",$name);

// Send email with username/password to voter

$hdrs = "From: info@philadelphiaII.us\r\n"
        ."Reply-To: info@philadelphiaII.us\r\n";

$msg = "Thank you for registering to vote in the national election on the National Initiative for Democracy. Below is your:
		Voter ID:  $voter_id
		Password:  $password

	For your convenience, you may use this url:
		$login_url?voter_id=$voter_id&password=$password

	Click on the above link or copy and paste the Voter ID and the Password into the proper box on the login form at https://www.votep2.us

	Be sure to save your Voter ID and Password in a safe place to protect your vote. In the event you wish to change your vote or your registration information, which you can do at any time, just log in using your Voter ID and Password. Your last vote is the one that will go into the final tally.";

mgmail($_POST['email'], "Your National Initiative Voter Registration", $msg, $hdrs);

$hdrs = "From: info@philadelphiaII.us\r\n"
        ."Reply-To: info@philadelphiaII.us\r\n";

$msg = "voter_id='$voter_id'
name='$title $first_name $middle_name $last_name $suffix'
address_1='$address_1'
address_2='$address_2'
address_3='$address_3'
city='$city'
state='$state'
country='$country'
postal_code='$postal_code'
email='$email'
phone='$phone'
keep_informed='$keep_informed'
reg_county='$reg_county'
reg_state='$reg_state'
registered='$registered'
birth_city='$birth_city'
birth_state='$birth_state'
birth_country='$birth_country'
birth_date='$birth_date'";

// mgmail("info@philadelphiaII.us", "New Voter", $msg, $hdrs);


// Initiate a session
session_start();
header("Cache-control: private");
session_register("SESSION");

$_SESSION['password'] = $password;
$_SESSION['voter_id'] = "$voter_id";
$_SESSION['vote'] = "";
$_SESSION['name'] = $name;

// Redirect to protected page
// header("Location: Ballot.php");
// exit();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
  <HEAD>
    <TITLE>
      Philadelphia II - Add Voter
    </TITLE>
    <link href="style.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {font-family: Arial, Helvetica, sans-serif}
-->
    </style>
</HEAD>
  <BODY>
    <?php include("menu-anon.htm"); ?>
    <?php include("top.htm"); ?>
  	  <h1>Thank you for registering</h1>
	  <p>You will receive an email shortly with your Voter ID and password.</p>
      <p>Clicking on 'Continue' will send you back to the Welcome Page. When the email arrives, click on the link contained within it or copy and paste the Voter ID and the Password into the proper boxes on the form. </p>
      <p>Be sure to save your Voter ID and Password in a safe place to protect your vote. In the event you wish to change your vote, which you can do so at any time, just log in using your Voter ID and Password. Your last vote is the one that will go into the final tally.</p>
      <p>Meanwhile, if you are not already registered to vote in your jurisdiction, please follow one of these links below:</p>
      <table border="0" cellpadding="0" cellspacing="15" style="width:80%; margin:auto;">
        <tr>
          <td width="50%">If you live within the US:
            <div align="center"><a href="http://www.canivote.org/" target="_blank"><img src="images/canivote-logo.jpg" alt="Can I vote" width="180" height="77" border="1" longdesc="http://www.canivote.org"><span class="style1">CanIVote.org</span></a> </div></td>
          <td width="50%"><div align="center">If you live outside the US: <a href="http://www.fvap.gov" target="_parent"><img src="images/favp-logo.gif" alt="Federal Voter Assistance Program" width="180" height="110" border="1" longdesc="http://www.favp.org">FAVP.org</a></div></td>
        </tr>
      </table>
      <div style="width:100px:margin:auto;text-align:center;">
	    <form id="form1" name="Continue" method="post" action="login.php">
	      <INPUT name="submit" type="submit" class="Button" value="Return to login screen">
	    </form>
     </div>
    <?php include("bottom.htm"); ?>
  </BODY>
</HTML>

