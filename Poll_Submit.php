<?
  require 'vars.php';
// for testing live poll: 
// $poll_table='poll';

  // session check
  session_start();
  header("Cache-control: private");
  if (!session_is_registered("POLL_SESSION"))
  {
	  // if session check fails, invoke error handler
	  header("Location: https://votep2.us/login.php");
	  exit();
  }

  session_destroy();

  switch ($_POST['question-1']) {
    case "1": $question_1 = "tv_radio";
    break;
    case "2": $question_1 = "news_mag";
    break;
    case "3": $question_1 = "internet";
    break;
    case "4": $question_1 = "friend";
    break;
    case "W1": $question_1 = "other";
    break;
  }

  $question_1_other = str_replace("'", "''", stripslashes($_POST['question-1-W1-writein']));

  $question_2 = str_replace("'", "''", stripslashes($_POST['question-2-W1']));
  
  switch ($_POST['question-3']) {
    case 1: $question_3 = "yes";
    break;
    case 2: $question_3 = "no";
    break;
    case 3: $question_3 = "not_sure";
    break;
  }

  switch ($_POST['question-9']) {
    case 1: $question_9 = "for";
    break;
    case 2: $question_9 = "against";
    break;
  }

  switch ($_POST['question-4']) {
    case 1: $question_4 = "male";
    break;
    case 2: $question_4 = "female";
    break;
  }

  switch ($_POST['question-5']) {
    case 1: $question_5 = "-21";
    break;
    case 2: $question_5 = "21-40";
    break;
    case 3: $question_5 = "41-60";
    break;
    case 4: $question_5 = "61+";
    break;
  }

  switch ($_POST['question-6']) {
    case 1: $question_6 = "hs";
    break;
    case 2: $question_6 = "college";
    break;
    case 3: $question_6 = "post_bachelors";
    break;
  }

  switch ($_POST['question-7']) {
    case 1: $question_7 = "-20k";
    break;
    case 2: $question_7 = "20k-45k";
    break;
    case 3: $question_7 = "45k-75k";
    break;
    case 4: $question_7 = "75k-150k";
    break;
    case 5: $question_7 = "150k+";
    break;
  }

  switch ($_POST['question-8']) {
    case 1: $question_8 = "democrat";
    break;
    case 2: $question_8 = "republican";
    break;
    case 3: $question_8 = "libertarian";
    break;
    case 4: $question_8 = "reform";
    break;
    case 5: $question_8 = "green";
    break;
    case 6: $question_8 = "indep";
    break;
    case 7: $question_8 = "other";
    break;
    case 8: $question_8 = "none";
    break;
}

mysql_connect ($sql_host, $sql_user, $sql_pass);
mysql_select_db ($sql_db);

if ($question_1 || $question_1_other || $question_2 || $question_3 || $question_4 || $question_5 || $question_6 || $question_7 || $question_8 || $question_9) {

    // write $vote to database
    $update = "INSERT INTO $poll_table
	       SET question_1='$question_1', question_1_other='$question_1_other',question_2='$question_2',question_3='$question_3',question_4='$question_4',question_5='$question_5',question_6='$question_6',question_7='$question_7',question_8='$question_8',question_9='$question_9',date_created=NOW()";

    $result = mysql_query ($update);

    if (!result || mysql_affected_rows() == 0) {
	// some error occured
	$msg = "mysql_error: ".mysql_error()."\n\n$update\n";
	$hdrs = "From: Poll/Submit.php@votep2.us\r\n";
	mail("mgrant@grant.org", "error updating Poll", $msg, $hdrs);
    }
}

	// get results of poll
	
	// create a template
	// example of working query:
	// SELECT tmp1.question_3 as choice,(tmp1.CountOfItems/tmp2.CountOfAll) * 100 as answer From (SELECT question_3, Count(poll_ID) AS CountOfItems FROM testpoll where question_3 is not null GROUP BY question_3) tmp1, (SELECT Count(poll_ID) AS CountOfAll FROM testpoll where question_3 is not null) tmp2;
	$template = 'SELECT tmp1.%1$s as choice,(tmp1.CountOfItems/tmp2.CountOfAll) * 100 as answer From (SELECT %1$s, Count(poll_ID) AS CountOfItems FROM %2$s where %1$s is not null GROUP BY %1$s) tmp1, (SELECT Count(poll_ID) AS CountOfAll FROM %2$s where %1$s is not null) tmp2;';

	$query = sprintf($template, 'question_1', $poll_table);
	$result = mysql_query ($query) or die(mysql_error());
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
	  if ($row['choice'] == "") {
	    $q1["no response"]=round($row['answer']);
	  } else {
	    $q1[$row['choice']]=round($row['answer']);
	  }
	}
	mysql_free_result($result);

	$query = sprintf($template, 'question_3', $poll_table);
	$result = mysql_query ($query) or die(mysql_error());
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
	  if ($row['choice'] == "") {
	    $q3["no response"]=round($row['answer']);
	  } else {
	    $q3[$row['choice']]=round($row['answer']);
	  }
	}
	mysql_free_result($result);
	
	$query = sprintf($template, 'question_4', $poll_table);
	$result = mysql_query ($query) or die(mysql_error());
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
	  if ($row['choice'] == "") {
	    $q4["no response"]=round($row['answer']);
	  } else {
	    $q4[$row['choice']]=round($row['answer']);
	  }
	}
	mysql_free_result($result);

	$query = sprintf($template, 'question_5', $poll_table);
	$result = mysql_query ($query) or die(mysql_error());
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
	  if ($row['choice'] == "") {
	    $q5["no response"]=round($row['answer']);
	  } else {
	    $q5[$row['choice']]=round($row['answer']);
	  }
	}
	mysql_free_result($result);

	$query = sprintf($template, 'question_6', $poll_table);
	$result = mysql_query ($query) or die(mysql_error());
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
	  if ($row['choice'] == "") {
	    $q6["no response"]=round($row['answer']);
	  } else {
	    $q6[$row['choice']]=round($row['answer']);
	  }
	}
	mysql_free_result($result);

	$query = sprintf($template, 'question_7', $poll_table);
	$result = mysql_query ($query) or die(mysql_error());
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
	  if ($row['choice'] == "") {
	    $q7["no response"]=round($row['answer']);
	  } else {
	    $q7[$row['choice']]=round($row['answer']);
	  }
	}
	mysql_free_result($result);
	
	$query = sprintf($template, 'question_8', $poll_table);
	$result = mysql_query ($query) or die(mysql_error());
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
	  if ($row['choice'] == "") {
	    $q8["no response"]=round($row['answer']);
	  } else {
	    $q8[$row['choice']]=round($row['answer']);
	  }
	}
	mysql_free_result($result);
	
	$query = sprintf($template, 'question_9', $poll_table);
	$result = mysql_query ($query) or die(mysql_error());
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
	  if ($row['choice'] == "") {
	    $q9["no response"]=round($row['answer']);
	  } else {
	    $q9[$row['choice']]=round($row['answer']);
	  }
	}
	mysql_free_result($result);
	
	$result = mysql_query ("Select count(*) as answer from $poll_table") or die(mysql_error());
    $row = mysql_fetch_array($result, MYSQL_ASSOC);
	$count=$row['answer'];
	mysql_free_result($result);
	
	$result = mysql_query ("select count(*) as answer from $poll_table where question_9 is not null;") or die(mysql_error());
    $row = mysql_fetch_array($result, MYSQL_ASSOC);
	$fa_count=$row['answer'];
	mysql_free_result($result);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<TITLE>Philadelphia II - Submit Questionaire</TITLE>
<STYLE type="text/CSS">
    <!--
      td.candidate { font-family: verdana; color: #000000; font-size: 10pt; width: 35%}
      td.selection { font-family: verdana; color: #000000; font-size: 10pt }
      td.errmess   { font-family: verdana; color: #000000; font-size: 8pt }
      .text        { color: #000000; font-family: verdana; font-size: 10pt; margin-left: 10; 
		     margin-right: 10 }
      .copywrite { color: #000000; font-family: Verdana; font-size: 7pt; margin-left: 10; 
		     margin-right: 10 }
      .title       { color: #000000; font-family: verdana; font-size: 16pt; text-align: center; 
		     margin-left: 10; margin-right: 10 }
      .putcenter   { vertical-align: top; text-align: center }
      .textcenter  { text-align: center }
      .error       { color: #000000; font-family: verdana; font-size: 12pt; margin-left: 20; 
		     margin-right: 20 }
      table.c2 {text-align: center}
	  .c5 {line-height: 1em; font-size:80%; }
      div.c1 {color: #990000; font-size: 150%; text-align: center}
      .c3         { color: #000000; font-family: verdana; font-size: 10pt; margin-left: 10; 
		    margin-right: 10; text-align: left }
      .c4         { font-size: larger; margin-left: 10; }
      A                        {color: blue}
      .Button 	{color: #000066; font-size: 150%; font-weight: bold; background-color: white;
		     border: outset #000066 }
    -->
    </STYLE>
<link href="style.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY topmargin="2">
  <?php include("top.htm"); ?>
<TABLE width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
                  <TBODY>
				    <tr>
                      <TD height="30">
					  <h1>Thank you for completing the Exit Poll</h1></TD>
                    </tr>
                    <TR>
                      <TD height="30"><TABLE class="c2">
                        <TBODY>
                          <TR>
                            <TD><p class="c3">By voting in this election, you are helping to achieve
                a broad consensus on whether or not the People want to make laws and
                vote on issues that affect their lives. The second and equally important step is your help in funding this undertaking.  We hope you will choose to back up your vote.
                              <h2><A href="https://demofound.org/donate.htm">Please Back-up Your Vote</A> <BR>
                              </h2>
                              <h3>Poll Results</h3>
                                    <p class="c3">The poll has been taken <?php print $count ?> times.  Here are the current aggregate results of the poll.
Be aware that not everyone who votes will take the poll and no attempt has been made to verify or certify the data.</p>
                                    <table border="1" bordercolor="#000000" style="width:80%;margin:auto;border-style:solid;text-height:1em;">
                                      <tbody>
                                        <tr>
                                          <th width="50%">Question</th>
                                          <th>Responses</th>
                                        </tr>
                                        <tr>
                                          <td>Where did you first learn about the National Initiative?</td>
                                          <td><table class="c5">
                                            <?php foreach($q1 as $key => $val) { ?>
                                            <tr>
                                              <td width="80%"><?php print $key; ?></td>
                                              <td><?php print $val; ?>%</td>
                                            </tr>
                                            <?php } ?>
                                            </table></td>
                                        </tr>
                                        <tr>
                                          <td>Do you feel we have a government "by the people"?</td>
                                          <td><table class="c5">
                                            <?php foreach($q3 as $key => $val) { ?>
                                            <tr>
                                              <td width="80%"><?php print $key; ?></td>
                                              <td><?php print $val; ?>%</td>
                                            </tr>
                                            <?php } ?>
                                            </table></td>
                                        </tr>
                                        <tr>
                                          <td>Gender</td>
                                          <td><table class="c5">
                                            <?php foreach($q4 as $key => $val) { ?>
                                            <tr>
                                              <td width="80%"><?php print $key; ?></td>
                                              <td><?php print $val; ?>%</td>
                                            </tr>
                                            <?php } ?>
                                            </table></td>
                                        </tr>
                                        <tr>
                                          <td>Age</td>
                                          <td><table class="c5">
                                            <?php foreach($q5 as $key => $val) { ?>
                                            <tr>
                                              <td width="80%"><?php print $key; ?></td>
                                              <td><?php print $val; ?>%</td>
                                            </tr>
                                            <?php } ?>
                                            </table class="c3"></td>
                                        </tr>
                                        <tr>
                                          <td>Education</td>
                                          <td><table class="c5">
                                            <?php foreach($q6 as $key => $val) { ?>
                                            <tr>
                                              <td width="80%"><?php print $key; ?></td>
                                              <td><?php print $val; ?>%</td>
                                            </tr>
                                            <?php } ?>
                                            </table class="c3"></td>
                                        </tr>
                                        <tr>
                                          <td>Income</td>
                                          <td><table class="c5">
                                            <?php foreach($q7 as $key => $val) { ?>
                                            <tr>
                                              <td width="80%"><?php print $key; ?></td>
                                              <td><?php print $val; ?>%</td>
                                            </tr>
                                            <?php } ?>
                                            </table></td>
                                        </tr>
                                        <tr>
                                          <td>Party Affiliation</td>
                                          <td><table class="c5">
                                            <?php foreach($q8 as $key => $val) { ?>
                                            <tr>
                                              <td width="80%"><?php print $key; ?></td>
                                              <td><?php print $val; ?>%</td>
                                            </tr>
                                            <?php } ?>
                                            </table></td>
                                        </tr>
                                        <?php /* ?>
                                      <tr>
                                        <td>For/Against (*) </td>
                                        <td><table class="c5">
                                            <?php foreach($q9 as $key => $val) { ?>
                                            <tr>
                                              <td width="80%"><?php print $key; ?></td>
                                              <td><?php print $val; ?>%</td>
                                            </tr>
                                            <?php } ?>
                                          </table></td>
                                      </tr>
<?php */ ?>
                                      </tbody>
                                    </table>
    <?php /* ?>
                                <p class="c3">* The for/against question was added in September 2008.  This field is the result of only <?php print $fa_count; ?> responses.</p>
<?php */ ?>
                              <p class="c3">* The for/against question was added in September 2008.  We will publish this field after we have sufficient data for it to be fairly averaged.</p>
                              </TD>
                                </TR>
                          </TBODY>
                        </TABLE>
                            <BR>                        </TD>
                        </TR>
                      <TR>
                        <TD align="center" class="c4">
					      <h2><A href="https://demofound.org/donate.htm">Please Back-up Your Vote</A></h2>
					    </TD>
                      </TR>
                    </TBODY>
                  </TABLE>
  <?php include("bottom.htm"); ?>
</BODY>
</HTML>
