<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<HTML>
  <HEAD>
    <TITLE>
      Philadelphia II - Voter Registration User/Password search    </TITLE>
    <STYLE type="text/css">
    <--
      a.c2		{font-size: smaller}
      table.c1	{text-align: center}
      .Button {color: #000066; font-size: 150%; font-weight: bold; background-color: white; border: outset #000066 }
.copywrite {color: #000000; font-family: Verdana; font-size: 7pt; margin-left: 10;
		     margin-right: 10 }
    -->
    </STYLE>
    <link href="style.css" rel="stylesheet" type="text/css">
  </HEAD>
  <BODY>
    <?php include("menu-anon.htm"); ?>
	<?php include("top.htm"); ?>
	<h1>Search Registration Advanced</h1>
      <FORM method="post" id="frmMemberInfo" name="frmMemberInfo" action="ConfirmAddNewVoter.php">

		<TABLE class="c1">
		  <TBODY>
		    <TR>
		      <TD align="left" colspan="3" height="10">
			This information is being collected on a secure site and
			will only be used to validate your government registration and your
			right to vote.
		      </TD>
		    </TR>
		    <TR>
		      <TD align="left" colspan="2">
			<B>Your name as you registerd to vote:</B>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap width="250">
			Title
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="title" size="40" name="title" value="<?= $_POST['title'] ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* First name
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="first_name" size="40" name="first_name" value="<?= $_POST['first_name'] ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			Middle name or initial
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="middle_name" size="40" name="middle_name" value="<?= $_POST['middle_name'] ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* Last name
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="last_name" size="40" name="last_name" value="<?= $_POST['last_name'] ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			Suffix
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="suffix" size="40" name="suffix" value="<?= $_POST['suffix'] ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* Address
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="address_1" size="40" name="address_1" value="<?= $_POST['address_1'] ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="left">&nbsp;
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="address_2" size="40" name="address_2" value="<?= $_POST['address_2'] ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="left">&nbsp;
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="address_3" size="40" name="address_3" value="<?= $_POST['address_3'] ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* City
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="city" size="40" name="city" value="<?= $_POST['city'] ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			** State
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="state" size="40" name="state" value="<?= $_POST['state'] ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* Country
		      </TD>
		      <TD align="left">
			<SELECT name="country">
			<OPTION value="US"" <?= $_POST['country']=="US"?"SELECTED":"" ?>>
			  UNITED STATES OF AMERICA
			</OPTION>
			<OPTION value="AF"" <?= $_POST['country']=="AF"?"SELECTED":"" ?>>
			  AFGHANISTAN
			</OPTION>
			<OPTION value="AL"" <?= $_POST['country']=="AL"?"SELECTED":"" ?>>
			  ALBANIA
			</OPTION>
			<OPTION value="DZ"" <?= $_POST['country']=="DZ"?"SELECTED":"" ?>>
			  ALGERIA
			</OPTION>
			<OPTION value="AS"" <?= $_POST['country']=="AS"?"SELECTED":"" ?>>
			  AMERICAN SAMOA
			</OPTION>
			<OPTION value="AD"" <?= $_POST['country']=="AD"?"SELECTED":"" ?>>
			  ANDORRA
			</OPTION>
			<OPTION value="AO"" <?= $_POST['country']=="AO"?"SELECTED":"" ?>>
			  ANGOLA
			</OPTION>
			<OPTION value="AI"" <?= $_POST['country']=="AI"?"SELECTED":"" ?>>
			  ANGUILLA
			</OPTION>
			<OPTION value="AQ"" <?= $_POST['country']=="AQ"?"SELECTED":"" ?>>
			  ANTARCTICA
			</OPTION>
			<OPTION value="AG"" <?= $_POST['country']=="AG"?"SELECTED":"" ?>>
			  ANTIGUA AND BARBUDA
			</OPTION>
			<OPTION value="AR"" <?= $_POST['country']=="AR"?"SELECTED":"" ?>>
			  ARGENTINA
			</OPTION>
			<OPTION value="AM"" <?= $_POST['country']=="AM"?"SELECTED":"" ?>>
			  ARMENIA
			</OPTION>
			<OPTION value="AW"" <?= $_POST['country']=="AW"?"SELECTED":"" ?>>
			  ARUBA
			</OPTION>
			<OPTION value="AU"" <?= $_POST['country']=="AU"?"SELECTED":"" ?>>
			  AUSTRALIA
			</OPTION>
			<OPTION value="AT"" <?= $_POST['country']=="AT"?"SELECTED":"" ?>>
			  AUSTRIA
			</OPTION>
			<OPTION value="AZ"" <?= $_POST['country']=="AZ"?"SELECTED":"" ?>>
			  AZERBAIJAN
			</OPTION>
			<OPTION value="BS"" <?= $_POST['country']=="BS"?"SELECTED":"" ?>>
			  BAHAMAS
			</OPTION>
			<OPTION value="BH"" <?= $_POST['country']=="BH"?"SELECTED":"" ?>>
			  BAHRAIN
			</OPTION>
			<OPTION value="BD"" <?= $_POST['country']=="BD"?"SELECTED":"" ?>>
			  BANGLADESH
			</OPTION>
			<OPTION value="BB"" <?= $_POST['country']=="BB"?"SELECTED":"" ?>>
			  BARBADOS
			</OPTION>
			<OPTION value="BY"" <?= $_POST['country']=="BY"?"SELECTED":"" ?>>
			  BELARUS
			</OPTION>
			<OPTION value="BE"" <?= $_POST['country']=="BE"?"SELECTED":"" ?>>
			  BELGIUM
			</OPTION>
			<OPTION value="BZ"" <?= $_POST['country']=="BZ"?"SELECTED":"" ?>>
			  BELIZE
			</OPTION>
			<OPTION value="BJ"" <?= $_POST['country']=="BJ"?"SELECTED":"" ?>>
			  BENIN
			</OPTION>
			<OPTION value="BM"" <?= $_POST['country']=="BM"?"SELECTED":"" ?>>
			  BERMUDA
			</OPTION>
			<OPTION value="BT"" <?= $_POST['country']=="BT"?"SELECTED":"" ?>>
			  BHUTAN
			</OPTION>
			<OPTION value="BO"" <?= $_POST['country']=="BO"?"SELECTED":"" ?>>
			  BOLIVIA
			</OPTION>
			<OPTION value="BA"" <?= $_POST['country']=="BA"?"SELECTED":"" ?>>
			  BOSNIA AND HERZEGOVINA
			</OPTION>
			<OPTION value="BW"" <?= $_POST['country']=="BW"?"SELECTED":"" ?>>
			  BOTSWANA
			</OPTION>
			<OPTION value="BV"" <?= $_POST['country']=="BV"?"SELECTED":"" ?>>
			  BOUVET ISLAND
			</OPTION>
			<OPTION value="BR"" <?= $_POST['country']=="BR"?"SELECTED":"" ?>>
			  BRAZIL
			</OPTION>
			<OPTION value="IO"" <?= $_POST['country']=="IO"?"SELECTED":"" ?>>
			  BRITISH INDIAN OCEAN TERRITORY
			</OPTION>
			<OPTION value="BN"" <?= $_POST['country']=="BN"?"SELECTED":"" ?>>
			  BRUNEI DARUSSALAM
			</OPTION>
			<OPTION value="BG"" <?= $_POST['country']=="BG"?"SELECTED":"" ?>>
			  BULGARIA
			</OPTION>
			<OPTION value="BF"" <?= $_POST['country']=="BF"?"SELECTED":"" ?>>
			  BURKINA FASO
			</OPTION>
			<OPTION value="BI"" <?= $_POST['country']=="BI"?"SELECTED":"" ?>>
			  BURUNDI
			</OPTION>
			<OPTION value="KH"" <?= $_POST['country']=="KH"?"SELECTED":"" ?>>
			  CAMBODIA
			</OPTION>
			<OPTION value="CM"" <?= $_POST['country']=="CM"?"SELECTED":"" ?>>
			  CAMEROON
			</OPTION>
			<OPTION value="CA"" <?= $_POST['country']=="CA"?"SELECTED":"" ?>>
			  CANADA
			</OPTION>
			<OPTION value="CV"" <?= $_POST['country']=="CV"?"SELECTED":"" ?>>
			  CAPE VERDE
			</OPTION>
			<OPTION value="KY"" <?= $_POST['country']=="KY"?"SELECTED":"" ?>>
			  CAYMAN ISLANDS
			</OPTION>
			<OPTION value="CF"" <?= $_POST['country']=="CF"?"SELECTED":"" ?>>
			  CENTRAL AFRICAN REPUBLIC
			</OPTION>
			<OPTION value="TD"" <?= $_POST['country']=="TD"?"SELECTED":"" ?>>
			  CHAD
			</OPTION>
			<OPTION value="CL"" <?= $_POST['country']=="CL"?"SELECTED":"" ?>>
			  CHILE
			</OPTION>
			<OPTION value="CN"" <?= $_POST['country']=="CN"?"SELECTED":"" ?>>
			  CHINA
			</OPTION>
			<OPTION value="CX"" <?= $_POST['country']=="CX"?"SELECTED":"" ?>>
			  CHRISTMAS ISLAND
			</OPTION>
			<OPTION value="CC"" <?= $_POST['country']=="CC"?"SELECTED":"" ?>>
			  COCOS (KEELING) ISLANDS
			</OPTION>
			<OPTION value="CO"" <?= $_POST['country']=="CO"?"SELECTED":"" ?>>
			  COLOMBIA
			</OPTION>
			<OPTION value="KM"" <?= $_POST['country']=="KM"?"SELECTED":"" ?>>
			  COMOROS
			</OPTION>
			<OPTION value="CG"" <?= $_POST['country']=="CG"?"SELECTED":"" ?>>
			  CONGO
			</OPTION>
			<OPTION value="CD"" <?= $_POST['country']=="CD"?"SELECTED":"" ?>>
			  CONGO, THE DEMOCRATIC REPUBLIC OF THE
			</OPTION>
			<OPTION value="CK"" <?= $_POST['country']=="CK"?"SELECTED":"" ?>>
			  COOK ISLANDS
			</OPTION>
			<OPTION value="CR"" <?= $_POST['country']=="CR"?"SELECTED":"" ?>>
			  COSTA RICA
			</OPTION>
			<OPTION value="CI"" <?= $_POST['country']=="CI"?"SELECTED":"" ?>>
			  C�TE D'IVOIRE
			</OPTION>
			<OPTION value="HR"" <?= $_POST['country']=="HR"?"SELECTED":"" ?>>
			  CROATIA
			</OPTION>
			<OPTION value="CU"" <?= $_POST['country']=="CU"?"SELECTED":"" ?>>
			  CUBA
			</OPTION>
			<OPTION value="CY"" <?= $_POST['country']=="CY"?"SELECTED":"" ?>>
			  CYPRUS
			</OPTION>
			<OPTION value="CZ"" <?= $_POST['country']=="CZ"?"SELECTED":"" ?>>
			  CZECH REPUBLIC
			</OPTION>
			<OPTION value="DK"" <?= $_POST['country']=="DK"?"SELECTED":"" ?>>
			  DENMARK
			</OPTION>
			<OPTION value="DJ"" <?= $_POST['country']=="DJ"?"SELECTED":"" ?>>
			  DJIBOUTI
			</OPTION>
			<OPTION value="DM"" <?= $_POST['country']=="DM"?"SELECTED":"" ?>>
			  DOMINICA
			</OPTION>
			<OPTION value="DO"" <?= $_POST['country']=="DO"?"SELECTED":"" ?>>
			  DOMINICAN REPUBLIC
			</OPTION>
			<OPTION value="EC"" <?= $_POST['country']=="EC"?"SELECTED":"" ?>>
			  ECUADOR
			</OPTION>
			<OPTION value="EG"" <?= $_POST['country']=="EG"?"SELECTED":"" ?>>
			  EGYPT
			</OPTION>
			<OPTION value="SV"" <?= $_POST['country']=="SV"?"SELECTED":"" ?>>
			  EL SALVADOR
			</OPTION>
			<OPTION value="GQ"" <?= $_POST['country']=="GQ"?"SELECTED":"" ?>>
			  EQUATORIAL GUINEA
			</OPTION>
			<OPTION value="ER"" <?= $_POST['country']=="ER"?"SELECTED":"" ?>>
			  ERITREA
			</OPTION>
			<OPTION value="EE"" <?= $_POST['country']=="EE"?"SELECTED":"" ?>>
			  ESTONIA
			</OPTION>
			<OPTION value="ET"" <?= $_POST['country']=="ET"?"SELECTED":"" ?>>
			  ETHIOPIA
			</OPTION>
			<OPTION value="FK"" <?= $_POST['country']=="FK"?"SELECTED":"" ?>>
			  FALKLAND ISLANDS (MALVINAS)
			</OPTION>
			<OPTION value="FO"" <?= $_POST['country']=="FO"?"SELECTED":"" ?>>
			  FAROE ISLANDS
			</OPTION>
			<OPTION value="FJ"" <?= $_POST['country']=="FJ"?"SELECTED":"" ?>>
			  FIJI
			</OPTION>
			<OPTION value="FI"" <?= $_POST['country']=="FI"?"SELECTED":"" ?>>
			  FINLAND
			</OPTION>
			<OPTION value="FR"" <?= $_POST['country']=="FR"?"SELECTED":"" ?>>
			  FRANCE
			</OPTION>
			<OPTION value="GF"" <?= $_POST['country']=="GF"?"SELECTED":"" ?>>
			  FRENCH GUIANA
			</OPTION>
			<OPTION value="PF"" <?= $_POST['country']=="PF"?"SELECTED":"" ?>>
			  FRENCH POLYNESIA
			</OPTION>
			<OPTION value="TF"" <?= $_POST['country']=="TF"?"SELECTED":"" ?>>
			  FRENCH SOUTHERN TERRITORIES
			</OPTION>
			<OPTION value="GA"" <?= $_POST['country']=="GA"?"SELECTED":"" ?>>
			  GABON
			</OPTION>
			<OPTION value="GM"" <?= $_POST['country']=="GM"?"SELECTED":"" ?>>
			  GAMBIA
			</OPTION>
			<OPTION value="GE"" <?= $_POST['country']=="GE"?"SELECTED":"" ?>>
			  GEORGIA
			</OPTION>
			<OPTION value="DE"" <?= $_POST['country']=="DE"?"SELECTED":"" ?>>
			  GERMANY
			</OPTION>
			<OPTION value="GH"" <?= $_POST['country']=="GH"?"SELECTED":"" ?>>
			  GHANA
			</OPTION>
			<OPTION value="GI"" <?= $_POST['country']=="GI"?"SELECTED":"" ?>>
			  GIBRALTAR
			</OPTION>
			<OPTION value="GR"" <?= $_POST['country']=="GR"?"SELECTED":"" ?>>
			  GREECE
			</OPTION>
			<OPTION value="GL"" <?= $_POST['country']=="GL"?"SELECTED":"" ?>>
			  GREENLAND
			</OPTION>
			<OPTION value="GD"" <?= $_POST['country']=="GD"?"SELECTED":"" ?>>
			  GRENADA
			</OPTION>
			<OPTION value="GP"" <?= $_POST['country']=="GP"?"SELECTED":"" ?>>
			  GUADELOUPE
			</OPTION>
			<OPTION value="GU"" <?= $_POST['country']=="GU"?"SELECTED":"" ?>>
			  GUAM
			</OPTION>
			<OPTION value="GT"" <?= $_POST['country']=="GT"?"SELECTED":"" ?>>
			  GUATEMALA
			</OPTION>
			<OPTION value="GN"" <?= $_POST['country']=="GN"?"SELECTED":"" ?>>
			  GUINEA
			</OPTION>
			<OPTION value="GW"" <?= $_POST['country']=="GW"?"SELECTED":"" ?>>
			  GUINEA-BISSAU
			</OPTION>
			<OPTION value="GY"" <?= $_POST['country']=="GY"?"SELECTED":"" ?>>
			  GUYANA
			</OPTION>
			<OPTION value="HT"" <?= $_POST['country']=="HT"?"SELECTED":"" ?>>
			  HAITI
			</OPTION>
			<OPTION value="HM"" <?= $_POST['country']=="HM"?"SELECTED":"" ?>>
			  HEARD ISLAND AND MCDONALD ISLANDS
			</OPTION>
			<OPTION value="HN"" <?= $_POST['country']=="HN"?"SELECTED":"" ?>>
			  HONDURAS
			</OPTION>
			<OPTION value="HK"" <?= $_POST['country']=="HK"?"SELECTED":"" ?>>
			  HONG KONG
			</OPTION>
			<OPTION value="HU"" <?= $_POST['country']=="HU"?"SELECTED":"" ?>>
			  HUNGARY
			</OPTION>
			<OPTION value="IS"" <?= $_POST['country']=="IS"?"SELECTED":"" ?>>
			  ICELAND
			</OPTION>
			<OPTION value="IN"" <?= $_POST['country']=="IN"?"SELECTED":"" ?>>
			  INDIA
			</OPTION>
			<OPTION value="ID"" <?= $_POST['country']=="ID"?"SELECTED":"" ?>>
			  INDONESIA
			</OPTION>
			<OPTION value="IR"" <?= $_POST['country']=="IR"?"SELECTED":"" ?>>
			  IRAN, ISLAMIC REPUBLIC OF
			</OPTION>
			<OPTION value="IQ"" <?= $_POST['country']=="IQ"?"SELECTED":"" ?>>
			  IRAQ
			</OPTION>
			<OPTION value="IE"" <?= $_POST['country']=="IE"?"SELECTED":"" ?>>
			  IRELAND
			</OPTION>
			<OPTION value="IL"" <?= $_POST['country']=="IL"?"SELECTED":"" ?>>
			  ISRAEL
			</OPTION>
			<OPTION value="IT"" <?= $_POST['country']=="IT"?"SELECTED":"" ?>>
			  ITALY
			</OPTION>
			<OPTION value="JM"" <?= $_POST['country']=="JM"?"SELECTED":"" ?>>
			  JAMAICA
			</OPTION>
			<OPTION value="JP"" <?= $_POST['country']=="JP"?"SELECTED":"" ?>>
			  JAPAN
			</OPTION>
			<OPTION value="JO"" <?= $_POST['country']=="JO"?"SELECTED":"" ?>>
			  JORDAN
			</OPTION>
			<OPTION value="KZ"" <?= $_POST['country']=="KZ"?"SELECTED":"" ?>>
			  KAZAKHSTAN
			</OPTION>
			<OPTION value="KE"" <?= $_POST['country']=="KE"?"SELECTED":"" ?>>
			  KENYA
			</OPTION>
			<OPTION value="KI"" <?= $_POST['country']=="KI"?"SELECTED":"" ?>>
			  KIRIBATI
			</OPTION>
			<OPTION value="KP"" <?= $_POST['country']=="KP"?"SELECTED":"" ?>>
			  KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF
			</OPTION>
			<OPTION value="KR"" <?= $_POST['country']=="KR"?"SELECTED":"" ?>>
			  KOREA, REPUBLIC OF
			</OPTION>
			<OPTION value="KW"" <?= $_POST['country']=="KW"?"SELECTED":"" ?>>
			  KUWAIT
			</OPTION>
			<OPTION value="KG"" <?= $_POST['country']=="KG"?"SELECTED":"" ?>>
			  KYRGYZSTAN
			</OPTION>
			<OPTION value="LA"" <?= $_POST['country']=="LA"?"SELECTED":"" ?>>
			  LAO PEOPLE'S DEMOCRATIC REPUBLIC
			</OPTION>
			<OPTION value="LV"" <?= $_POST['country']=="LV"?"SELECTED":"" ?>>
			  LATVIA
			</OPTION>
			<OPTION value="LB"" <?= $_POST['country']=="LB"?"SELECTED":"" ?>>
			  LEBANON
			</OPTION>
			<OPTION value="LS"" <?= $_POST['country']=="LS"?"SELECTED":"" ?>>
			  LESOTHO
			</OPTION>
			<OPTION value="LR"" <?= $_POST['country']=="LR"?"SELECTED":"" ?>>
			  LIBERIA
			</OPTION>
			<OPTION value="LY"" <?= $_POST['country']=="LY"?"SELECTED":"" ?>>
			  LIBYAN ARAB JAMAHIRIYA
			</OPTION>
			<OPTION value="LI"" <?= $_POST['country']=="LI"?"SELECTED":"" ?>>
			  LIECHTENSTEIN
			</OPTION>
			<OPTION value="LT"" <?= $_POST['country']=="LT"?"SELECTED":"" ?>>
			  LITHUANIA
			</OPTION>
			<OPTION value="LU"" <?= $_POST['country']=="LU"?"SELECTED":"" ?>>
			  LUXEMBOURG
			</OPTION>
			<OPTION value="MO"" <?= $_POST['country']=="MO"?"SELECTED":"" ?>>
			  MACAO
			</OPTION>
			<OPTION value="MK"" <?= $_POST['country']=="MK"?"SELECTED":"" ?>>
			  MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF
			</OPTION>
			<OPTION value="MG"" <?= $_POST['country']=="MG"?"SELECTED":"" ?>>
			  MADAGASCAR
			</OPTION>
			<OPTION value="MW"" <?= $_POST['country']=="MW"?"SELECTED":"" ?>>
			  MALAWI
			</OPTION>
			<OPTION value="MY"" <?= $_POST['country']=="MY"?"SELECTED":"" ?>>
			  MALAYSIA
			</OPTION>
			<OPTION value="MV"" <?= $_POST['country']=="MV"?"SELECTED":"" ?>>
			  MALDIVES
			</OPTION>
			<OPTION value="ML"" <?= $_POST['country']=="ML"?"SELECTED":"" ?>>
			  MALI
			</OPTION>
			<OPTION value="MT"" <?= $_POST['country']=="MT"?"SELECTED":"" ?>>
			  MALTA
			</OPTION>
			<OPTION value="MH"" <?= $_POST['country']=="MH"?"SELECTED":"" ?>>
			  MARSHALL ISLANDS
			</OPTION>
			<OPTION value="MQ"" <?= $_POST['country']=="MQ"?"SELECTED":"" ?>>
			  MARTINIQUE
			</OPTION>
			<OPTION value="MR"" <?= $_POST['country']=="MR"?"SELECTED":"" ?>>
			  MAURITANIA
			</OPTION>
			<OPTION value="MU"" <?= $_POST['country']=="MU"?"SELECTED":"" ?>>
			  MAURITIUS
			</OPTION>
			<OPTION value="YT"" <?= $_POST['country']=="YT"?"SELECTED":"" ?>>
			  MAYOTTE
			</OPTION>
			<OPTION value="MX"" <?= $_POST['country']=="MX"?"SELECTED":"" ?>>
			  MEXICO
			</OPTION>
			<OPTION value="FM"" <?= $_POST['country']=="FM"?"SELECTED":"" ?>>
			  MICRONESIA, FEDERATED STATES OF
			</OPTION>
			<OPTION value="MD"" <?= $_POST['country']=="MD"?"SELECTED":"" ?>>
			  MOLDOVA, REPUBLIC OF
			</OPTION>
			<OPTION value="MC"" <?= $_POST['country']=="MC"?"SELECTED":"" ?>>
			  MONACO
			</OPTION>
			<OPTION value="MN"" <?= $_POST['country']=="MN"?"SELECTED":"" ?>>
			  MONGOLIA
			</OPTION>
			<OPTION value="MS"" <?= $_POST['country']=="MS"?"SELECTED":"" ?>>
			  MONTSERRAT
			</OPTION>
			<OPTION value="MA"" <?= $_POST['country']=="MA"?"SELECTED":"" ?>>
			  MOROCCO
			</OPTION>
			<OPTION value="MZ"" <?= $_POST['country']=="MZ"?"SELECTED":"" ?>>
			  MOZAMBIQUE
			</OPTION>
			<OPTION value="MM"" <?= $_POST['country']=="MM"?"SELECTED":"" ?>>
			  MYANMAR
			</OPTION>
			<OPTION value="NA"" <?= $_POST['country']=="NA"?"SELECTED":"" ?>>
			  NAMIBIA
			</OPTION>
			<OPTION value="NR"" <?= $_POST['country']=="NR"?"SELECTED":"" ?>>
			  NAURU
			</OPTION>
			<OPTION value="NP"" <?= $_POST['country']=="NP"?"SELECTED":"" ?>>
			  NEPAL
			</OPTION>
			<OPTION value="NL"" <?= $_POST['country']=="NL"?"SELECTED":"" ?>>
			  NETHERLANDS
			</OPTION>
			<OPTION value="AN"" <?= $_POST['country']=="AN"?"SELECTED":"" ?>>
			  NETHERLANDS ANTILLES
			</OPTION>
			<OPTION value="NC"" <?= $_POST['country']=="NC"?"SELECTED":"" ?>>
			  NEW CALEDONIA
			</OPTION>
			<OPTION value="NZ"" <?= $_POST['country']=="NZ"?"SELECTED":"" ?>>
			  NEW ZEALAND
			</OPTION>
			<OPTION value="NI"" <?= $_POST['country']=="NI"?"SELECTED":"" ?>>
			  NICARAGUA
			</OPTION>
			<OPTION value="NE"" <?= $_POST['country']=="NE"?"SELECTED":"" ?>>
			  NIGER
			</OPTION>
			<OPTION value="NG"" <?= $_POST['country']=="NG"?"SELECTED":"" ?>>
			  NIGERIA
			</OPTION>
			<OPTION value="NU"" <?= $_POST['country']=="NU"?"SELECTED":"" ?>>
			  NIUE
			</OPTION>
			<OPTION value="NF"" <?= $_POST['country']=="NF"?"SELECTED":"" ?>>
			  NORFOLK ISLAND
			</OPTION>
			<OPTION value="MP"" <?= $_POST['country']=="MP"?"SELECTED":"" ?>>
			  NORTHERN MARIANA ISLANDS
			</OPTION>
			<OPTION value="NO"" <?= $_POST['country']=="NO"?"SELECTED":"" ?>>
			  NORWAY
			</OPTION>
			<OPTION value="OM"" <?= $_POST['country']=="OM"?"SELECTED":"" ?>>
			  OMAN
			</OPTION>
			<OPTION value="PK"" <?= $_POST['country']=="PK"?"SELECTED":"" ?>>
			  PAKISTAN
			</OPTION>
			<OPTION value="PW"" <?= $_POST['country']=="PW"?"SELECTED":"" ?>>
			  PALAU
			</OPTION>
			<OPTION value="PS"" <?= $_POST['country']=="PS"?"SELECTED":"" ?>>
			  PALESTINIAN TERRITORY, OCCUPIED
			</OPTION>
			<OPTION value="PA"" <?= $_POST['country']=="PA"?"SELECTED":"" ?>>
			  PANAMA
			</OPTION>
			<OPTION value="PG"" <?= $_POST['country']=="PG"?"SELECTED":"" ?>>
			  PAPUA NEW GUINEA
			</OPTION>
			<OPTION value="PY"" <?= $_POST['country']=="PY"?"SELECTED":"" ?>>
			  PARAGUAY
			</OPTION>
			<OPTION value="PE"" <?= $_POST['country']=="PE"?"SELECTED":"" ?>>
			  PERU
			</OPTION>
			<OPTION value="PH"" <?= $_POST['country']=="PH"?"SELECTED":"" ?>>
			  PHILIPPINES
			</OPTION>
			<OPTION value="PN"" <?= $_POST['country']=="PN"?"SELECTED":"" ?>>
			  PITCAIRN
			</OPTION>
			<OPTION value="PL"" <?= $_POST['country']=="PL"?"SELECTED":"" ?>>
			  POLAND
			</OPTION>
			<OPTION value="PT"" <?= $_POST['country']=="PT"?"SELECTED":"" ?>>
			  PORTUGAL
			</OPTION>
			<OPTION value="PR"" <?= $_POST['country']=="PR"?"SELECTED":"" ?>>
			  PUERTO RICO
			</OPTION>
			<OPTION value="QA"" <?= $_POST['country']=="QA"?"SELECTED":"" ?>>
			  QATAR
			</OPTION>
			<OPTION value="RE"" <?= $_POST['country']=="RE"?"SELECTED":"" ?>>
			  R�UNION
			</OPTION>
			<OPTION value="RO"" <?= $_POST['country']=="RO"?"SELECTED":"" ?>>
			  ROMANIA
			</OPTION>
			<OPTION value="RU"" <?= $_POST['country']=="RU"?"SELECTED":"" ?>>
			  RUSSIAN FEDERATION
			</OPTION>
			<OPTION value="RW"" <?= $_POST['country']=="RW"?"SELECTED":"" ?>>
			  RWANDA
			</OPTION>
			<OPTION value="SH"" <?= $_POST['country']=="SH"?"SELECTED":"" ?>>
			  SAINT HELENA
			</OPTION>
			<OPTION value="KN"" <?= $_POST['country']=="KN"?"SELECTED":"" ?>>
			  SAINT KITTS AND NEVIS
			</OPTION>
			<OPTION value="LC"" <?= $_POST['country']=="LC"?"SELECTED":"" ?>>
			  SAINT LUCIA
			</OPTION>
			<OPTION value="PM"" <?= $_POST['country']=="PM"?"SELECTED":"" ?>>
			  SAINT PIERRE AND MIQUELON
			</OPTION>
			<OPTION value="VC"" <?= $_POST['country']=="VC"?"SELECTED":"" ?>>
			  SAINT VINCENT AND THE GRENADINES
			</OPTION>
			<OPTION value="WS"" <?= $_POST['country']=="WS"?"SELECTED":"" ?>>
			  SAMOA
			</OPTION>
			<OPTION value="SM"" <?= $_POST['country']=="SM"?"SELECTED":"" ?>>
			  SAN MARINO
			</OPTION>
			<OPTION value="ST"" <?= $_POST['country']=="ST"?"SELECTED":"" ?>>
			  SAO TOME AND PRINCIPE
			</OPTION>
			<OPTION value="SA"" <?= $_POST['country']=="SA"?"SELECTED":"" ?>>
			  SAUDI ARABIA
			</OPTION>
			<OPTION value="SN"" <?= $_POST['country']=="SN"?"SELECTED":"" ?>>
			  SENEGAL
			</OPTION>
			<OPTION value="SC"" <?= $_POST['country']=="SC"?"SELECTED":"" ?>>
			  SEYCHELLES
			</OPTION>
			<OPTION value="SL"" <?= $_POST['country']=="SL"?"SELECTED":"" ?>>
			  SIERRA LEONE
			</OPTION>
			<OPTION value="SG"" <?= $_POST['country']=="SG"?"SELECTED":"" ?>>
			  SINGAPORE
			</OPTION>
			<OPTION value="SK"" <?= $_POST['country']=="SK"?"SELECTED":"" ?>>
			  SLOVAKIA
			</OPTION>
			<OPTION value="SI"" <?= $_POST['country']=="SI"?"SELECTED":"" ?>>
			  SLOVENIA
			</OPTION>
			<OPTION value="SB"" <?= $_POST['country']=="SB"?"SELECTED":"" ?>>
			  SOLOMON ISLANDS
			</OPTION>
			<OPTION value="SO"" <?= $_POST['country']=="SO"?"SELECTED":"" ?>>
			  SOMALIA
			</OPTION>
			<OPTION value="ZA"" <?= $_POST['country']=="ZA"?"SELECTED":"" ?>>
			  SOUTH AFRICA
			</OPTION>
			<OPTION value="GS"" <?= $_POST['country']=="GS"?"SELECTED":"" ?>>
			  SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS
			</OPTION>
			<OPTION value="ES"" <?= $_POST['country']=="ES"?"SELECTED":"" ?>>
			  SPAIN
			</OPTION>
			<OPTION value="LK"" <?= $_POST['country']=="LK"?"SELECTED":"" ?>>
			  SRI LANKA
			</OPTION>
			<OPTION value="SD"" <?= $_POST['country']=="SD"?"SELECTED":"" ?>>
			  SUDAN
			</OPTION>
			<OPTION value="SR"" <?= $_POST['country']=="SR"?"SELECTED":"" ?>>
			  SURINAME
			</OPTION>
			<OPTION value="SJ"" <?= $_POST['country']=="SJ"?"SELECTED":"" ?>>
			  SVALBARD AND JAN MAYEN
			</OPTION>
			<OPTION value="SZ"" <?= $_POST['country']=="SZ"?"SELECTED":"" ?>>
			  SWAZILAND
			</OPTION>
			<OPTION value="SE"" <?= $_POST['country']=="SE"?"SELECTED":"" ?>>
			  SWEDEN
			</OPTION>
			<OPTION value="CH"" <?= $_POST['country']=="CH"?"SELECTED":"" ?>>
			  SWITZERLAND
			</OPTION>
			<OPTION value="SY"" <?= $_POST['country']=="SY"?"SELECTED":"" ?>>
			  SYRIAN ARAB REPUBLIC
			</OPTION>
			<OPTION value="TW"" <?= $_POST['country']=="TW"?"SELECTED":"" ?>>
			  TAIWAN, PROVINCE OF CHINA
			</OPTION>
			<OPTION value="TJ"" <?= $_POST['country']=="TJ"?"SELECTED":"" ?>>
			  TAJIKISTAN
			</OPTION>
			<OPTION value="TZ"" <?= $_POST['country']=="TZ"?"SELECTED":"" ?>>
			  TANZANIA, UNITED REPUBLIC OF
			</OPTION>
			<OPTION value="TH"" <?= $_POST['country']=="TH"?"SELECTED":"" ?>>
			  THAILAND
			</OPTION>
			<OPTION value="TG"" <?= $_POST['country']=="TG"?"SELECTED":"" ?>>
			  TOGO
			</OPTION>
			<OPTION value="TK"" <?= $_POST['country']=="TK"?"SELECTED":"" ?>>
			  TOKELAU
			</OPTION>
			<OPTION value="TO"" <?= $_POST['country']=="TO"?"SELECTED":"" ?>>
			  TONGA
			</OPTION>
			<OPTION value="TT"" <?= $_POST['country']=="TT"?"SELECTED":"" ?>>
			  TRINIDAD AND TOBAGO
			</OPTION>
			<OPTION value="TN"" <?= $_POST['country']=="TN"?"SELECTED":"" ?>>
			  TUNISIA
			</OPTION>
			<OPTION value="TR"" <?= $_POST['country']=="TR"?"SELECTED":"" ?>>
			  TURKEY
			</OPTION>
			<OPTION value="TM"" <?= $_POST['country']=="TM"?"SELECTED":"" ?>>
			  TURKMENISTAN
			</OPTION>
			<OPTION value="TC"" <?= $_POST['country']=="TC"?"SELECTED":"" ?>>
			  TURKS AND CAICOS ISLANDS
			</OPTION>
			<OPTION value="TV"" <?= $_POST['country']=="TV"?"SELECTED":"" ?>>
			  TUVALU
			</OPTION>
			<OPTION value="UG"" <?= $_POST['country']=="UG"?"SELECTED":"" ?>>
			  UGANDA
			</OPTION>
			<OPTION value="UA"" <?= $_POST['country']=="UA"?"SELECTED":"" ?>>
			  UKRAINE
			</OPTION>
			<OPTION value="AE"" <?= $_POST['country']=="AE"?"SELECTED":"" ?>>
			  UNITED ARAB EMIRATES
			</OPTION>
			<OPTION value="GB"" <?= $_POST['country']=="GB"?"SELECTED":"" ?>>
			  UNITED KINGDOM
			</OPTION>
			<OPTION value="US"" <?= $_POST['country']=="US"?"SELECTED":"" ?>>
			  UNITED STATES OF AMERICA
			</OPTION>
			<OPTION value="UM"" <?= $_POST['country']=="UM"?"SELECTED":"" ?>>
			  UNITED STATES MINOR OUTLYING ISLANDS
			</OPTION>
			<OPTION value="UY"" <?= $_POST['country']=="UY"?"SELECTED":"" ?>>
			  URUGUAY
			</OPTION>
			<OPTION value="UZ"" <?= $_POST['country']=="UZ"?"SELECTED":"" ?>>
			  UZBEKISTAN
			</OPTION>
			<OPTION value="VU"" <?= $_POST['country']=="VU"?"SELECTED":"" ?>>
			  VANUATU
			</OPTION>
			<OPTION value="VA"" <?= $_POST['country']=="VA"?"SELECTED":"" ?>>
			  VATICAN CITY STATE
			</OPTION>
			<OPTION value="VE"" <?= $_POST['country']=="VE"?"SELECTED":"" ?>>
			  VENEZUELA
			</OPTION>
			<OPTION value="VN"" <?= $_POST['country']=="VN"?"SELECTED":"" ?>>
			  VIET NAM
			</OPTION>
			<OPTION value="VG"" <?= $_POST['country']=="VG"?"SELECTED":"" ?>>
			  VIRGIN ISLANDS, BRITISH
			</OPTION>
			<OPTION value="VI"" <?= $_POST['country']=="VI"?"SELECTED":"" ?>>
			  VIRGIN ISLANDS, U.S.
			</OPTION>
			<OPTION value="WF"" <?= $_POST['country']=="WF"?"SELECTED":"" ?>>
			  WALLIS AND FUTUNA
			</OPTION>
			<OPTION value="EH"" <?= $_POST['country']=="EH"?"SELECTED":"" ?>>
			  WESTERN SAHARA
			</OPTION>
			<OPTION value="YE"" <?= $_POST['country']=="YE"?"SELECTED":"" ?>>
			  YEMEN
			</OPTION>
			<OPTION value="YU"" <?= $_POST['country']=="YU"?"SELECTED":"" ?>>
			  YUGOSLAVIA
			</OPTION>
			<OPTION value="CD"" <?= $_POST['country']=="CD"?"SELECTED":"" ?>>
			  ZAIRE
			</OPTION>
			<OPTION value="ZM"" <?= $_POST['country']=="ZM"?"SELECTED":"" ?>>
			  ZAMBIA
			</OPTION>
			<OPTION value="ZW"" <?= $_POST['country']=="ZW"?"SELECTED":"" ?>>
			  ZIMBABWE
			</OPTION>
			</SELECT>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			Postal/Zip Code
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="postal_code" size="10" maxlength="10" name="postal_code" value="<?= $_POST['postal_code'] ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap valign="middle" height="2">
			<A name="Note1back">&nbsp;* Email address</A>
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="email" size="40" name="email" value="<?= $_POST['Note1back'] ?>">
		      </TD>
		      <TD align="left" width="10">
			<A href="#Note1" class="c2" tabindex="-1">Note&nbsp;1</A>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap valign="middle" height="2">
			* Phone number
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="phone" size="40" name="phone" value="<?= $_POST['phone'] ?>">
		      </TD>
		    </TR>
		    <TR>
                      <? // if keep_informed is neither "Yes" nor "No", check the box
		         if (! $_POST['keep_informed']) { $sel_keep_informed = "checked"; }
			 else { $sel_keep_informed = $_POST['keep_informed']=="Yes" ? "checked" : ""; } ?>
		      <TD align="left" colspan="2">
			&nbsp;&nbsp;&nbsp;&nbsp;Keep me informed on the progress of this election
			<INPUT type="checkbox" id="sel_keep_informed" name="sel_keep_informed" value="yes" <?= $sel_keep_informed ?>>
		      </TD>
		    </TR>
		    <TR>
		      <TD align=left colspan=2>
			<BR><B>The place you are registered to vote:</B>
		      </TD>
		    </TR>
		    <TR>
		      <TD valign="middle" height="2" align="right">
			<A name="Note1back">County or Parish</A>
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="reg_county" size="40" name="reg_county" value="<?= $_POST['reg_county'] ?>">
		      </TD>
		      <TD align="left" width="10">
			<A href="#Note2" class="c2" tabindex="-1">Note&nbsp;2</A>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* State
		      </TD>
		      <TD align="left">
			<SELECT name="reg_state">
			  <OPTION value="0" <?= $_POST['reg_state']=="0"?"SELECTED":"" ?>>
			    --select--
			  </OPTION>
			  <OPTION value="AL" <?= $_POST['reg_state']=="AL"?"SELECTED":"" ?>>
			    Alabama
			  </OPTION>
			  <OPTION value="AK" <?= $_POST['reg_state']=="AK"?"SELECTED":"" ?>>
			    Alaska
			  </OPTION>
			  <OPTION value="AZ" <?= $_POST['reg_state']=="AZ"?"SELECTED":"" ?>>
			    Arizona
			  </OPTION>
			  <OPTION value="AR" <?= $_POST['reg_state']=="AR"?"SELECTED":"" ?>>
			    Arkansas
			  </OPTION>
			  <OPTION value="CA" <?= $_POST['reg_state']=="CA"?"SELECTED":"" ?>>
			    California
			  </OPTION>
			  <OPTION value="CO" <?= $_POST['reg_state']=="CO"?"SELECTED":"" ?>>
			    Colorado
			  </OPTION>
			  <OPTION value="CT" <?= $_POST['reg_state']=="CT"?"SELECTED":"" ?>>
			    Connecticut
			  </OPTION>
			  <OPTION value="DE" <?= $_POST['reg_state']=="DE"?"SELECTED":"" ?>>
			    Delaware
			  </OPTION>
			  <OPTION value="DC" <?= $_POST['reg_state']=="DC"?"SELECTED":"" ?>>
			    District Of Columbia
			  </OPTION>
			  <OPTION value="FL" <?= $_POST['reg_state']=="FL"?"SELECTED":"" ?>>
			    Florida
			  </OPTION>
			  <OPTION value="GA" <?= $_POST['reg_state']=="GA"?"SELECTED":"" ?>>
			    Georgia
			  </OPTION>
			  <OPTION value="HI" <?= $_POST['reg_state']=="HI"?"SELECTED":"" ?>>
			    Hawaii
			  </OPTION>
			  <OPTION value="ID" <?= $_POST['reg_state']=="ID"?"SELECTED":"" ?>>
			    Idaho
			  </OPTION>
			  <OPTION value="IL" <?= $_POST['reg_state']=="IL"?"SELECTED":"" ?>>
			    Illinois
			  </OPTION>
			  <OPTION value="IN" <?= $_POST['reg_state']=="IN"?"SELECTED":"" ?>>
			    Indiana
			  </OPTION>
			  <OPTION value="IA" <?= $_POST['reg_state']=="IA"?"SELECTED":"" ?>>
			    Iowa
			  </OPTION>
			  <OPTION value="KS" <?= $_POST['reg_state']=="KS"?"SELECTED":"" ?>>
			    Kansas
			  </OPTION>
			  <OPTION value="KY" <?= $_POST['reg_state']=="KY"?"SELECTED":"" ?>>
			    Kentucky
			  </OPTION>
			  <OPTION value="LA" <?= $_POST['reg_state']=="LA"?"SELECTED":"" ?>>
			    Louisiana
			  </OPTION>
			  <OPTION value="ME" <?= $_POST['reg_state']=="ME"?"SELECTED":"" ?>>
			    Maine
			  </OPTION>
			  <OPTION value="MD" <?= $_POST['reg_state']=="MD"?"SELECTED":"" ?>>
			    Maryland
			  </OPTION>
			  <OPTION value="MA" <?= $_POST['reg_state']=="MA"?"SELECTED":"" ?>>
			    Massachusetts
			  </OPTION>
			  <OPTION value="MI" <?= $_POST['reg_state']=="MI"?"SELECTED":"" ?>>
			    Michigan
			  </OPTION>
			  <OPTION value="MN" <?= $_POST['reg_state']=="MN"?"SELECTED":"" ?>>
			    Minnesota
			  </OPTION>
			  <OPTION value="MS" <?= $_POST['reg_state']=="MS"?"SELECTED":"" ?>>
			    Mississippi
			  </OPTION>
			  <OPTION value="MO" <?= $_POST['reg_state']=="MO"?"SELECTED":"" ?>>
			    Missouri
			  </OPTION>
			  <OPTION value="MT" <?= $_POST['reg_state']=="MT"?"SELECTED":"" ?>>
			    Montana
			  </OPTION>
			  <OPTION value="NE" <?= $_POST['reg_state']=="NE"?"SELECTED":"" ?>>
			    Nebraska
			  </OPTION>
			  <OPTION value="NV" <?= $_POST['reg_state']=="NV"?"SELECTED":"" ?>>
			    Nevada
			  </OPTION>
			  <OPTION value="NH" <?= $_POST['reg_state']=="NH"?"SELECTED":"" ?>>
			    New Hampshire
			  </OPTION>
			  <OPTION value="NJ" <?= $_POST['reg_state']=="NJ"?"SELECTED":"" ?>>
			    New Jersey
			  </OPTION>
			  <OPTION value="NM" <?= $_POST['reg_state']=="NM"?"SELECTED":"" ?>>
			    New Mexico
			  </OPTION>
			  <OPTION value="NY" <?= $_POST['reg_state']=="NY"?"SELECTED":"" ?>>
			    New York
			  </OPTION>
			  <OPTION value="NC" <?= $_POST['reg_state']=="NC"?"SELECTED":"" ?>>
			    North Carolina
			  </OPTION>
			  <OPTION value="ND" <?= $_POST['reg_state']=="ND"?"SELECTED":"" ?>>
			    North Dakota
			  </OPTION>
			  <OPTION value="OH" <?= $_POST['reg_state']=="OH"?"SELECTED":"" ?>>
			    Ohio
			  </OPTION>
			  <OPTION value="OK" <?= $_POST['reg_state']=="OK"?"SELECTED":"" ?>>
			    Oklahoma
			  </OPTION>
			  <OPTION value="OR" <?= $_POST['reg_state']=="OR"?"SELECTED":"" ?>>
			    Oregon
			  </OPTION>
			  <OPTION value="PA" <?= $_POST['reg_state']=="PA"?"SELECTED":"" ?>>
			    Pennsylvania
			  </OPTION>
			  <OPTION value="RI" <?= $_POST['reg_state']=="RI"?"SELECTED":"" ?>>
			    Rhode Island
			  </OPTION>
			  <OPTION value="SC" <?= $_POST['reg_state']=="SC"?"SELECTED":"" ?>>
			    South Carolina
			  </OPTION>
			  <OPTION value="SD" <?= $_POST['reg_state']=="SD"?"SELECTED":"" ?>>
			    South Dakota
			  </OPTION>
			  <OPTION value="TN" <?= $_POST['reg_state']=="TN"?"SELECTED":"" ?>>
			    Tennessee
			  </OPTION>
			  <OPTION value="TX" <?= $_POST['reg_state']=="TX"?"SELECTED":"" ?>>
			    Texas
			  </OPTION>
			  <OPTION value="UT" <?= $_POST['reg_state']=="UT"?"SELECTED":"" ?>>
			    Utah
			  </OPTION>
			  <OPTION value="VT" <?= $_POST['reg_state']=="VT"?"SELECTED":"" ?>>
			    Vermont
			  </OPTION>
			  <OPTION value="VA" <?= $_POST['reg_state']=="VA"?"SELECTED":"" ?>>
			    Virginia
			  </OPTION>
			  <OPTION value="WA" <?= $_POST['reg_state']=="WA"?"SELECTED":"" ?>>
			    Washington
			  </OPTION>
			  <OPTION value="WV" <?= $_POST['reg_state']=="WV"?"SELECTED":"" ?>>
			    West Virginia
			  </OPTION>
			  <OPTION value="WI" <?= $_POST['reg_state']=="WI"?"SELECTED":"" ?>>
			    Wisconsin
			  </OPTION>
			  <OPTION value="WY" <?= $_POST['reg_state']=="WY"?"SELECTED":"" ?>>
			    Wyoming
			  </OPTION>
			  <OPTION value="AS" <?= $_POST['reg_state']=="AS"?"SELECTED":"" ?>>
			    American Samoa
			  </OPTION>
			  <OPTION value="AE" <?= $_POST['reg_state']=="AE"?"SELECTED":"" ?>>
			    Armed Forces Other
			  </OPTION>
			  <OPTION value="AA" <?= $_POST['reg_state']=="AA"?"SELECTED":"" ?>>
			    Armed Forces Americas
			  </OPTION>
			  <OPTION value="AP" <?= $_POST['reg_state']=="AP"?"SELECTED":"" ?>>
			    Armed Forces Pacific
			  </OPTION>
			  <OPTION value="FM" <?= $_POST['reg_state']=="FM"?"SELECTED":"" ?>>
			    Federated States of Micronesia
			  </OPTION>
			  <OPTION value="GU" <?= $_POST['reg_state']=="GU"?"SELECTED":"" ?>>
			    Guam
			  </OPTION>
			  <OPTION value="MH" <?= $_POST['reg_state']=="MH"?"SELECTED":"" ?>>
			    Marshall Islands
			  </OPTION>
			  <OPTION value="MP" <?= $_POST['reg_state']=="MP"?"SELECTED":"" ?>>
			    Northern Mariana Islands
			  </OPTION>
			  <OPTION value="PW" <?= $_POST['reg_state']=="PW"?"SELECTED":"" ?>>
			    Palau
			  </OPTION>
			  <OPTION value="PR" <?= $_POST['reg_state']=="PR"?"SELECTED":"" ?>>
			    Puerto Rico
			  </OPTION>
			  <OPTION value="VI" <?= $_POST['reg_state']=="VI"?"SELECTED":"" ?>>
			    Virgin Islands
			  </OPTION>
			</SELECT>
		      </TD>
		    </TR>
		    <TR>
                      <? // check the box if it was previously set
		         if (! $_POST['not_registered']) { $sel_not_registered = ""; }
			 else { $sel_not_registered = $_POST['not_registered']=="true" ? "checked" : ""; } ?>
		      <TD align="left" colspan="2">
			&nbsp;&nbsp;&nbsp;&nbsp;I am not currently registered to vote
			<INPUT type="checkbox" id="not_registered" name="not_registered" value="true" <?= $sel_not_registered ?>>
		      </TD>
		    </TR>
		    <TR>
		      <TD align=left colspan=2>
			<BR><B>Additional information used to validate your identity:</B>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* City, Town of birth
		      </TD>
		      <TD align="left">
			<INPUT type="text" size="40" id="birth_city" name="birth_city" value="<?= $_POST['birth_city'] ?>">
		      </TD>
		      <TD align="left" width="10">
			<A href="#Note3" class="c2" tabindex="-1">Note&nbsp;3</A>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			** State of birth
		      </TD>
		      <TD align="left">
			<INPUT type="text" size="40" id="birth_state" name="birth_state" value="<?= $_POST['birth_state'] ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* Country of birth
		      </TD>
		      <TD align="left">
			<INPUT type="text" size="40" id="birth_country" name="birth_country" value="<?= $_POST['birth_country']?$_POST['birth_country']:'USA'; ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap valign="top">
			* Birth date
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="birth_date" name="birth_date" value="<?= $_POST['birth_date'] ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD>
		      </TD>
		      <TD align="left">
			mm/dd/yyyy (e.g.: 01/15/1944)<BR>
			(must be at least 18 years old)
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap valign="top">
		       * Select a question
		      </TD>
		      <TD align="left">
			<INPUT type="hidden" id="question" name="question">
			<SELECT name="selQuestion">
			  <OPTION value="0" <?= $_POST['selQuestion']=="0"?"SELECTED":"" ?>>
			    --select--
			  </OPTION>
			  <OPTION value="1" <?= $_POST['selQuestion']=="1"?"SELECTED":"" ?>>
			    Favorite pet
			  </OPTION>
			  <OPTION value="2" <?= $_POST['selQuestion']=="2"?"SELECTED":"" ?>>
			    Favorite drink
			  </OPTION>
			  <OPTION value="3" <?= $_POST['selQuestion']=="3"?"SELECTED":"" ?>>
			    Favorite show
			  </OPTION>
			  <OPTION value="4" <?= $_POST['selQuestion']=="4"?"SELECTED":"" ?>>
			    Mother's maiden name
			  </OPTION>
			  <OPTION value="5" <?= $_POST['selQuestion']=="5"?"SELECTED":"" ?>>
			    Place where you grew up
			  </OPTION>
			  <OPTION value="6" <?= $_POST['selQuestion']=="6"?"SELECTED":"" ?>>
			    Childhood best friend
			  </OPTION>
			  <OPTION value="7" <?= $_POST['selQuestion']=="7"?"SELECTED":"" ?>>
			    A password or phrase
			  </OPTION>
			</SELECT>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap valign="top">
			* Your response
		      </TD>
		      <TD align="left">
			<INPUT type="text" size=40 id="response" name="response" value="<?= $_POST['response'] ?>">
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10">
			&nbsp; * required field &nbsp; ** required within the US
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10">
			<TABLE>
			  <TBODY>
			    <TR>
			      <TD valign="top" nowrap>
				<A name="Note1">&nbsp;</A>
				<B>Note 1:</B>
			      </TD>
			      <TD>
				Your phone number and email are vital to process your Registration. To protect your vote, we must validate your identity as a registered voter for government elections; this gives you the right to vote in this Philadelphia II Election. Validating your identity will permit us to count your vote in the final tally. <A href="#Note1back" class="c2">back</A>
			      </TD>
			     </TR>
			    </TBODY>
			  </TABLE>
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10">
			<TABLE>
			  <TBODY>
			    <TR>
			      <TD valign="top" nowrap>
				<A name="Note2">&nbsp;</A>
				<B>Note 2:</B>
			      </TD>
			      <TD>
				Enter your State (or US Territory) and County or Parish of residence (where you are registered to vote in government elections). If you are not registered in your county or parish or have just come of age to vote (18 years old.), please continue to register here. We will help you to register. If you live outside the United States, please enter here the State and County or Parish where you last voted in the United States. If you are a US Citizen who has never lived in the United States and you have just come of age to vote, please enter the State and County or Parish where one of your parents are registered to vote in US elections.<A href="#Note2back" class="c2">back</A>
			      </TD>
			     </TR>
			    </TBODY>
			  </TABLE>
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10" align="left">
			<TABLE>
			  <TBODY>
			    <TR>
			      <TD valign="top" nowrap>
				<A name="Note3">&nbsp;</A>
				<B>Note 3:</B>
			      </TD>
			      <TD>
				The place you were born is used to uniquely identify you within your voting district.  Enter the place you were born, for example the name of the city, town that is marked on your birth certificate.  Enter the two letter abbreviation for the state you were born in.  If you were not born in the United States or one of its territories, enter the name of the country you were born in.<A href="#Note3back" class="c2">back</A>
			      </TD>
			     </TR>
			    </TBODY>
			  </TABLE>
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10">&nbsp;
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" align="center">
			<INPUT type="submit" name="submit" value="Continue" class="Button" onClick="return Validate(this.form);">
		      </TD>
		    </TR>
		  </TBODY>
		</TABLE>

      </FORM>
    <?php include("bottom.htm"); ?>    
  </BODY>
  </BODY>
</HTML>

