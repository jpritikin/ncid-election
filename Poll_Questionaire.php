<?
  // initiate a session
  session_start();
  header("Cache-control: private");
  session_register("POLL_SESSION");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
  <TITLE>Philadelphia II</TITLE>
  <META content="text/html; charset=iso-8859-1">
  <LINK rel="stylesheet"
        type="text/css"
        href="Poll_Questionaire.css">
  <link href="style.css" rel="stylesheet" type="text/css">
</HEAD>

<BODY topmargin="2">
<?php include("top.htm"); ?>

                  <TABLE width="100%"
                         height="100%"
                         cellspacing="0"
                         cellpadding="0"
                         border="0">
                      <TR>
                        <TD align="center"
                            valign="middle">
                          <!-- outline the top of the window, this contains the logo and poll info -->

                          
                        </TD>
                      </TR>

                      <TR>
                        <TD>
                          <FORM method="post"
                                id="poll"
                                name="poll"
                                action="Poll_Submit.php">
                            <TABLE width="90%"
                                   align="center"
                                   cellpadding="2"
                                   cellspacing="0"
                                   border="0"
                                   bordercolor="blue"
                                   bgcolor="#ffffff">
                                <TR>
                                  <TD valign="middle">
                                    <TABLE width="100%"
                                           cellpadding="2"
                                           cellspacing="0"
                                           border="0"
                                           bordercolor="green">
                                        <TR>
                                          <TD align="center"
                                              valign="middle">
                                            <DIV class="poll-name">
					      <BR>
                                              <B>National Initiative For Democracy<B><BR>
					      Exit Poll<BR>
					      &nbsp;
                                            </DIV>
                                          </TD>
                                        </TR>

                                        <TR>
                                          <TD align=""
                                              valign="middle">
                                            <p class="question-name"><B>This is an anonymous survey. Moreover, you may skip any question that you prefer not to answer. </B></p>
                                            <p class="question-name"><B>Where did you first learn about the National Initiative?</B></p></TD>
                                        </TR>

                                        <TR>
                                          <TD>
                                            <TABLE align=""
                                                   cellpadding="2"
                                                   cellspacing="0"
                                                   border="0"
                                                   bordercolor="#CCCCCC">
                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-1"
                                                         id="question-1-1"
                                                         value="1"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">TV or radio</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-1"
                                                         id="question-1-2"
                                                         value="2"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Newspaper or magazine</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-1"
                                                         id="question-1-3"
                                                         value="3"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Internet</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-1"
                                                         id="question-1-4"
                                                         value="4"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">A friend or acquaintance&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-1"
                                                         id="question-1-W1"
                                                         value="W1"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Other &nbsp;&nbsp;</SPAN>
												  </TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>
                                            </TABLE>
                                          </TD>
                                        </TR>
<TR>
                                        <TD align=""
                                              valign="middle"><SPAN class="question-name"><B>Could please tell us which website, magazine, TV, radio show or other source which led you to the National Initiative:<br>
                                          <br>
                                        </B></SPAN>
                                          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                              <td width="4%">&nbsp;</td>
                                              <td width="96%"><SPAN class="question-name"><B>
                                                <input
                                                         name="question-1-W1-writein" type="text"
                                                         class="WriteIn"
                                                         id="question-1-W1-writein"
                                                         value="" size="100"
                                                         maxlength="100">
                                              </B></SPAN></td>
                                            </tr>
                                          </table>
                              <SPAN class="question-name"><B>                                          </B></SPAN></TD>
                                      </TR>
										
                                        <TR>
                                          <TD align=""
                                              valign="middle">
                                            <SPAN class="question-name"><B>Do you feel we have a government "by the people"?</B></SPAN></TD>
                                        </TR>

                                        <TR>
                                          <TD>
                                            <TABLE align=""
                                                   cellpadding="2"
                                                   cellspacing="0"
                                                   border="0"
                                                   bordercolor="#CCCCCC">
                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-3"
                                                         id="question-3-1"
                                                         value="1"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Yes&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-3"
                                                         id="question-3-2"
                                                         value="2"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">No&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-3"
                                                         id="question-3-3"
                                                         value="3"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Not sure&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>
                                            </TABLE>
                                          </TD>
                                        </TR>
										
                                        <TR>
                                          <TD align=""
                                              valign="middle">
                                            <SPAN class="question-name"><B>Did you vote For or Against the National Initiative? You are anonymous, you do not need to answer this or any of these questions if you prefer not to answer. </B></SPAN></TD>
                                        </TR>

                                        <TR>
                                          <TD>
                                            <TABLE align=""
                                                   cellpadding="2"
                                                   cellspacing="0"
                                                   border="0"
                                                   bordercolor="#CCCCCC">
                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-9"
                                                         id="question-9-1"
                                                         value="1"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">For&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-9"
                                                         id="question-9-2"
                                                         value="2"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Against&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                            </TABLE>
                                          </TD>
                                        </TR>

                                        <TR>
                                          <TD align=""
                                              valign="middle"><SPAN class="question-name"><B>Why did you vote for (or against) the National Initiative?</B></SPAN></TD>
                                        </TR>

                                        <TR>
                                          <TD>
                                            <TABLE align=""
                                                   cellpadding="2"
                                                   cellspacing="0"
                                                   border="0"
                                                   bordercolor="#CCCCCC">
                                                <TR>
                                                  <TD align="center"
                                                      valign="middle"></TD>

                                                  <TD align="left"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                  <TEXTAREA cols="30"
                                                            rows="2"
                                                            name="question-2-W1"
                                                            id="question-2-W1">
</TEXTAREA></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>
                                            </TABLE>
                                          </TD>
                                        </TR>

                                    </TABLE>

                                    <TABLE width="100%"
                                           cellpadding="2"
                                           cellspacing="0"
                                           border="0"
                                           bordercolor="green">
                                        <TR>
                                          <TD align="center"
                                              valign="middle">
                                            <DIV class="poll-name">
                                              <B>Demographics</B>
                                            </DIV><BR>
                                            <SPAN class="text">Please provide the following demographic information. It will only be used to make statistical comparisons between groups; individuals records will not be published.</SPAN>
                                          </TD>
                                        </TR>

                                        <TR>
                                          <TD align=""
                                              valign="middle">
                                            <SPAN class="question-name"><B>What is your gender?</B></SPAN></TD>
                                        </TR>

                                        <TR>
                                          <TD>
                                            <TABLE align=""
                                                   cellpadding="2"
                                                   cellspacing="0"
                                                   border="0"
                                                   bordercolor="#CCCCCC">
                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-4"
                                                         id="question-4-1"
                                                         value="1"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Male &nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-4"
                                                         id="question-4-2"
                                                         value="2"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Female &nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>
                                            </TABLE>
                                          </TD>
                                        </TR>

                                        <TR>
                                          <TD align=""
                                              valign="middle">
                                            <SPAN class="question-name"><B>How old are you?</B></SPAN></TD>
                                        </TR>

                                        <TR>
                                          <TD>
                                            <TABLE align=""
                                                   cellpadding="2"
                                                   cellspacing="0"
                                                   border="0"
                                                   bordercolor="#CCCCCC">
                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-5"
                                                         id="question-5-1"
                                                         value="1"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Under 21 years&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-5"
                                                         id="question-5-2"
                                                         value="2"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">21 - 40&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-5"
                                                         id="question-5-3"
                                                         value="3"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">41 - 60&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-5"
                                                         id="question-5-4"
                                                         value="4"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">61+&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>
                                            </TABLE>
                                          </TD>
                                        </TR>

                                        <TR>
                                          <TD align=""
                                              valign="middle">
                                            <SPAN class="question-name"><B>Which of the following best represents the highest level of education that you have completed?</B></SPAN></TD>
                                        </TR>

                                        <TR>
                                          <TD>
                                            <TABLE align=""
                                                   cellpadding="2"
                                                   cellspacing="0"
                                                   border="0"
                                                   bordercolor="#CCCCCC">
                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-6"
                                                         id="question-6-1"
                                                         value="1"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">High School&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-6"
                                                         id="question-6-2"
                                                         value="2"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">College&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-6"
                                                         id="question-6-3"
                                                         value="3"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Post-Bachelors&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>
                                            </TABLE>
                                          </TD>
                                        </TR>

                                        <TR>
                                          <TD align=""
                                              valign="middle">
                                            <SPAN class="question-name"><B>Which of the following best describes your total household income before taxes last year?</B></SPAN></TD>
                                        </TR>

                                        <TR>
                                          <TD>
                                            <TABLE align=""
                                                   cellpadding="2"
                                                   cellspacing="0"
                                                   border="0"
                                                   bordercolor="#CCCCCC">
                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-7"
                                                         id="question-7-1"
                                                         value="1"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Under $20,000&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-7"
                                                         id="question-7-2"
                                                         value="2"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">$20,000 - $44,999&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-7"
                                                         id="question-7-3"
                                                         value="3"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">$45,000 - $74,999&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-7"
                                                         id="question-7-4"
                                                         value="4"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">$75,000 - $149,999&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-7"
                                                         id="question-7-5"
                                                         value="5"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">$150,000+&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>
                                            </TABLE>
                                          </TD>
                                        </TR>

                                        <TR>
                                          <TD align=""
                                              valign="middle">
                                            <SPAN class="question-name"><B>Which political group do you identify with the most?</B></SPAN></TD>
                                        </TR>

                                        <TR>
                                          <TD>
                                            <TABLE align=""
                                                   cellpadding="2"
                                                   cellspacing="0"
                                                   border="0"
                                                   bordercolor="#CCCCCC">
                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-8"
                                                         id="question-8-1"
                                                         value="1"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Democrat&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-8"
                                                         id="question-8-2"
                                                         value="2"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Republican&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-8"
                                                         id="question-8-3"
                                                         value="3"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Libertarian&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-8"
                                                         id="question-8-4"
                                                         value="4"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Reform Party&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-8"
                                                         id="question-8-4"
                                                         value="5"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Green Party&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-8"
                                                         id="question-8-6"
                                                         value="6"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Independent&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-8"
                                                         id="question-8-7"
                                                         value="7"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">Other&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>

                                                <TR>
                                                  <TD align="center"
                                                      valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <INPUT type="radio"
                                                         class="BigRadio"
                                                         name="question-8"
                                                         id="question-8-8"
                                                         value="8"></TD>

                                                  <TD align="left"
                                                      valign="middle">
                                                    <SPAN class="response-name">None&nbsp;</SPAN></TD>

                                                  <TD align="left"
                                                      valign="middle"></TD>
                                                </TR>
                                            </TABLE>
                                          </TD>
                                        </TR>
                                    </TABLE>
                                  </TD>
                                </TR>

                                <TR>
                                  <TD>
                                    <TABLE align="center"
                                           cellpadding="3"
                                           cellspacing="0"
                                           border="0"
                                           bordercolor="#006699"
                                           bgcolor="white">
                                        <TR>
                                          <TD align="center"
                                              valign="middle"
                                              bgcolor="#ffffff">
					    <INPUT type="submit" name="submit" value="Submit" class="Button">
                                        </TR>
                                    </TABLE>
                                  </TD>
                                </TR>
                            </TABLE>
                          </FORM>
                        </TD>
                      </TR>

                      <TR>
                        <TD><p>All information is optional and there is no information identifying you. Thank you! </p>
                          </p></TD>
                      </TR>

                  </TABLE>

  <?php include("bottom.htm"); ?>
</BODY>
</HTML>
