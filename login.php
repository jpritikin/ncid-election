<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<?
require 'vars.php';
require 'mgmail.php';

// login.php - performs validation

// make sure we start with a clean session
session_start();
header("Cache-control: private");
session_destroy();

function clean($input) {
    $input = stripslashes($input);
    $input = str_replace("'", "''", $input);
    $input = str_replace("\\", "", $input);
    return($input);
}

$voter_id = clean($_POST["voter_id"]);
$password = clean($_POST["password"]);

if (!$voter_id) {
    $voter_id = clean($_GET["voter_id"]);
}
if (!$password) {
    $password = clean($_GET["password"]);
}

if (strlen($voter_id) && strlen($password)) {

    // authenticate using form variables
    list($status,$name,$email,$vote) = authenticate($voter_id, $password);

    // if  user/pass combination is correct
    if ($status == 1)
    {
	// initiate a session
	session_start();
	header("Cache-control: private");
	session_register("SESSION");

	$_SESSION['voter_id'] = $voter_id;
	$_SESSION['password'] = $password;
	$_SESSION['vote'] = $vote;
	$_SESSION['name'] = $name;
	$_SESSION['email'] = $email;

	// redirect to protected page
	header("Location: Ballot.php");
	exit();
    }
    else
    // user/pass check failed
    {
	// redirect to error page
	header("Location: InvalidLogin.php?e=$status");
	exit();
    }

}

// authenticate username/password against /etc/passwd
// returns:  0 if user does not exist or password is incorrect
//           1 if username and password are correct
function authenticate($voter_id, $password)
{
    global $sql_host, $sql_user, $sql_pass, $sql_db, $voter_table;

    $auth = 0;

    mysql_connect ($sql_host, $sql_user, $sql_pass);

    mysql_select_db ($sql_db);

    $query = "SELECT voter_id,password,title,first_name,
                     middle_name,last_name,suffix,email,latest_vote
	      FROM $voter_table
	      WHERE voter_id='$voter_id' AND password='$password'";
    $result = mysql_query ($query);
    $error = mysql_error();

    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
	$auth = 1;
	$vote = strtoupper($row['latest_vote']);
	// should be YES/NO/ABSTAIN, but just in case...
	if ($vote == "Y") { $vote = "YES"; }
	if ($vote == "N") { $vote = "NO"; }
	if ($vote == "A") { $vote = "ABSTAIN"; }
	$name = $row['title']." ".$row['first_name']." ".$row['middle_name']." ".$row['last_name']." ".$row['suffix'];
	$name=str_replace("  "," ",$name);
	$email = $row['email'];
	break;
    }

    if (!$result || !$auth) {
        // some error occured
        $msg = "mysql_error=$error\n\nquery=$query";
        $hdrs = "From: Vote.php@votep2.us\r\n";
        mgmail("mgrant@grant.org", "voter login error", $msg, $hdrs);
    }
    mysql_free_result($result);

    // return value
    return array($auth, $name, $email, $vote);
}

?>
<html>
<HEAD>
<TITLE>Vote on The National Initiative - Login</TITLE>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META name="author" content="Philadelphia II">
<META name="publisher" content="www.votep2.us">
<META name="keywords" content="vote, national initiative, initiative, democracy, direct democracy, first amendment, US, constitution, constitutional amendment, ratify, ratification, election, we the people, law, legislation, legislative power, Mike Gravel">
<META name="description" content="This an election held by the people to enact The National Initiative into law. The National Initiative gives Americans the power to enact laws.">
<META name="audience" content="all">
<link href="style.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 100%}
.style3 {
	font-size: 100%;
	font-weight: bold;
}
-->
</style>
</HEAD>
<BODY>
<?php include("menu-anon.htm"); ?>
<?php include("top.htm"); ?>
          <h1><STRONG>Login / Register</STRONG> </h1>
          <blockquote>
            <p class="style1">This is the election to vote on the National Initiative.
              If enacted, the National Initiative will provide Americans with an orderly procedure to enact laws by initiative. </p>
            <p><span class="style3">Any American who is eligible to vote in a state run election is eligible to vote on the National Initiative</span>. </p>
            <p>The information you enter on this site is kept private, not resold or disclosed, and your email address is not used for email solicitation (&quot;spam&quot;). The information you enter will be manually checked against your public voter registration information in your state of legal residence to certify that you were eligible to have voted. Please read our <a href="Privacy_Policy.php">Privacy Policy</a>. </p>
            <DIV>
              <h3 align="left">1. If you haven't already <a href="Summary.php" align="left">learn what you are voting on</a></h3>
                  <h3 align="left">2. Then <a href="EditNewVoter.php" align="left">click here to register to vote on this initiative </a></h3>
                  <hr>
              <h3 align="left">Already registered? To change your vote or update registration info</h3>
              <BLOCKQUOTE class="style1">
                <FORM action="login.php" method="post" id="login" name="login">
                  <TABLE border="0" cellpadding="0" cellspacing="10">
                    <TBODY>
                      <TR>
                        <TD align="center"><STRONG>Voter ID:</STRONG> </TD>
                        <TD align="left"><INPUT type="text" name="voter_id"></TD>
                        <TD colspan="3">or</TD>
                        <TD rowspan="3" valign="top"><STRONG>Can't remember your <br>
                          username or password?<BR>
                        Click <A href="EditUserPassSearch.php">here.</A></STRONG> </TD>
                      </TR>
                      <TR>
                        <TD align="right"><STRONG>Password:</STRONG> </TD>
                        <TD align="left"><INPUT type="password" name="password"></TD>
                        <TD colspan="3">&nbsp; </TD>
                      </TR>
                      <TR>
                        <TD colspan="2" valign="middle" height="45" align="center"><DIV class="c9">
                            <INPUT type="submit" name="submit3" value="Login" class="Button">
                          </DIV></TD>
                      </TR>
                    </TBODY>
                  </TABLE>
                </FORM>
              </BLOCKQUOTE>
            </DIV>
          </blockquote>          <?php include("bottom.htm"); ?>
</BODY>
</html>
