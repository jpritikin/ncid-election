<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<SCRIPT language="JavaScript" type="text/javascript">

function check_date(field)
{
  var checkstr = "0123456789";
  var DateField = field;
  var Datevalue = "";
  var DateTemp = "";
  var separator = ".";
  var seperator = "/";
  var month;
  var day;
  var year;
  var leap = 0;
  var err = 0;
  var i;
  err = 0;
  DateValue = DateField.value;
  var x = DateValue;
  var arr = x.split("/");

  if ( arr.length == 3)
  {
        if (DateValue.length != 10)
        {

                if (arr[0].length != 2 )
                {
                    arr[0]= "0" + arr[0];
                }
                if (arr[1].length != 2 )
                {
                    arr[1]= "0" + arr[1];
                }
                if (arr[2].length == 2 )
                {
                    arr[2]= "19" + arr[2];
                }

                DateValue = arr[0] + "/" + arr[1] + "/" + arr[2] ;
        }
   }
   /* Delete all chars except 0..9 */
   for (i = 0; i < DateValue.length; i++)
   {
      if (checkstr.indexOf(DateValue.substr(i,1)) >= 0)
      {
         DateTemp = DateTemp + DateValue.substr(i,1);
      }
   }
   DateValue = DateTemp;
   /* Always change date to 8 digits - string*/
   /* if year is entered as 2-digit / always assume 20xx */
   /* if (DateValue.length == 6) {
      DateValue = DateValue.substr(0,4) + '19' + DateValue.substr(4,2); }
   */

   if (DateValue.length != 8) {
   alert("Please enter your birth date in mm/dd/yyyy format.");
      DateField.select();
      DateField.focus();
      return false;
      //err = 19;
      }
   /* year is wrong if year = 0000 */
   year = DateValue.substr(4,4);
   if (year == 0) {
      err = 20;
   }

    year = DateValue.substr(4,4);
      month = DateValue.substr(0,2);
     day = DateValue.substr(2,2);

    var d = new Date();
   if ((d.getFullYear() - year < 18)){ err = 101;}

   if ((d.getFullYear() - year == 18)){
      if( parseInt(month) > (d.getMonth()+ 1)){err = 101;}
      if ( parseInt(month) == (d.getMonth()+ 1)){
        if( parseInt(day) > d.getDate()) {err = 101;}
      }
   }

   /* Validation of month*/
   if ((month < 1) || (month > 12)) {
      err = 21;
   }

   /* Validation of day*/
   if (day < 1) {
     err = 22;
   }
   /* Validation leap-year / february / day */
   if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0)) {
      leap = 1;
   }
   if ((month == 2) && (leap == 1) && (day > 29)) {
      err = 23;
   }
   if ((month == 2) && (leap != 1) && (day > 28)) {
      err = 24;
   }
   /* Validation of other months */
   if ((day > 31) && ((month == "01") || (month == "03") || (month == "05") || (month == "07") || (month == "08") || (month == "10") || (month == "12"))) {
      err = 25;
   }
   if ((day > 30) && ((month == "04") || (month == "06") || (month == "09") || (month == "11"))) {
      err = 26;
   }
   /* if 00 ist entered, no error, deleting the entry */
   if ((day == 0) && (month == 0) && (year == 00)) {
      err = 0; day = ""; month = ""; year = ""; seperator = "";
   }
   /* if no error, write the completed date to Input-Field (e.g. 13.12.2001) */

   if (err == 0) {
      DateField.value =  month + seperator + day + seperator + year;
   }
   /* Error-message if err != 0 */
   else {
     if(err == 101){
       alert("You must be at least 18 years old to participate.");
       }
     else{
      alert("Your birth date is incorrect.");
      }

      DateField.select();
      DateField.focus();
      return false;
   }
   return true;
}

function Validate(form)
{
    if ( trim( form.first_name.value ) == "" )
    {
	alert("Your first name cannot not be left blank.\nPlease enter your first name.");
	form.first_name.focus();
        return false;
    }

    if ( trim( form.last_name.value ) == "" )
    {
        alert("Your last name cannot be left blank.\nPlease enter your last name.");
        form.last_name.focus();
        return false;
    }

    if ( trim(form.birth_date.value) == "" )
    {
        alert("Your birth date is incorrect.");
        form.birth_date.focus();
        return false;
    }

    if ( ! check_date(form.birth_date) )
    {
        return false;
    }

    return true;
}

function trim( str )
{
    var trimmed = str;
    while ( trimmed.substr(0, 1) == ' ' )
        trimmed = trimmed.substr(1);
    while ( trimmed.substr(trimmed.length - 1, trimmed.length) == ' ' )
        trimmed = trimmed.substr( 0, trimmed.length - 1 );
    return trimmed;
}

</SCRIPT>
<HTML>
  <HEAD>
    <TITLE>Philadelphia II - Voter Registration User/Password search</TITLE>
    <link href="style.css" rel="stylesheet" type="text/css">
  </HEAD>
  <BODY>
    <?php include("menu-anon.htm"); ?>
	<?php include("top.htm"); ?>
	  <h1>Search Registration</h1>
      <FORM method="post" id="frmMemberInfo" name="frmMemberInfo" action="UserPassSearch.php">

		<TABLE class="c1">
		  <TBODY>
		    <TR>
		      <TD align="left" colspan="3" height="10">
			Please enter the information below, we will attempt to find your previous user identifier and password:<BR>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* First name
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="first_name" size="40" name="first_name" value="">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* Last name
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="last_name" size="40" name="last_name" value="">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			City, Town of birth
		      </TD>
		      <TD align="left">
			<INPUT type="text" size="40" id="birth_city" name="birth_city" value="">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			State of birth (if US, 2-letter abbrev.)
		      </TD>
		      <TD align="left">
			<INPUT type="text" size="40" id="birth_state" name="birth_state" value="">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			Country of birth
		      </TD>
		      <TD align="left">
			<INPUT type="text" size="40" id="birth_country" name="birth_country" value="">
		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap valign="top">
			* Birth date
		      </TD>
		      <TD align="left">
			<INPUT type="text" id="birth_date" name="birth_date" value="">
		      </TD>
		    </TR>
		    <TR>
		      <TD>
		      </TD>
		      <TD align="left">
			mm/dd/yyyy (e.g.: 01/15/1944)<BR>
			(must be at least 18 years old)<BR>
			* required
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10">&nbsp;
		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" align="center">
			<INPUT type="submit" name="submit" value="Search" class="Button" onClick="return Validate(this.form);">
		      </TD>
		    </TR>
		  </TBODY>
		</TABLE>

      </FORM>
    <?php include("bottom.htm"); ?>    
  </BODY>
</HTML>

