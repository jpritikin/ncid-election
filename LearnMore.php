<?php
  session_start();
  $name = $_SESSION["name"];
  header("Cache-control: private");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<TITLE>About This Election</TITLE>
<link href="style.css" rel="stylesheet" type="text/css">
<style type="text/css">
li { margin-top: 2ex; }
 </style>
</HEAD>
<BODY>
  <?php
      if (session_is_registered("SESSION")) {
		include("menu-user.htm");
      } else {
	    include("menu-anon.htm");
	  }
  ?>
  <?php include("top.htm"); ?>
          <h1>About This Election</h1>
          <h4>Who is Philadelphia II? I thought this was a national election?</h4>
          <blockquote>
            <p>The organization Philadelphia II is holding the election on the National Initiative for Democracy.
This voting web site is run by Philadelphia II.
The Philadelphia II web site is located at <a href="http://philadelphiaii.us">philadelphiaii.us</a>.</p>
			<p>The name "Philadelphia II"  is based on the historical fact that the first American constitutional  convention took place in Philadelphia from May 25th to September 17th,  1787. The name does not merely refer to the city of Philadelphia. The name is meant to suggest a second national constitutional convention held  by the People. </p>

            <p>This election is nationwide in the sense that any American eligible to vote (even living abroad) may vote in this election. See 'Who Can Vote?' below.</p>
          </blockquote>
          <h4>Why is this election being conducted by Philadelphia II?</h4>
          <blockquote>
            <p>The National Initiative for Democracy could be passed in any one of these ways:</p>
            <ul>
              <li>Congress could take on the legislation (nothing is stopping them). Once passed the House and the Senate, the President could sign it into law.</li>

<li>The legislatures of at least two-thirds of the states can call a
 national convention. The amendment would then need to be ratified by
 either the legislatures of or ratifying conventions held in
 three-fourths of the states.</li>

              <li>A direct vote by the People.</li>
            </ul>
            <p>The founders of Philadelphia II chose to ask the People directly for the following reasons:</p>
            <ul>
              <li>Similar proposals have been floating around Congress for more than 100 years, but none have even been put to a vote.</li>

<li>During  1993-1996, Senator Gravel attempted to get resolutions passed
by initiative in Missouri, Washington state, and California. His efforts
were stymied in Missouri and Washington by court cases, and the
initiative process in California proved to be too expensive for his
grassroots approach.</li>

              <li>By putting the National Initiative to direct election, we will achieve
                a broad consensus on whether or not the People want direct oversight over the decisions which affect their lives.
              </li>
              <li>Finally, it is the core idea of the National Initiative that the People ought to decide on laws directly. What better law to decide than the law which would enable them to decide on laws?
              </li>
            </ul>
          </blockquote>
          <h4>Is this &quot;electronic voting&quot; over the internet?</h4>
          <blockquote>
            <p>What we are doing is not fully electronic voting. We collect your information and your ballot over the internet, but we come back later and manually certify that it is valid. The procedures we use are every bit as rigorous as any government run election, perhaps even more rigorous than certain states which do not check any identification when you show up to vote.</p>
            <p>This election is done in two parts, the voting part which this site is performing and the certification part which will be done manually. The ballots will be certified before publishing the final tally.</p>
            <p>If you prefer not to vote over the internet, a paper ballot will be available soon.
Please contact us, and we'll email it to you when it's ready.</p>
          </blockquote>
          <h4>Who Can Vote?</h4>
          <blockquote>
            <p>Any citizen registered to vote in any government jurisdiction within the United States may participate in this election. This includes United States citizens living overseas. </p>
            <p>You may register to vote in your government jurisdiction <em>after</em> voting in this election as long as you are registered at the time your vote is certified.</p>
          </blockquote>
          <h4>Privacy.</h4>
          <blockquote>
            <p>The registration information collected by
              Philadelphia II is entered into a database on a secure server.
The registration database will
              only be used to verify registrants
              identity, the fact that the registrant voted and that the registrant
              is registered in a proper government jurisdiction. If the registrant asked to be kept informed, they will receive
              a newsletter on the progress of the Philadelphia II election. The
              registration database will not be released to any other person or
              organization. If the
              election cannot be completed, the registration database and the voting
              records will be
              destroyed.
<P>Once the election is complete, we will verify that you
              are registered to vote with your county or parish government. Verifying
              who you are and your right to vote in government elections is vital to
              the integrity of this election. If we find that you are
              not registered in your government jurisdiction, we may attempt to  notify you and
              help you register so that your vote can be counted in
              the final tally.</p>
            <p>Please read our full <a href="Privacy_Policy.php">privacy policy</a> </p>
          </blockquote>
          <h4>How long will the Philadelphia II election last?</h4>
          <blockquote>
            <p>This election officially began on September
              17, 2002 and will last until the number of votes in favor of the
              National Initiative exceeds 50% of the total number of votes cast in
              the presidential election most recent to the date of the final tally
              certification, i.e. more than 50 million registered &#8220;yes&#8221;
              votes. This could take a number of years. During the course of the
              election, new voters coming of age and voting will be added to the
              final tally. Those voters who died during the course of the election
              will be removed from the final tally.</p>
          </blockquote>
          <h4>Constitutionality.</h4>
          <p>The constitutionality of this election (whether we are &quot;allowed&quot; to do it by the Constitution) has been widely discussed amongst constitutional scholars and experts in the field.</p>
          <ul>
            <li>&#8220;First Principles&#8221; is the innate right of the People to create or alter 
              governments, constitutions, charters, and laws. The Declaration of
              Independence from Great Britain and the ratification of the Constitution
              are examples of the People&rsquo;s use of First Principles. The election for
              the National Initiative for Democracy is also a direct legislative
              act&mdash;on First Principles&mdash;the result of which can be an alteration of the
              government.
            </li>
            <li>The First Amendment gives the People the right to petition the government for a redress of grievances. The election of the National Initiative could also be construed as a petition under this amendment.
            Philadelphia II may use the results of this election as a petition under this amendment. </li>
            <li>The Ninth Amendment states &#8220;The enumeration in the Constitution, of certain rights, shall not be construed to deny or disparage others retained by the people.&#8221; That the Constitution
              specifies no procedure for the People to amend the Constitution does not preclude the People from amending it.
            </li>
            <li>The Constitution&#8217;s Preamble records the fact that &quot;We the People of the United
              States ... ordain and establish this Constitution for the United States
              of America.&quot; Although the Constitution was drafted by &quot;The
              Framers,&quot; it was &quot;We the People,&quot; through  Article VII of the Constitution, who elected delegates to state
              conventions for the sole purpose of ratification of the Constitution.
Even though it was done through the intermediary of specially
              elected delegates,              this ratification process
              of 1787 to 1789 is portrayed and accepted as the ratification of the
              Constitution by the People, creating the government of the United
              States. The communications technology of the day did not
              permit a direct vote.
              Madison&#8217;s plan of ratification was designed to
              circumvent the Congress and the state legislatures, who in all
              likelihood would not have ratified the Constitution since it
              dissolved the Confederation and diluted the power of the
              states.<br>
              <br>
 Article VII describes how the Constitution itself was ratified. The People&#8217;s sovereign power as <em>creators</em> of the Constitution is
              therein acknowledged. Thus the election of the National Initiative can be implemented by the same methodology
              used in 1787.
              The People being the creator and the government
              their creation clearly establishes the right and power of the People
              to implement the National Initiative.
              If that is not the case and the Constitution
              can only be amended through Article V, a process controlled
              by government officialdom, then the creator, the People, are
              unequivocally and forever ruled by their creation, a ludicrous
              proposition for a democratic republic.
            </li>
          </ul>
          <p>There is a general misconception that the People are not allowed to change the constitution through a direct election. Yet,
            from at least four different viewpoints, it is clear that we, the
            People, have the right to vote on and enact the National Initiative.
            Though it is difficult to hold such an election without assistance from 
            the government, it is not impossible.</p>
          <p>&#8220;The
            basis of our political systems is the right of the People to make and
            to alter their constitutions of
            government.&#8221;<br>
            &nbsp;&nbsp;&nbsp;&nbsp;-- George
            Washington.</p>
          <p>&#8220;Each generation has a right
            to choose for itself the form of government it believes most promotive of its
            happiness.&#8221;<br>
            &nbsp;&nbsp;&nbsp;&nbsp;-- Thomas Jefferson.</p>
          <table align="center">
            <tbody>
              <tr>
                <?php if (session_is_registered("SESSION")) { ?>
                <td><FORM action="Ballot.php">
                    <INPUT type="submit" value="Return to your ballot" class="Button">
                  </FORM></td>
                <?php } else { ?>
                <td><FORM action="login.php">
                    <INPUT type="submit" value="Return to login screen" class="Button">
                  </FORM></td>
                <td>&nbsp;</td>
                <td><FORM action="EditNewVoter.php">
                    <INPUT type="submit" value="Register" class="Button">
                  </FORM></td>
                <?php } ?>
              </tr>
            </tbody>
          </table>
          <?php include("bottom.htm"); ?>
</BODY>
</HTML>
