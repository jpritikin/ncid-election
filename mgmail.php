<?

function mgmail( $to, $subj, $message, $headers ) {

	$from = "info@votep2.us";

	$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
	if ($socket < 0) {
		error_log("socket_create() failed: reason: " . socket_strerror($socket));
		exit();
	}
	
	$address = "127.0.0.1";
	$service_port = 25;
	$result = socket_connect($socket, $address, $service_port);
	if ($result < 0) {
		error_log("socket_connect() failed.\nReason: ($result) " . socket_strerror($result));
		exit();
	}
	
	$response = socket_read($socket, 2048);

	$msg = "HELO networkguild.org\r\n";
	socket_write($socket, $msg, strlen($msg));
	$response = socket_read($socket, 2048);
	//print "1:" .$response . "<br>";

	$msg = "MAIL FROM: <$from>\r\n";
	socket_write($socket, $msg, strlen($msg));
	$response = socket_read($socket, 2048);
	//print "2:" .$response . "<br>";
	
	$msg = "RCPT TO: <$to>\r\n";
	socket_write($socket, $msg, strlen($msg));
	$response = socket_read($socket, 2048);
	//print "3:" .$response . "<br>";

	$msg = "DATA\r\n";
	socket_write($socket, $msg, strlen($msg));
	$response = socket_read($socket, 2048);
	//print "4:" .$response . "<br>";
	
	
	/* MUST REMOVE any TRAILING \r\n from headers!!! */
	$headers = preg_replace( "/(\\r\\n)*$/","",$headers );

	$headers .= "\r\n"; // assume last of headers doesn't have \r\n
	
	
	$msg = "$headers"
	  . "Subject: $subj\r\n"
	  . "To: $to\r\n\r\n"
		. "$message\r\n.\r\n";
	socket_write($socket, $msg, strlen($msg));
	$response = socket_read($socket, 2048);
	//print "5:" .$response . "<br>";

}

?>
