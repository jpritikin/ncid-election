<?php
  session_start();
  $name = $_SESSION["name"];
  header("Cache-control: private");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<TITLE>Copywrite</TITLE>
<link href="style.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY>
<?php
      if (session_is_registered("SESSION")) {
		include("menu-user.htm");
      } else {
	    include("menu-anon.htm");
	  }
	?>
<?php include("top.htm"); ?>

Copyright &copy; 2008 <a href="http://philadelphiatwo.org">Philadelphia II</a>
<P>
This website is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
<P>
This website is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.
<P>
You can retrieve a copy of the license and source code using
<a href="http://git.or.cz/">GIT</a>:
<P>
<tt>git clone git://place/url/here

<?php include("bottom.htm"); ?>
</BODY>
</HTML>
