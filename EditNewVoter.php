<?
    header("Cache-control: private");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<SCRIPT language="JavaScript" type="text/javascript">

function check_date(field)
{
  var checkstr = "0123456789";
  var DateField = field;
  var Datevalue = "";
  var DateTemp = "";
  var separator = ".";
  var seperator = "/";
  var month;
  var day;
  var year;
  var leap = 0;
  var err = 0;
  var i;
  err = 0;
  DateValue = DateField.value;
  var x = DateValue;
  var arr = x.split("/");

  if ( arr.length == 3)
  {
        if (DateValue.length != 10)
        {
                if (arr[0].length != 2 )
                {
                    arr[0]= "0" + arr[0];
                }
                if (arr[1].length != 2 )
                {
                    arr[1]= "0" + arr[1];
                }
                if (arr[2].length == 2 )
                {
                    arr[2]= "19" + arr[2];
                }

                DateValue = arr[0] + "/" + arr[1] + "/" + arr[2] ;
        }
   }
   /* Delete all chars except 0..9 */
   for (i = 0; i < DateValue.length; i++)
   {
      if (checkstr.indexOf(DateValue.substr(i,1)) >= 0)
      {
         DateTemp = DateTemp + DateValue.substr(i,1);
      }
   }
   DateValue = DateTemp;
   /* Always change date to 8 digits - string*/
   /* if year is entered as 2-digit / always assume 20xx */
   /* if (DateValue.length == 6) {
      DateValue = DateValue.substr(0,4) + '19' + DateValue.substr(4,2); }
   */

   if (DateValue.length != 8) {
   alert("Please enter your birth date in mm/dd/yyyy format.");
      DateField.select();
      DateField.focus();
      return false;
      //err = 19;
      }
   /* year is wrong if year = 0000 */
   year = DateValue.substr(4,4);
   if (year == 0) {
      err = 20;
   }

    year = DateValue.substr(4,4);
      month = DateValue.substr(0,2);
     day = DateValue.substr(2,2);

    var d = new Date();
   if ((d.getFullYear() - year < 18)){ err = 101;}

   if ((d.getFullYear() - year == 18)){
      if( parseInt(month) > (d.getMonth()+ 1)){err = 101;}
      if ( parseInt(month) == (d.getMonth()+ 1)){
        if( parseInt(day) > d.getDate()) {err = 101;}
      }
   }

   /* Validation of month*/
   if ((month < 1) || (month > 12)) {
      err = 21;
   }

   /* Validation of day*/
   if (day < 1) {
     err = 22;
   }
   /* Validation leap-year / february / day */
   if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0)) {
      leap = 1;
   }
   if ((month == 2) && (leap == 1) && (day > 29)) {
      err = 23;
   }
   if ((month == 2) && (leap != 1) && (day > 28)) {
      err = 24;
   }
   /* Validation of other months */
   if ((day > 31) && ((month == "01") || (month == "03") || (month == "05") || (month == "07") || (month == "08") || (month == "10") || (month == "12"))) {
      err = 25;
   }
   if ((day > 30) && ((month == "04") || (month == "06") || (month == "09") || (month == "11"))) {
      err = 26;
   }
   /* if 00 ist entered, no error, deleting the entry */
   if ((day == 0) && (month == 0) && (year == 00)) {
      err = 0; day = ""; month = ""; year = ""; seperator = "";
   }
   /* if no error, write the completed date to Input-Field (e.g. 13.12.2001) */

   if (err == 0) {
      DateField.value =  month + seperator + day + seperator + year;
   }
   /* Error-message if err != 0 */
   else {
     if(err == 101){
       alert("You must be at least 18 years old to participate.");
       }
     else{
      alert("Your birth date is incorrect.");
      }

      DateField.select();
      DateField.focus();
      return false;
   }
   return true;
}

function Reload()
{
    document.frmMemberInfo.action = "EditNewVoter.php";
}

function Validate(form)
{
    if ( trim( form.first_name.value ) == "" )
    {
	alert("Your first name cannot not be left blank.\nPlease enter your first name.");
	form.first_name.focus();
        return false;
    }

    if ( trim( form.last_name.value ) == "" )
    {
        alert("Your last name cannot be left blank.\nPlease enter your last name.");
        form.last_name.focus();
        return false;
    }

    if ( trim( form.address_1.value ) == "" )
    {
        alert("The first line of your address cannot be left blank.\nPlease enter the first line of your address.");
        form.address_1.focus();
        return false;
    }

    if ( trim( form.city.value ) == "" )
    {
        alert("Your city cannot be left blank.\nPlease enter your city.");
        form.city.focus();
        return false;
    }

    if ( form.state.value == 0 )
    {
        alert("Your state cannot be left blank.");
        form.city.focus();
        return false;
    }
    //form.state.value = form.state.options[form.state.selectedIndex].text;

    form.zip_code.value = trim( form.zip_code.value );
    var str = form.zip_code.value;
    var newLength = str.length;
    var aChar;
    if(newLength != 5)
    {
	alert ("Please enter your zip code");
	form.phone.focus();
	return false;
    }

    for(var i = 0; i < newLength; i++)
    {
	aChar = str.charAt(i);

	if ( (aChar < '0') || (aChar > '9') )
	{
	    alert ("Your zip code has invalid characters.\nPlease enter a valid zip code.");
	    form.zip_code.focus();
	    return false;
	}
    }

    form.postal_code.value = form.zip_code.value;

    form.zip_code4.value = trim( form.zip_code4.value );
    var str = form.zip_code4.value;
    var newLength = str.length;
    var aChar;
    if ( !( (newLength == 0) || (newLength == 4) ) )
    {
	alert ("Your zip+4 code is invalid");
	form.phone.focus();
	return false;
    }

    for(var i = 0; i < newLength; i++)
    {
	aChar = str.charAt(i);

	if ( (aChar < '0') || (aChar > '9') )
	{
	    alert ("Your zip+4 code has invalid characters.\nPlease enter a valid zip+4 code.");
	    form.zip_code.focus();
	    return false;
	}
    }

    if (newLength > 0)
    {
	form.postal_code.value += "-"+form.zip_code4.value;
    }

    if ( is_email_valid( form.email.value ) == false )
    {
	form.email.focus();
	return false;
    }

    if ( trim( form.phone.value ) == "" )
    {
        alert("Your Phone cannot not be left blank.");
        form.phone.focus();
        return false;
    }

    var str = form.phone.value;
    var newLength = str.length;
    var aChar;
    if(newLength < 10)
    {
	alert ("Please enter your phone number with areacode and/or country code\nFor example 202-555-1212 or +33.98.78.54.32.10");
	form.phone.focus();
	return false;
    }

    for(var i = 0; i < newLength; i++)
    {
	aChar = str.charAt(i);

	if (
	    (aChar != '1') &&
	    (aChar != '2') &&
	    (aChar != '3') &&
	    (aChar != '4') &&
	    (aChar != '5') &&
	    (aChar != '6') &&
	    (aChar != '7') &&
	    (aChar != '8') &&
	    (aChar != '9') &&
	    (aChar != '0') &&
	    (aChar != '.') &&
	    (aChar != '-') &&
	    (aChar != '+') &&
	    (aChar != '*') &&
	    (aChar != '#') &&
	    (aChar != '(') &&
	    (aChar != ' ') &&
	    (aChar != ')')
	    )
	{
	    alert ("Your phone number has invalid characters.\nPlease enter a valid phone number!");
	    form.phone.focus();
	    return false;
	}
    }

    if ( form.reg_state.value == "0" && form.not_registered.checked )
    {
        alert("Please enter the state or territory and county/parish if applicable\nwhere you reside and we will help you to register to vote.  You may\nvote in this election, but your vote will not be counted until you are\nregistered locally.");
        form.city.focus();
        return false;
    }

    if ( form.reg_state.value == "0" )
    {
        alert("Your state of voter registration cannot be left blank.\nPlease select your state.");
        form.city.focus();
        return false;
    }

    form.birth_city.value = trim (form.birth_city.value);
    if ( form.birth_city.value == "" )
    {
	alert("Your Birth city, town cannot be blank.");
	form.birth_city.focus();
	return false;
    }

    form.birth_country.value = trim (form.birth_country.value);
    form.birth_state.value = trim (form.birth_state.value);

    if ( form.birth_country.value == "" )
    {
	alert("Your country can not be blank.");
	form.birth_country.focus();
	return false;
    }

    if ( (form.birth_country.value == "US") || (form.birth_country.value == "USA") )
    {
	form.birth_state.value = form.birth_state.value.toUpperCase();
    }

    if ( ( (form.birth_country.value == "US") || (form.birth_country.value == "USA")) &&
	 ( (form.birth_state.value != "AA") &&
	   (form.birth_state.value != "AE") &&
	   (form.birth_state.value != "AL") &&
	   (form.birth_state.value != "AK") &&
	   (form.birth_state.value != "AP") &&
	   (form.birth_state.value != "AZ") &&
	   (form.birth_state.value != "AR") &&
	   (form.birth_state.value != "CA") &&
	   (form.birth_state.value != "CO") &&
	   (form.birth_state.value != "CT") &&
	   (form.birth_state.value != "DE") &&
	   (form.birth_state.value != "DC") &&
	   (form.birth_state.value != "FL") &&
	   (form.birth_state.value != "GA") &&
	   (form.birth_state.value != "HI") &&
	   (form.birth_state.value != "ID") &&
	   (form.birth_state.value != "IL") &&
	   (form.birth_state.value != "IN") &&
	   (form.birth_state.value != "IA") &&
	   (form.birth_state.value != "KS") &&
	   (form.birth_state.value != "KY") &&
	   (form.birth_state.value != "LA") &&
	   (form.birth_state.value != "MA") &&
	   (form.birth_state.value != "ME") &&
	   (form.birth_state.value != "MD") &&
	   (form.birth_state.value != "MH") &&
	   (form.birth_state.value != "MI") &&
	   (form.birth_state.value != "MN") &&
	   (form.birth_state.value != "MS") &&
	   (form.birth_state.value != "MO") &&
	   (form.birth_state.value != "MT") &&
	   (form.birth_state.value != "NE") &&
	   (form.birth_state.value != "NV") &&
	   (form.birth_state.value != "NH") &&
	   (form.birth_state.value != "NJ") &&
	   (form.birth_state.value != "NM") &&
	   (form.birth_state.value != "NY") &&
	   (form.birth_state.value != "NC") &&
	   (form.birth_state.value != "ND") &&
	   (form.birth_state.value != "OH") &&
	   (form.birth_state.value != "OK") &&
	   (form.birth_state.value != "OR") &&
	   (form.birth_state.value != "PA") &&
	   (form.birth_state.value != "PW") &&
	   (form.birth_state.value != "PH") &&
	   (form.birth_state.value != "RI") &&
	   (form.birth_state.value != "SC") &&
	   (form.birth_state.value != "SD") &&
	   (form.birth_state.value != "TN") &&
	   (form.birth_state.value != "TX") &&
	   (form.birth_state.value != "UT") &&
	   (form.birth_state.value != "VT") &&
	   (form.birth_state.value != "VA") &&
	   (form.birth_state.value != "WA") &&
	   (form.birth_state.value != "WV") &&
	   (form.birth_state.value != "WI") &&
	   (form.birth_state.value != "WY") &&
	   (form.birth_state.value != "AS") &&
	   (form.birth_state.value != "FM") &&
	   (form.birth_state.value != "GU") &&
	   (form.birth_state.value != "MP") &&
	   (form.birth_state.value != "PR") &&
	   (form.birth_state.value != "VI") ) )
    {
        alert("Please select the US state or territory you were born in");
        form.birth_state.focus();
        return false;
    }

    if ( trim(form.birth_date.value) == "" )
    {
        alert("Your birth date is incorrect.");
        form.birth_date.focus();
        return false;
    }

    if ( ! check_date(form.birth_date) )
    {
        return false;
    }

    if ( form.selQuestion.value == "0" )
    {
        alert("You must select a question.");
        form.selQuestion.focus();
        return false;
    }

    form.question.value = form.selQuestion.options[form.selQuestion.selectedIndex].text;

    form.response.value = trim( form.response.value );
    if ( form.response.value == "")
    {
        alert("You must give a response to your question.");
        form.response.focus();
        return false;
    }

//    form.reg_state.value = form.reg_state.options[form.reg_state.selectedIndex].text;

    form.action = "ConfirmAddNewVoter.php";

    return true;
}

function is_email_valid( test_email )
{
    var email = trim( test_email );

    var len = email.length;
    if ( len == 0 )
    {   /* j@e.x */
        alert("Your email address cannot not be left blank.\nPlease enter your email address. ");
        return false;
    }

    if ( len < 5 )
    {   /* j@e.x */
        alert("Your email address does not have enough characters.\nPlease enter a valid email address.");
        return false;
    }

    var pos = email.indexOf("@");
    if ( pos == -1 )
    {
        alert("Your email address does not have an @ (at) character.\nPlease enter a valid email address.");
        return false;
    }
    if ( pos == 0 )
    {
        alert("Your email address should not have the @ (at) sign as the first character.\nPlease enter a valid email address.");
        return false;
    }
    var dom = email.substr(pos+1);
    if ( (pos == len - 1) || (dom.indexOf(".") < 1) || (dom.substr(-1,1) == ".") )
    {
        alert("Your email address must have a valid domain name after the @.\nPlease enter a valid email address.");
        return false;
    }
    if ( email.indexOf("@", pos + 1) != -1 )
    {
        alert("Your email address has more than one @ (at) character.\nPlease enter a valid email address.");
        return false;
    }
    var invalid_chars = "*|,\":<>[]{}`\';()&$#%";
    for ( var i = 0; i < len; i++ )
    {
        if ( invalid_chars.indexOf(email.charAt(i)) != -1 )
        {
            alert("Your email address has invalid characters.\nPlease enter a valid email address.");
            return false;
        }
    }

    return true;
}

function trim( str )
{
    var trimmed = str;
    while ( trimmed.substr(0, 1) == ' ' )
        trimmed = trimmed.substr(1);
    while ( trimmed.substr(trimmed.length - 1, trimmed.length) == ' ' )
        trimmed = trimmed.substr( 0, trimmed.length - 1 );
    return trimmed;
}

function setRegState( form )
{
//    if ( form.reg_state.value == "0" ) {
	form.reg_state.options[form.state.selectedIndex].selected=true;
//    }
    return;
}

</SCRIPT>

<HTML>
  <HEAD>
    <TITLE>Philadelphia II - Voter Registration</TITLE>
    <link href="style.css" rel="stylesheet" type="text/css">
  </HEAD>
  <BODY>
    <?php include("menu-anon.htm"); ?>
	<?php include("top.htm"); ?>
	  <h1>US Resident Registration</h1>
      <FORM method="post" id="frmMemberInfo" name="frmMemberInfo" action="ConfirmAddNewVoter.php">
	    <INPUT type="hidden" name="reedit_uri" value="EditNewVoter.php">

		<TABLE>
		  <TBODY>
		    <TR>
		      <TD align="left" colspan="3">
			    <p>If you are a US citizen living outside the United States or its territories, click <a href="EditNewVoterIntl.php">here</a> for an international version of this form.</p>
			    <p>This information is being collected on a secure site and
			      will<strong> </strong>be used to validate your government registration and your right to vote.  Please see our <a href="Privacy_Policy.php">privacy policy</a>. Since information will be validated manually, please be as accurate as you can to help us in this monumental effort.</p>
			    <p>This is an election, so please bear with us as we fulfill the legal requirements to identify you.</p>			    </TD>
		    </TR>
		    <TR>
		      <TD align="left" colspan="2">
		        <B><br>
		        Your name as you registered to vote:</B>		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			Title		      </TD>
		      <TD align="left">
			<INPUT type="text" id="title" size="40" name="title" value="<?= $_POST['title'] ?>">		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* First name		      </TD>
		      <TD align="left">
			<INPUT type="text" id="first_name" size="40" name="first_name" value="<?= $_POST['first_name'] ?>">		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			Middle name or initial		      </TD>
		      <TD align="left">
			<INPUT type="text" id="middle_name" size="40" name="middle_name" value="<?= $_POST['middle_name'] ?>">		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* Last name		      </TD>
		      <TD align="left">
			<INPUT type="text" id="last_name" size="40" name="last_name" value="<?= $_POST['last_name'] ?>">		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			Suffix		      </TD>
		      <TD align="left">
			<INPUT type="text" id="suffix" size="40" name="suffix" value="<?= $_POST['suffix'] ?>">		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* Address		      </TD>
		      <TD align="left">
			<INPUT type="text" id="address_1" size="40" name="address_1" value="<?= $_POST['address_1'] ?>">		      </TD>
		    </TR>
		    <TR>
		      <TD align="left">&nbsp;		      </TD>
		      <TD align="left">
			<INPUT type="text" id="address_2" size="40" name="address_2" value="<?= $_POST['address_2'] ?>">		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* City		      </TD>
		      <TD align="left">
			<INPUT type="text" id="city" size="40" name="city" value="<?= $_POST['city'] ?>">		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* State		      </TD>
		      <TD align="left">
			<SELECT name="state" onChange="setRegState(form);">
			  <OPTION value="0" <?= $_POST['state']=="0"?"SELECTED":"" ?>>
			    --select--			  </OPTION>
			  <OPTION value="AL" <?= $_POST['state']=="AL"?"SELECTED":"" ?>>
			    Alabama			  </OPTION>
			  <OPTION value="AK" <?= $_POST['state']=="AK"?"SELECTED":"" ?>>
			    Alaska			  </OPTION>
			  <OPTION value="AZ" <?= $_POST['state']=="AZ"?"SELECTED":"" ?>>
			    Arizona			  </OPTION>
			  <OPTION value="AR" <?= $_POST['state']=="AR"?"SELECTED":"" ?>>
			    Arkansas			  </OPTION>
			  <OPTION value="CA" <?= $_POST['state']=="CA"?"SELECTED":"" ?>>
			    California			  </OPTION>
			  <OPTION value="CO" <?= $_POST['state']=="CO"?"SELECTED":"" ?>>
			    Colorado			  </OPTION>
			  <OPTION value="CT" <?= $_POST['state']=="CT"?"SELECTED":"" ?>>
			    Connecticut			  </OPTION>
			  <OPTION value="DE" <?= $_POST['state']=="DE"?"SELECTED":"" ?>>
			    Delaware			  </OPTION>
			  <OPTION value="DC" <?= $_POST['state']=="DC"?"SELECTED":"" ?>>
			    District Of Columbia			  </OPTION>
			  <OPTION value="FL" <?= $_POST['state']=="FL"?"SELECTED":"" ?>>
			    Florida			  </OPTION>
			  <OPTION value="GA" <?= $_POST['state']=="GA"?"SELECTED":"" ?>>
			    Georgia			  </OPTION>
			  <OPTION value="HI" <?= $_POST['state']=="HI"?"SELECTED":"" ?>>
			    Hawaii			  </OPTION>
			  <OPTION value="ID" <?= $_POST['state']=="ID"?"SELECTED":"" ?>>
			    Idaho			  </OPTION>
			  <OPTION value="IL" <?= $_POST['state']=="IL"?"SELECTED":"" ?>>
			    Illinois			  </OPTION>
			  <OPTION value="IN" <?= $_POST['state']=="IN"?"SELECTED":"" ?>>
			    Indiana			  </OPTION>
			  <OPTION value="IA" <?= $_POST['state']=="IA"?"SELECTED":"" ?>>
			    Iowa			  </OPTION>
			  <OPTION value="KS" <?= $_POST['state']=="KS"?"SELECTED":"" ?>>
			    Kansas			  </OPTION>
			  <OPTION value="KY" <?= $_POST['state']=="KY"?"SELECTED":"" ?>>
			    Kentucky			  </OPTION>
			  <OPTION value="LA" <?= $_POST['state']=="LA"?"SELECTED":"" ?>>
			    Louisiana			  </OPTION>
			  <OPTION value="ME" <?= $_POST['state']=="ME"?"SELECTED":"" ?>>
			    Maine			  </OPTION>
			  <OPTION value="MD" <?= $_POST['state']=="MD"?"SELECTED":"" ?>>
			    Maryland			  </OPTION>
			  <OPTION value="MA" <?= $_POST['state']=="MA"?"SELECTED":"" ?>>
			    Massachusetts			  </OPTION>
			  <OPTION value="MI" <?= $_POST['state']=="MI"?"SELECTED":"" ?>>
			    Michigan			  </OPTION>
			  <OPTION value="MN" <?= $_POST['state']=="MN"?"SELECTED":"" ?>>
			    Minnesota			  </OPTION>
			  <OPTION value="MS" <?= $_POST['state']=="MS"?"SELECTED":"" ?>>
			    Mississippi			  </OPTION>
			  <OPTION value="MO" <?= $_POST['state']=="MO"?"SELECTED":"" ?>>
			    Missouri			  </OPTION>
			  <OPTION value="MT" <?= $_POST['state']=="MT"?"SELECTED":"" ?>>
			    Montana			  </OPTION>
			  <OPTION value="NE" <?= $_POST['state']=="NE"?"SELECTED":"" ?>>
			    Nebraska			  </OPTION>
			  <OPTION value="NV" <?= $_POST['state']=="NV"?"SELECTED":"" ?>>
			    Nevada			  </OPTION>
			  <OPTION value="NH" <?= $_POST['state']=="NH"?"SELECTED":"" ?>>
			    New Hampshire			  </OPTION>
			  <OPTION value="NJ" <?= $_POST['state']=="NJ"?"SELECTED":"" ?>>
			    New Jersey			  </OPTION>
			  <OPTION value="NM" <?= $_POST['state']=="NM"?"SELECTED":"" ?>>
			    New Mexico			  </OPTION>
			  <OPTION value="NY" <?= $_POST['state']=="NY"?"SELECTED":"" ?>>
			    New York			  </OPTION>
			  <OPTION value="NC" <?= $_POST['state']=="NC"?"SELECTED":"" ?>>
			    North Carolina			  </OPTION>
			  <OPTION value="ND" <?= $_POST['state']=="ND"?"SELECTED":"" ?>>
			    North Dakota			  </OPTION>
			  <OPTION value="OH" <?= $_POST['state']=="OH"?"SELECTED":"" ?>>
			    Ohio			  </OPTION>
			  <OPTION value="OK" <?= $_POST['state']=="OK"?"SELECTED":"" ?>>
			    Oklahoma			  </OPTION>
			  <OPTION value="OR" <?= $_POST['state']=="OR"?"SELECTED":"" ?>>
			    Oregon			  </OPTION>
			  <OPTION value="PA" <?= $_POST['state']=="PA"?"SELECTED":"" ?>>
			    Pennsylvania			  </OPTION>
			  <OPTION value="RI" <?= $_POST['state']=="RI"?"SELECTED":"" ?>>
			    Rhode Island			  </OPTION>
			  <OPTION value="SC" <?= $_POST['state']=="SC"?"SELECTED":"" ?>>
			    South Carolina			  </OPTION>
			  <OPTION value="SD" <?= $_POST['state']=="SD"?"SELECTED":"" ?>>
			    South Dakota			  </OPTION>
			  <OPTION value="TN" <?= $_POST['state']=="TN"?"SELECTED":"" ?>>
			    Tennessee			  </OPTION>
			  <OPTION value="TX" <?= $_POST['state']=="TX"?"SELECTED":"" ?>>
			    Texas			  </OPTION>
			  <OPTION value="UT" <?= $_POST['state']=="UT"?"SELECTED":"" ?>>
			    Utah			  </OPTION>
			  <OPTION value="VT" <?= $_POST['state']=="VT"?"SELECTED":"" ?>>
			    Vermont			  </OPTION>
			  <OPTION value="VA" <?= $_POST['state']=="VA"?"SELECTED":"" ?>>
			    Virginia			  </OPTION>
			  <OPTION value="WA" <?= $_POST['state']=="WA"?"SELECTED":"" ?>>
			    Washington			  </OPTION>
			  <OPTION value="WV" <?= $_POST['state']=="WV"?"SELECTED":"" ?>>
			    West Virginia			  </OPTION>
			  <OPTION value="WI" <?= $_POST['state']=="WI"?"SELECTED":"" ?>>
			    Wisconsin			  </OPTION>
			  <OPTION value="WY" <?= $_POST['state']=="WY"?"SELECTED":"" ?>>
			    Wyoming			  </OPTION>
			  <OPTION value="AS" <?= $_POST['state']=="AS"?"SELECTED":"" ?>>
			    American Samoa			  </OPTION>
			  <OPTION value="AE" <?= $_POST['state']=="AE"?"SELECTED":"" ?>>
			    Armed Forces Other			  </OPTION>
			  <OPTION value="AA" <?= $_POST['state']=="AA"?"SELECTED":"" ?>>
			    Armed Forces Americas			  </OPTION>
			  <OPTION value="AP" <?= $_POST['state']=="AP"?"SELECTED":"" ?>>
			    Armed Forces Pacific			  </OPTION>
			  <OPTION value="FM" <?= $_POST['state']=="FM"?"SELECTED":"" ?>>
			    Federated States of Micronesia			  </OPTION>
			  <OPTION value="GU" <?= $_POST['state']=="GU"?"SELECTED":"" ?>>
			    Guam			  </OPTION>
			  <OPTION value="MH" <?= $_POST['state']=="MH"?"SELECTED":"" ?>>
			    Marshall Islands			  </OPTION>
			  <OPTION value="MP" <?= $_POST['state']=="MP"?"SELECTED":"" ?>>
			    Northern Mariana Islands			  </OPTION>
			  <OPTION value="PW" <?= $_POST['state']=="PW"?"SELECTED":"" ?>>
			    Palau			  </OPTION>
			  <OPTION value="PR" <?= $_POST['state']=="PR"?"SELECTED":"" ?>>
			    Puerto Rico			  </OPTION>
			  <OPTION value="VI" <?= $_POST['state']=="VI"?"SELECTED":"" ?>>
			    Virgin Islands			  </OPTION>
			</SELECT>
			<INPUT type="hidden" id="country" name="country" value="<?= $_POST['country']?$_POST['country']:'US'; ?>">		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* Zip Code		      </TD>
		      <TD align="left">
			<INPUT type="text" id="zip_code" size="5" maxlength="5" name="zip_code" value="<?= $_POST['zip_code'] ?>">-
			<INPUT type="text" id="zip_code4" size="4" maxlength="4" name="zip_code4" value="<?= $_POST['zip_code4'] ?>">
			<INPUT type="hidden" id="postal_code" size="10" maxlength="10" name="postal_code" value="<?= $_POST['postal_code'] ?>">		      </TD>
		    </TR>
			<TR>
	      <TD colspan="3">If you live outside the United States or its territories, click <a href="EditNewVoterIntl.php">here</a> for an international version of this form. </TD>
			</TR>
		    <TR>
		      <TD align="right" nowrap valign="middle" height="2">
			<A name="Note1back">&nbsp;* Email address</A>		      </TD>
		      <TD align="left">
			<INPUT type="text" id="email" size="40" name="email" value="<?= $_POST['email'] ?>">		      </TD>
		      <TD align="left" width="10">
			<A href="#Note1" class="c2" tabindex="-1">Note&nbsp;1</A>		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap valign="middle" height="2">
			* Phone number		      </TD>
		      <TD align="left">
			<INPUT type="text" id="phone" size="40" name="phone" value="<?= $_POST['phone'] ?>">		      </TD>
		    </TR>
		    <TR>
                      <? // if keep_informed is neither "Yes" nor "No", check the box
		         if (! $_POST['keep_informed']) { $sel_keep_informed = "checked"; }
			 else { $sel_keep_informed = $_POST['keep_informed']=="true" ? "checked" : ""; } ?>
		      <TD align="left" colspan="2">
			<CENTER><INPUT type="checkbox" id="keep_informed" name="keep_informed" value="true" <?= $sel_keep_informed ?>> Keep me informed on the progress of this election</CENTER>
		      </TD>
		    </TR>
		    <TR>
		      <TD align="left" colspan="2">
		        <BR><B>The place you are registered to vote (or if not registered the last place resided at in US:</B>		      </TD>
		    </TR>
		    <TR>
		      <TD valign="middle" height="2" align="right" nowrap>
			<A name="Note1back">County or Parish</A>		      </TD>
		      <TD align="left">
			<INPUT type="text" id="reg_county" size="40" name="reg_county" value="<?= $_POST['reg_county'] ?>">		      </TD>
		      <TD align="left" width="10">
			<A href="#Note2" class="c2" tabindex="-1">Note&nbsp;2</A>		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* State		      </TD>
		      <TD align="left">
			<SELECT name="reg_state">
			  <OPTION value="0" <?= $_POST['reg_state']=="0"?"SELECTED":"" ?>>
			    --select--			  </OPTION>
			  <OPTION value="AL" <?= $_POST['reg_state']=="AL"?"SELECTED":"" ?>>
			    Alabama			  </OPTION>
			  <OPTION value="AK" <?= $_POST['reg_state']=="AK"?"SELECTED":"" ?>>
			    Alaska			  </OPTION>
			  <OPTION value="AZ" <?= $_POST['reg_state']=="AZ"?"SELECTED":"" ?>>
			    Arizona			  </OPTION>
			  <OPTION value="AR" <?= $_POST['reg_state']=="AR"?"SELECTED":"" ?>>
			    Arkansas			  </OPTION>
			  <OPTION value="CA" <?= $_POST['reg_state']=="CA"?"SELECTED":"" ?>>
			    California			  </OPTION>
			  <OPTION value="CO" <?= $_POST['reg_state']=="CO"?"SELECTED":"" ?>>
			    Colorado			  </OPTION>
			  <OPTION value="CT" <?= $_POST['reg_state']=="CT"?"SELECTED":"" ?>>
			    Connecticut			  </OPTION>
			  <OPTION value="DE" <?= $_POST['reg_state']=="DE"?"SELECTED":"" ?>>
			    Delaware			  </OPTION>
			  <OPTION value="DC" <?= $_POST['reg_state']=="DC"?"SELECTED":"" ?>>
			    District Of Columbia			  </OPTION>
			  <OPTION value="FL" <?= $_POST['reg_state']=="FL"?"SELECTED":"" ?>>
			    Florida			  </OPTION>
			  <OPTION value="GA" <?= $_POST['reg_state']=="GA"?"SELECTED":"" ?>>
			    Georgia			  </OPTION>
			  <OPTION value="HI" <?= $_POST['reg_state']=="HI"?"SELECTED":"" ?>>
			    Hawaii			  </OPTION>
			  <OPTION value="ID" <?= $_POST['reg_state']=="ID"?"SELECTED":"" ?>>
			    Idaho			  </OPTION>
			  <OPTION value="IL" <?= $_POST['reg_state']=="IL"?"SELECTED":"" ?>>
			    Illinois			  </OPTION>
			  <OPTION value="IN" <?= $_POST['reg_state']=="IN"?"SELECTED":"" ?>>
			    Indiana			  </OPTION>
			  <OPTION value="IA" <?= $_POST['reg_state']=="IA"?"SELECTED":"" ?>>
			    Iowa			  </OPTION>
			  <OPTION value="KS" <?= $_POST['reg_state']=="KS"?"SELECTED":"" ?>>
			    Kansas			  </OPTION>
			  <OPTION value="KY" <?= $_POST['reg_state']=="KY"?"SELECTED":"" ?>>
			    Kentucky			  </OPTION>
			  <OPTION value="LA" <?= $_POST['reg_state']=="LA"?"SELECTED":"" ?>>
			    Louisiana			  </OPTION>
			  <OPTION value="ME" <?= $_POST['reg_state']=="ME"?"SELECTED":"" ?>>
			    Maine			  </OPTION>
			  <OPTION value="MD" <?= $_POST['reg_state']=="MD"?"SELECTED":"" ?>>
			    Maryland			  </OPTION>
			  <OPTION value="MA" <?= $_POST['reg_state']=="MA"?"SELECTED":"" ?>>
			    Massachusetts			  </OPTION>
			  <OPTION value="MI" <?= $_POST['reg_state']=="MI"?"SELECTED":"" ?>>
			    Michigan			  </OPTION>
			  <OPTION value="MN" <?= $_POST['reg_state']=="MN"?"SELECTED":"" ?>>
			    Minnesota			  </OPTION>
			  <OPTION value="MS" <?= $_POST['reg_state']=="MS"?"SELECTED":"" ?>>
			    Mississippi			  </OPTION>
			  <OPTION value="MO" <?= $_POST['reg_state']=="MO"?"SELECTED":"" ?>>
			    Missouri			  </OPTION>
			  <OPTION value="MT" <?= $_POST['reg_state']=="MT"?"SELECTED":"" ?>>
			    Montana			  </OPTION>
			  <OPTION value="NE" <?= $_POST['reg_state']=="NE"?"SELECTED":"" ?>>
			    Nebraska			  </OPTION>
			  <OPTION value="NV" <?= $_POST['reg_state']=="NV"?"SELECTED":"" ?>>
			    Nevada			  </OPTION>
			  <OPTION value="NH" <?= $_POST['reg_state']=="NH"?"SELECTED":"" ?>>
			    New Hampshire			  </OPTION>
			  <OPTION value="NJ" <?= $_POST['reg_state']=="NJ"?"SELECTED":"" ?>>
			    New Jersey			  </OPTION>
			  <OPTION value="NM" <?= $_POST['reg_state']=="NM"?"SELECTED":"" ?>>
			    New Mexico			  </OPTION>
			  <OPTION value="NY" <?= $_POST['reg_state']=="NY"?"SELECTED":"" ?>>
			    New York			  </OPTION>
			  <OPTION value="NC" <?= $_POST['reg_state']=="NC"?"SELECTED":"" ?>>
			    North Carolina			  </OPTION>
			  <OPTION value="ND" <?= $_POST['reg_state']=="ND"?"SELECTED":"" ?>>
			    North Dakota			  </OPTION>
			  <OPTION value="OH" <?= $_POST['reg_state']=="OH"?"SELECTED":"" ?>>
			    Ohio			  </OPTION>
			  <OPTION value="OK" <?= $_POST['reg_state']=="OK"?"SELECTED":"" ?>>
			    Oklahoma			  </OPTION>
			  <OPTION value="OR" <?= $_POST['reg_state']=="OR"?"SELECTED":"" ?>>
			    Oregon			  </OPTION>
			  <OPTION value="PA" <?= $_POST['reg_state']=="PA"?"SELECTED":"" ?>>
			    Pennsylvania			  </OPTION>
			  <OPTION value="RI" <?= $_POST['reg_state']=="RI"?"SELECTED":"" ?>>
			    Rhode Island			  </OPTION>
			  <OPTION value="SC" <?= $_POST['reg_state']=="SC"?"SELECTED":"" ?>>
			    South Carolina			  </OPTION>
			  <OPTION value="SD" <?= $_POST['reg_state']=="SD"?"SELECTED":"" ?>>
			    South Dakota			  </OPTION>
			  <OPTION value="TN" <?= $_POST['reg_state']=="TN"?"SELECTED":"" ?>>
			    Tennessee			  </OPTION>
			  <OPTION value="TX" <?= $_POST['reg_state']=="TX"?"SELECTED":"" ?>>
			    Texas			  </OPTION>
			  <OPTION value="UT" <?= $_POST['reg_state']=="UT"?"SELECTED":"" ?>>
			    Utah			  </OPTION>
			  <OPTION value="VT" <?= $_POST['reg_state']=="VT"?"SELECTED":"" ?>>
			    Vermont			  </OPTION>
			  <OPTION value="VA" <?= $_POST['reg_state']=="VA"?"SELECTED":"" ?>>
			    Virginia			  </OPTION>
			  <OPTION value="WA" <?= $_POST['reg_state']=="WA"?"SELECTED":"" ?>>
			    Washington			  </OPTION>
			  <OPTION value="WV" <?= $_POST['reg_state']=="WV"?"SELECTED":"" ?>>
			    West Virginia			  </OPTION>
			  <OPTION value="WI" <?= $_POST['reg_state']=="WI"?"SELECTED":"" ?>>
			    Wisconsin			  </OPTION>
			  <OPTION value="WY" <?= $_POST['reg_state']=="WY"?"SELECTED":"" ?>>
			    Wyoming			  </OPTION>
			  <OPTION value="AS" <?= $_POST['reg_state']=="AS"?"SELECTED":"" ?>>
			    American Samoa			  </OPTION>
			  <OPTION value="AE" <?= $_POST['reg_state']=="AE"?"SELECTED":"" ?>>
			    Armed Forces Other			  </OPTION>
			  <OPTION value="AA" <?= $_POST['reg_state']=="AA"?"SELECTED":"" ?>>
			    Armed Forces Americas			  </OPTION>
			  <OPTION value="AP" <?= $_POST['reg_state']=="AP"?"SELECTED":"" ?>>
			    Armed Forces Pacific			  </OPTION>
			  <OPTION value="FM" <?= $_POST['reg_state']=="FM"?"SELECTED":"" ?>>
			    Federated States of Micronesia			  </OPTION>
			  <OPTION value="GU" <?= $_POST['reg_state']=="GU"?"SELECTED":"" ?>>
			    Guam			  </OPTION>
			  <OPTION value="MH" <?= $_POST['reg_state']=="MH"?"SELECTED":"" ?>>
			    Marshall Islands			  </OPTION>
			  <OPTION value="MP" <?= $_POST['reg_state']=="MP"?"SELECTED":"" ?>>
			    Northern Mariana Islands			  </OPTION>
			  <OPTION value="PW" <?= $_POST['reg_state']=="PW"?"SELECTED":"" ?>>
			    Palau			  </OPTION>
			  <OPTION value="PR" <?= $_POST['reg_state']=="PR"?"SELECTED":"" ?>>
			    Puerto Rico			  </OPTION>
			  <OPTION value="VI" <?= $_POST['reg_state']=="VI"?"SELECTED":"" ?>>
			    Virgin Islands			  </OPTION>
			</SELECT>		      </TD>
		    </TR>
		    <TR>
                      <? // check the box if it was previously set
		         if (! $_POST['not_registered']=="true") { $sel_not_registered = ""; }
			 else { $sel_not_registered = $_POST['not_registered']=="true" ? "checked" : ""; } ?>

		      <TD align="left" colspan="2">
			<CENTER><INPUT type="checkbox" id="not_registered" name="not_registered" value="true" <?= $sel_not_registered ?>> I am <strong>not</strong> currently registered to vote</CENTER>
		    </TR>
		    <TR>
		      <TD align="left" colspan="2">
		        <BR><B>Additional information used to validate your identity:</B>		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			<A name="Note3back">
			* City of birth</A>		      </TD>
		      <TD align="left">
			<INPUT type="text" size="40" id="birth_city" name="birth_city" value="<?= $_POST['birth_city'] ?>">		      </TD>
		      <TD align="left" width="10">
			<A href="#Note3" class="c2" tabindex="-1">Note&nbsp;3</A>		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			** State of birth		      </TD>
		      <TD align="left">
			<SELECT name="birth_state">
			  <OPTION value="" <?= $_POST['birth_state']=="0"?"SELECTED":"" ?>>
			    --select--			  </OPTION>
			  <OPTION value="AL" <?= $_POST['birth_state']=="AL"?"SELECTED":"" ?>>
			    Alabama			  </OPTION>
			  <OPTION value="AK" <?= $_POST['birth_state']=="AK"?"SELECTED":"" ?>>
			    Alaska			  </OPTION>
			  <OPTION value="AZ" <?= $_POST['birth_state']=="AZ"?"SELECTED":"" ?>>
			    Arizona			  </OPTION>
			  <OPTION value="AR" <?= $_POST['birth_state']=="AR"?"SELECTED":"" ?>>
			    Arkansas			  </OPTION>
			  <OPTION value="CA" <?= $_POST['birth_state']=="CA"?"SELECTED":"" ?>>
			    California			  </OPTION>
			  <OPTION value="CO" <?= $_POST['birth_state']=="CO"?"SELECTED":"" ?>>
			    Colorado			  </OPTION>
			  <OPTION value="CT" <?= $_POST['birth_state']=="CT"?"SELECTED":"" ?>>
			    Connecticut			  </OPTION>
			  <OPTION value="DE" <?= $_POST['birth_state']=="DE"?"SELECTED":"" ?>>
			    Delaware			  </OPTION>
			  <OPTION value="DC" <?= $_POST['birth_state']=="DC"?"SELECTED":"" ?>>
			    District Of Columbia			  </OPTION>
			  <OPTION value="FL" <?= $_POST['birth_state']=="FL"?"SELECTED":"" ?>>
			    Florida			  </OPTION>
			  <OPTION value="GA" <?= $_POST['birth_state']=="GA"?"SELECTED":"" ?>>
			    Georgia			  </OPTION>
			  <OPTION value="HI" <?= $_POST['birth_state']=="HI"?"SELECTED":"" ?>>
			    Hawaii			  </OPTION>
			  <OPTION value="ID" <?= $_POST['birth_state']=="ID"?"SELECTED":"" ?>>
			    Idaho			  </OPTION>
			  <OPTION value="IL" <?= $_POST['birth_state']=="IL"?"SELECTED":"" ?>>
			    Illinois			  </OPTION>
			  <OPTION value="IN" <?= $_POST['birth_state']=="IN"?"SELECTED":"" ?>>
			    Indiana			  </OPTION>
			  <OPTION value="IA" <?= $_POST['birth_state']=="IA"?"SELECTED":"" ?>>
			    Iowa			  </OPTION>
			  <OPTION value="KS" <?= $_POST['birth_state']=="KS"?"SELECTED":"" ?>>
			    Kansas			  </OPTION>
			  <OPTION value="KY" <?= $_POST['birth_state']=="KY"?"SELECTED":"" ?>>
			    Kentucky			  </OPTION>
			  <OPTION value="LA" <?= $_POST['birth_state']=="LA"?"SELECTED":"" ?>>
			    Louisiana			  </OPTION>
			  <OPTION value="ME" <?= $_POST['birth_state']=="ME"?"SELECTED":"" ?>>
			    Maine			  </OPTION>
			  <OPTION value="MD" <?= $_POST['birth_state']=="MD"?"SELECTED":"" ?>>
			    Maryland			  </OPTION>
			  <OPTION value="MA" <?= $_POST['birth_state']=="MA"?"SELECTED":"" ?>>
			    Massachusetts			  </OPTION>
			  <OPTION value="MI" <?= $_POST['birth_state']=="MI"?"SELECTED":"" ?>>
			    Michigan			  </OPTION>
			  <OPTION value="MN" <?= $_POST['birth_state']=="MN"?"SELECTED":"" ?>>
			    Minnesota			  </OPTION>
			  <OPTION value="MS" <?= $_POST['birth_state']=="MS"?"SELECTED":"" ?>>
			    Mississippi			  </OPTION>
			  <OPTION value="MO" <?= $_POST['birth_state']=="MO"?"SELECTED":"" ?>>
			    Missouri			  </OPTION>
			  <OPTION value="MT" <?= $_POST['birth_state']=="MT"?"SELECTED":"" ?>>
			    Montana			  </OPTION>
			  <OPTION value="NE" <?= $_POST['birth_state']=="NE"?"SELECTED":"" ?>>
			    Nebraska			  </OPTION>
			  <OPTION value="NV" <?= $_POST['birth_state']=="NV"?"SELECTED":"" ?>>
			    Nevada			  </OPTION>
			  <OPTION value="NH" <?= $_POST['birth_state']=="NH"?"SELECTED":"" ?>>
			    New Hampshire			  </OPTION>
			  <OPTION value="NJ" <?= $_POST['birth_state']=="NJ"?"SELECTED":"" ?>>
			    New Jersey			  </OPTION>
			  <OPTION value="NM" <?= $_POST['birth_state']=="NM"?"SELECTED":"" ?>>
			    New Mexico			  </OPTION>
			  <OPTION value="NY" <?= $_POST['birth_state']=="NY"?"SELECTED":"" ?>>
			    New York			  </OPTION>
			  <OPTION value="NC" <?= $_POST['birth_state']=="NC"?"SELECTED":"" ?>>
			    North Carolina			  </OPTION>
			  <OPTION value="ND" <?= $_POST['birth_state']=="ND"?"SELECTED":"" ?>>
			    North Dakota			  </OPTION>
			  <OPTION value="OH" <?= $_POST['birth_state']=="OH"?"SELECTED":"" ?>>
			    Ohio			  </OPTION>
			  <OPTION value="OK" <?= $_POST['birth_state']=="OK"?"SELECTED":"" ?>>
			    Oklahoma			  </OPTION>
			  <OPTION value="OR" <?= $_POST['birth_state']=="OR"?"SELECTED":"" ?>>
			    Oregon			  </OPTION>
			  <OPTION value="PA" <?= $_POST['birth_state']=="PA"?"SELECTED":"" ?>>
			    Pennsylvania			  </OPTION>
			  <OPTION value="RI" <?= $_POST['birth_state']=="RI"?"SELECTED":"" ?>>
			    Rhode Island			  </OPTION>
			  <OPTION value="SC" <?= $_POST['birth_state']=="SC"?"SELECTED":"" ?>>
			    South Carolina			  </OPTION>
			  <OPTION value="SD" <?= $_POST['birth_state']=="SD"?"SELECTED":"" ?>>
			    South Dakota			  </OPTION>
			  <OPTION value="TN" <?= $_POST['birth_state']=="TN"?"SELECTED":"" ?>>
			    Tennessee			  </OPTION>
			  <OPTION value="TX" <?= $_POST['birth_state']=="TX"?"SELECTED":"" ?>>
			    Texas			  </OPTION>
			  <OPTION value="UT" <?= $_POST['birth_state']=="UT"?"SELECTED":"" ?>>
			    Utah			  </OPTION>
			  <OPTION value="VT" <?= $_POST['birth_state']=="VT"?"SELECTED":"" ?>>
			    Vermont			  </OPTION>
			  <OPTION value="VA" <?= $_POST['birth_state']=="VA"?"SELECTED":"" ?>>
			    Virginia			  </OPTION>
			  <OPTION value="WA" <?= $_POST['birth_state']=="WA"?"SELECTED":"" ?>>
			    Washington			  </OPTION>
			  <OPTION value="WV" <?= $_POST['birth_state']=="WV"?"SELECTED":"" ?>>
			    West Virginia			  </OPTION>
			  <OPTION value="WI" <?= $_POST['birth_state']=="WI"?"SELECTED":"" ?>>
			    Wisconsin			  </OPTION>
			  <OPTION value="WY" <?= $_POST['birth_state']=="WY"?"SELECTED":"" ?>>
			    Wyoming			  </OPTION>
			  <OPTION value="AS" <?= $_POST['birth_state']=="AS"?"SELECTED":"" ?>>
			    American Samoa			  </OPTION>
			  <OPTION value="AE" <?= $_POST['birth_state']=="AE"?"SELECTED":"" ?>>
			    Armed Forces Other			  </OPTION>
			  <OPTION value="AA" <?= $_POST['birth_state']=="AA"?"SELECTED":"" ?>>
			    Armed Forces Americas			  </OPTION>
			  <OPTION value="AP" <?= $_POST['birth_state']=="AP"?"SELECTED":"" ?>>
			    Armed Forces Pacific			  </OPTION>
			  <OPTION value="FM" <?= $_POST['birth_state']=="FM"?"SELECTED":"" ?>>
			    Federated States of Micronesia			  </OPTION>
			  <OPTION value="GU" <?= $_POST['birth_state']=="GU"?"SELECTED":"" ?>>
			    Guam			  </OPTION>
			  <OPTION value="MH" <?= $_POST['birth_state']=="MH"?"SELECTED":"" ?>>
			    Marshall Islands			  </OPTION>
			  <OPTION value="MP" <?= $_POST['birth_state']=="MP"?"SELECTED":"" ?>>
			    Northern Mariana Islands			  </OPTION>
			  <OPTION value="PW" <?= $_POST['birth_state']=="PW"?"SELECTED":"" ?>>
			    Palau			  </OPTION>
			  <OPTION value="PR" <?= $_POST['birth_state']=="PR"?"SELECTED":"" ?>>
			    Puerto Rico			  </OPTION>
			  <OPTION value="VI" <?= $_POST['birth_state']=="VI"?"SELECTED":"" ?>>
			    Virgin Islands			  </OPTION>
			</SELECT>		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap>
			* Country of birth		      </TD>
		      <TD align="left">
			<INPUT type="text" size="40" id="birth_country" name="birth_country" value="<?= $_POST['birth_country']?$_POST['birth_country']:'USA'; ?>"> 
			(<span class="style1">e.g. USA for United States</span>) </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap valign="top">
			* Birth date		      </TD>
		      <TD align="left">
			<INPUT type="text" id="birth_date" name="birth_date" value="<?= $_POST['birth_date'] ?>">&nbsp;mm/dd/yyyy (e.g. 01/15/1944)		      </TD>
		    </TR>
		    <TR>
		      <TD>&nbsp;</TD>
		      <TD align="left">
			(must be at least 18 years old)		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap valign="top">
		       * Select a question		      </TD>
		      <TD align="left">
			<INPUT type="hidden" id="question" name="question">
			<SELECT name="selQuestion">
			  <OPTION value="0" <?= $_POST['selQuestion']=="0"?"SELECTED":"" ?>>
			    --select--			  </OPTION>
			  <OPTION value="1" <?= $_POST['selQuestion']=="1"?"SELECTED":"" ?>>
			    Favorite pet			  </OPTION>
			  <OPTION value="2" <?= $_POST['selQuestion']=="2"?"SELECTED":"" ?>>
			    Favorite drink			  </OPTION>
			  <OPTION value="3" <?= $_POST['selQuestion']=="3"?"SELECTED":"" ?>>
			    Favorite show			  </OPTION>
			  <OPTION value="4" <?= $_POST['selQuestion']=="4"?"SELECTED":"" ?>>
			    Mother's maiden name			  </OPTION>
			  <OPTION value="5" <?= $_POST['selQuestion']=="5"?"SELECTED":"" ?>>
			    Place where you grew up			  </OPTION>
			  <OPTION value="6" <?= $_POST['selQuestion']=="6"?"SELECTED":"" ?>>
			    Childhood best friend			  </OPTION>
			  <OPTION value="7" <?= $_POST['selQuestion']=="7"?"SELECTED":"" ?>>
			    A password or phrase			  </OPTION>
			</SELECT>		      </TD>
		    </TR>
		    <TR>
		      <TD align="right" nowrap valign="top">
			* Your response		      </TD>
		      <TD align="left">
			<INPUT type="text" size=40 id="response" name="response" value="<?= $_POST['response'] ?>">		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10">
			&nbsp; * required field &nbsp; ** required within the US		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10">
			<TABLE>
			  <TBODY>
			    <TR>
			      <TD valign="top" nowrap>
			        <A name="Note1">&nbsp;</A>
			        <B>Note 1:</B>			      </TD>
			      <TD>
			        Your phone number and email are vital to process your registration. To certify your vote, we must validate your identity as a registered voter for government elections; this gives you the right to vote in this Philadelphia II Election. Validating your identity will permit us to count your vote in the final tally.  <A href="#Note1back" class="c2">back</A>			      </TD>
		        </TR>
		      </TBODY>
			  </TABLE>		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10">
			<TABLE>
			  <TBODY>
			    <TR>
			      <TD valign="top" nowrap>
				<A name="Note2">&nbsp;</A>
				<B>Note 2:</B>			      </TD>
			      <TD>
				Enter your State (or US Territory) and County or Parish of residence (where you are registered to vote in government elections). If you are not registered in your county or parish or have just come of age to vote (18 years old.), please continue to register here. We will help you to register. <A href="#Note2back" class="c2">back</A>			      </TD>
		        </TR>
		      </TBODY>
			  </TABLE>		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10" align="left">
			<TABLE>
			  <TBODY>
			    <TR>
			      <TD valign="top" nowrap>
				<A name="Note3">&nbsp;</A>
				<B>Note 3:</B>			      </TD>
			      <TD>
			  	This information is used to uniquely identify you within your voting district.  Enter the place you were born, for example the name of the city or town that is marked on your birth certificate.  Select the state you were born in.  If you were not born in the United States or one of its territories, enter the name of the country you were born in. <A href="#Note3back" class="c2">back</A>			      </TD>
		        </TR>
		      </TBODY>
			  </TABLE>		      </TD>
		    </TR>
		    <TR>
		      <TD colspan="3" height="10">&nbsp;</TD>
		    </TR>
		    <TR>
		      <TD colspan="3" align="center">
			<INPUT type="submit" name="submit" value="Continue" class="Button" onClick="return Validate(this.form);">		      </TD>
		    </TR>
		  </TBODY>
		</TABLE>

	  </FORM>
    <?php include("bottom.htm"); ?>    
	</BODY>
</HTML>

