<?php
  session_start();
  $name = $_SESSION["name"];
  header("Cache-control: private");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head><title>Philadelphia II</title>

  <link href="style.css" rel="stylesheet" type="text/css">
  </head>
  <body>
	<?php
      if (session_is_registered("SESSION")) {
		include("menu-user.htm");
      } else {
	    include("menu-anon.htm");
	  }
	?>
    <?php include("top.htm"); ?>
<h1>About Philadelphia II</h1>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td class="putcenter" height="450">
		                  
							<font size="2" face="arial,sans serif">
								Philadelphia II is an IRS 501(c)(4) nonprofit public benefit corporation
								incorporated in 1992 under California nonprofit laws.
							</font><p>
<font size="2" face="arial,sans serif">								The mission of Philadelphia II is to conduct a national election giving the
								American People the opportunity to empower themselves by voting on the National
								Initiative For Democracy.							  </font></p>
								<p>
<font size="2" face="arial,sans serif">								Philadelphia II has neither a direct nor indirect connection with any political
								party, business group, labor organization, religious group or other partisan
								political organization.

								The officers of Philadelphia II are:
								</font></p><ul>
<font size="2" face="arial,sans serif">									Chairman: Tom Lombardi, North Wales, PA

									<br>
									Secretary: Donald Kemner, Chesterfield, MO
									<br>
									
								</font></ul>
<font size="2" face="arial,sans serif">								
				  </font>
<p>
<font size="2" face="arial,sans serif">								</font>This website is run by Philadelphia II </p>
<p><font size="2" face="arial,sans serif">The Philadelphia II website is <a href="http://www.philadelphiaii.org">philadelphiatwo.org</a></p>
</td>
		</tr>
	</tbody></table>
</td>
</tr>
</tbody></table>

  <table align="center">
     <tbody>
	   <tr>
    <?php if (session_is_registered("SESSION")) { ?>
								    <td>
	                          		  <FORM action="Ballot.php">
	                           		   <INPUT type="submit" value="Return to your ballot" class="Button">
	                          		  </FORM>
									</td>
	<?php } else { ?>
								    <td>
	                          		  <FORM action="login.php">
	                           		   <INPUT type="submit" value="Return to login screen" class="Button">
	                          		  </FORM>
									</td>
									<td>&nbsp;</td>
									<td>
	                          		  <FORM action="EditNewVoter.php">
	                          		    <INPUT type="submit" value="Register" class="Button">
	                          		  </FORM>
							  		</td>
	<?php } ?>
								  </tr>
								</tbody>
							  </table>
	<?php include("bottom.htm"); ?>
